<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Faizchicago';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
     
    
    <?= $this->Html->meta('favicon.png','/favicon.png',['type' => 'icon']);?>
    <?= $this->Html->css('bootstrap.min.css') ?>
	<?= $this->Html->css('font-awesome.min.css') ?>
	<?= $this->Html->css('app.css') ?>
	<?= $this->Html->script('jquery.min.js') ?>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
   </head>
<body>
<?= $this->fetch('content') ?>

 
<footer>&#169; 2017 Faiz al Mawaid al Burhaniyah (Faizchicago.com)</footer>
  <?= $this->Html->script('bootstrap.min.js') ?>
   <?= $this->Html->script('jquery.sliderPro.min.js') ?>
   <?= $this->Html->script('app.js') ?>
   <?= $this->Html->script('jquery.validate.min.js') ?>
   <?= $this->Html->script('additional-methods.min.js') ?>
   <?= $this->Html->script('custom-validation-methods.js') ?>
  
</body>
</html>
