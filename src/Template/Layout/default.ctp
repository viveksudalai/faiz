<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Faizchicago';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
     
    
    <?= $this->Html->meta('favicon.png','/favicon.png',['type' => 'icon']);?>
    <?= $this->Html->script('jquery-1.12.4.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('jquery-ui.js') ?>
    <?= $this->Html->script('jquery.dataTables.min.js') ?>
    <?= $this->Html->script('dataTables.bootstrap.min.js') ?>
    <?= $this->Html->script('dataTables.buttons.js') ?>
    <!-- $this->Html->script('buttons.flash.js') -->
    <?= $this->Html->script('jszip.min.js') ?>
    <?= $this->Html->script('pdfmake.min.js') ?>
    <?= $this->Html->script('vfs_fonts.js') ?>
    <?= $this->Html->script('buttons.html5.js') ?>
   
    <?= $this->Html->script('jquery.validate.min.js') ?>
    <?= $this->Html->script('additional-methods.min.js') ?>
    <?= $this->Html->script('custom-validation-methods.js') ?>
    <?= $this->Html->script('bootstrap-colorpicker.min.js') ?>
 
    <?php //$this->Html->css('base.css') ;
    ?>
    <?php //$this->Html->css('cake.css') ;
    ?>
	<?= $this->Html->css('bootstrap.css') ?>
	<?= $this->Html->css('jquery-ui.css') ?>
	<?= $this->Html->css('font-awesome.min.css') ?>
	<?= $this->Html->css('dataTables.bootstrap.min.css') ?>
	<?= $this->Html->css('jquery.dataTables.css') ?>
	<?= $this->Html->css('buttons.dataTables.css') ?>
	<?= $this->Html->css('style.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->css('jquery-ui.css') ?>
    <?= $this->Html->css('bootstrap-colorpicker.min.css') ?>
	</head>
<body>
<?php   
	 
	  $session = $this->request->session();
      $user_data = $session->read('Auth.User');
 ?>
<div class="main-header">
  <div class="container"> 
    <!-- Static navbar -->
    <div class="navbar navbar-default" role="navigation">
      <div class="navbar-header mid-header col-lg-12">
     
        <div class="logo pull-left"> 
        	<?php 
        		/*echo $this->Html->link(
    			     $this->Html->image('logo.jpg', ['alt' => 'Faizchicago', 'class' => 'img-responsive']),
    			 	 '/',
   				 	 ['escapeTitle' => false, 'title' => 'Faizchicago']
					);*/
        	echo $this->Html->image('logo.jpg', ['alt' => 'Faizchicago', 'class' => 'img-responsive'])
			?>
        </div>
        <div class="logo-2 pull-right"> 
        	<?php 
        	/*	echo $this->Html->link(
    			     $this->Html->image('faiz.png', ['alt' => 'Faizchicago', 'class' => 'img-responsive']),
    			 	 '/',
   				 	 ['escapeTitle' => false, 'title' => 'Faizchicago']
					);*/
        	echo $this->Html->image('faiz.png', ['alt' => 'Faizchicago', 'class' => 'img-responsive']);
			?>
        </div>
       </div>
      <div class="col-lg-12" style="clear:both">
        <div class="container header-mid">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <?php if($loggedIn) : ?>
             	 <h3>Welcome "<?= $user_data['first_name']." ".$user_data['middle_name']." ".$user_data['last_name'] ;?> " </h3>
             <?php endif;?>  	 
            </div>
            <div class="col-lg-6 col-md-12 pull-right">
             <?php if($loggedIn && $user_data['user_role'] <= 2) : ?>
	               <ul class="nav navbar-right top-nav">
	                  <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="fa fa-user"></i>  My Settings <b class="caret"></b></a>
	                    <ul class="dropdown-menu">
	                        <?php if($loggedIn) : ?>
	               				<li><?= $this->Html->link('My Account', ['prefix' => 'backend','controller' => 'users', 'action' => 'view',$user_data['id']]);?></li>
	               				<li><?= $this->Html->link('Change Password', ['prefix' => 'backend','controller' => 'users', 'action' => 'updatePassword']);?></li>
	                            <li><?= $this->Html->link('LogOut', ['prefix' => 'backend','controller' => 'users', 'action' => 'logout']);?></li> 
	           				<?php endif;?> 
	                   </ul>
	                  </li>
	                </ul>
	             <?php endif;?>  
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12" style="clear:both">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
          <span class="sr-only">Toggle navigation</span> 
          <span class="icon-bar"></span> 
          <span class="icon-bar"></span> 
          <span class="icon-bar"></span> 
         </button>
      </div>
    </div>
  </div>
</div>
	<!-- /container -->
	
	<!-- menu -->
	 
	<?php if($user_data['user_role'] <= 2){?>
	    <?= $this->Element('admin-menu') ?>   
	<?php } else { ?>
		<?php //$this->Element('toplinks-menu');
		 ?>   
	<?php } ?> 
    <!-- /menu -->
    
    <div class="col-lg-12">
    	 <?= $this->Flash->render() ?>
    </div> 
   
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <div class="footer">
      <div class="container">
        <p class="text-center">&copy; 2017 faizchicago.com All Rights Reserved</p>
      </div>
    </div>
    <footer>
    <script>
$(document).ready(function(){
 //   $('[data-toggle="tooltip"]').tooltip();
});
</script>
    </footer>
    
</body>
</html>
