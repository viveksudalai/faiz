<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Faizchicago';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
     
    
    <?= $this->Html->meta('favicon.png','/favicon.png',['type' => 'icon']);?>
    <?= $this->Html->css('bootstrap.min.css') ?>
	<?= $this->Html->css('font-awesome.min.css') ?>
	<?= $this->Html->script('jquery-2.1.3.min.js') ?>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
   </head>
<body>
<?php   
	 $session = $this->request->session();
     $user_data = $session->read('Auth.User');
 ?>
	<header>
		<div class="row">
			<div class="col-md-8 col-sm 8 col-xs-12">
			<?php echo $this->Html->link(
    			     $this->Html->image('logo-1.png', ['alt' => 'Faizchicago', 'class' => '']),
    			 	 ['prefix' => 'customer', 'controller' => 'Users', 'action' => 'home'],
   				 	 ['escapeTitle' => false, 'title' => 'Faizchicago']
					);
		    ?>
		  </div>
		  <?php if($loggedIn) : ?>
			<div class="col-md-4 col-xs-12 col-sm-4" style="padding-top: 20px;">
				<span> 
					 Welcome "<?= $user_data['first_name']." ".$user_data['middle_name']." ".$user_data['last_name'] ;?> " 
                 </span>
				<?php echo $this->Html->link(
    			     $this->Html->image('edit_user.jpg', ['alt' => 'Edit Profile', 'class' => '']),
    			 	 ['prefix' => 'customer', 'controller' => 'Users', 'action' => 'edit'],
   				 	 ['escapeTitle' => false, 'title' => 'Edit Profile']
					);
				?>
				<?= $this->Html->link('LogOut', ['prefix' => 'customer','controller' => 'users', 'action' => 'logout'], ['class' => 'btn btn-custom hidden-xs']);?>
				<button class="btn btn-logout hidden-md hidden-sm hidden-lg"><i class="fa fa-sign-out" aria-hidden="true"></i></button>
			 </div>
		 <?php endif;?>  
		</div>
	</header>
	<?php if($loggedIn) : ?>
	<?= $this->Element('customer-menu') ?> 
 	<?php endif;?>  
 	<div class="row">
 	<div class="col-lg-5 col-lg-offset-3">
    	 <?= $this->Flash->render() ?>
    </div> 
    </div>
    <?= $this->fetch('content') ?>
 	 
		   
 
<footer>&#169;2017 Faiz al Mawaid al Burhaniyah (Faizchicago.com)</footer>
<?php  // echo $this->Html->css('base.css') ?>
<?= $this->Html->css('fullcalendar.min.css') ?>
<?= $this->Html->script('moment.min.js') ?>
<?= $this->Html->script('fullcalendar.min.js') ?>
<?= $this->Html->css('dataTables.bootstrap.min.css') ?>
<?= $this->Html->css('jquery.dataTables.css') ?>
<?= $this->Html->css('bootstrap-colorpicker.min.css') ?>
<?= $this->Html->css('jquery-ui.css') ?>
<?= $this->Html->css('app.css') ?>
<?= $this->Html->script('bootstrap.min.js') ?>
<?= $this->Html->script('bootstrap-rating.min.js') ?>
<?= $this->Html->script('app.js') ?>
<?= $this->Html->script('jquery-ui.js') ?>
<?= $this->Html->script('jquery.dataTables.min.js') ?>
<?= $this->Html->script('dataTables.bootstrap.min.js') ?>
<?= $this->Html->script('jquery.validate.min.js') ?>
<?= $this->Html->script('additional-methods.min.js') ?>
<?= $this->Html->script('custom-validation-methods.js') ?>
   
	
</body>
</html>
