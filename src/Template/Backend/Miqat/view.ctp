	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		 <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Miqat', 'action' => 'index']);?>
                </div>
            </div><!-- /.box-header -->
 
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		    <h3> <?= __('View Miqat') ?></h3> 
		   <table id="miqat" class="table table-striped table-bordered" cellspacing="0" width="100%">
		         <tr>
		            <th><?= __('Name') ?></th>
		            <td><?= h($miqat->event_title) ?></td>
		        </tr>
		        <!--<tr>
		            <th><?= __('Id') ?></th>
		            <td><?= $this->Number->format($miqat->id) ?></td>
		        </tr>-->
		        <tr>
		            <th><?= __('Start Date') ?></th>
		            <td><?= h($miqat->start_date->format('l Y/m/d')) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('End Date') ?></th>
		            <td><?= h($miqat->end_date->format('l Y/m/d'))?></td>
		        </tr>
		         <tr>
		            <th><?= __('Details') ?></th>
		            <td><?= $this->Text->autoParagraph(h($miqat->details)); ?></td>
		        </tr>
		        
		         <tr>
						            <th><?= __('Status') ?></th>
						            <td><?php switch ($miqat->status) {
			          						 
			          							case '1': echo 'Active'; break;
			          							case '0': echo 'Inactive';break;
			          					}
			         				 ?></td>
						        </tr>
		        <tr>
		            <th><?= __('Created') ?></th>
		            <td><?= h($miqat->created->format('l Y/m/d'))?></td>
		        </tr>
		        <tr>
		            <th><?= __('Modified') ?></th>
		            <td><?= h($miqat->modified->format('l Y/m/d'))?></td>
		        </tr>
		       
		    </table>
		</div>
	</div>
</div>
  
 
