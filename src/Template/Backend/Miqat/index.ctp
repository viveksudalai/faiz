 
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
	              <div class="box-header">
	                  <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">All Miqat</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			           <?= $this->Html->link('Add Miqat', ['controller' => 'miqat', 'action' => 'add'], ['class' => 'btn btn-primary center-block pull-right']);?>
			        </div>
			        </div>
	                   
	                </div><!-- /.box-header --> 
              	<div class="box-body table-responsive">
                	 <table id="allMiqat" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr class="skyblue-bg">
				              	<th><?= 'id' ?></th>
				                <th><?= 'Event Title' ?></th>
				                <th><?= 'details' ?></th>
				                <th><?= 'start date'?></th>
				                <th><?= 'end date' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php foreach ($miqat as $miqat): ?>
				            <tr>
				            	<td><?= h($miqat->id) ?></td>
				                <td><?= h(ucfirst($miqat->event_title)) ?></td>
				                <td><?= h(ucfirst($miqat->details)) ?></td>
				                <td><?= h($miqat->start_date->format('l m/d/Y')) ?></td>
				                <td><?= h($miqat->end_date->format('l m/d/Y')) ?></td>
				                <td class="actions">
				                    <ul class="action">
				                             <!--<li><?php //$this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-envelope', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Resend Email Notification', 'aria-hidden' => 'true']), ['action' => 'view', $miqat->id], ['escape' => false]) ?></li>-->
				                                <!--li><?php //$this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['action' => 'view', $miqat->id], ['escape' => false]) ?></li-->
							                	<?php if(date('Y-m-d') <= date('Y-m-d', strtotime($miqat->end_date)) ){  ?>
								                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $miqat->id], ['escape' => false]) ?></li>
				                	     		<?php } ?>
				                	     		<!--<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $miqat->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $miqat->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>-->
			                  				</ul>
				                    </td>
				            </tr>
				            <?php endforeach; ?>
				        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#allMiqat').DataTable({
	   	 "bAutoWidth": true , 
	   	"ordering": false,
	   	 "aoColumnDefs": [
         {  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
         {  "aTargets": [5], "bSearchable": false, "bSortable": false},
        ]
	   	 });
	} );
</script>
  