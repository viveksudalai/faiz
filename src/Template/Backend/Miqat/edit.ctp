   <?php   
     $sdate = date('Y-m-d', strtotime($miqat->start_date)); 
     $edate = date('Y-m-d', strtotime($miqat->end_date)); 
   ?>
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   	 <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Miqat', 'action' => 'index']);?>
			    </div>
            </div><!-- /.box-header -->
           
             <div class="row">
			  <div class="col-md-6">
			  
					<div class="box box-primary col-md-offset-1">    
					
						<div class="box-body">
						
					 
        			<!-- <div class="pad-4">
          				<div class="left">Adding/modifying Miqat Events needs to be completed at least 48 hours in advance</div>
        			</div>-->
      		 	 
							  <?= $this->Form->create($miqat) ?>
							    <fieldset>
							        <legend><?= __('Edit Miqat') ?></legend>
									     <?php
									            echo $this->Form->input('event_title', ['class'=> 'form-control']);
									            echo $this->Form->input('start_date', ['value' => $sdate , 'class'=>'datepicker form-control','type'=> 'text']);
									            echo $this->Form->input('end_date', ['value' => $edate , 'class'=>'datepicker form-control','type'=> 'text']);
									            echo $this->Ck->input('details',[ 'class'=> 'form-control']);
									            echo $this->Form->input('status', ['class'=>'form-control','type'=> 'hidden', 'value' => '1']);
									          /*  echo '</div>';
	       										$attributes['value'] =   $miqat->status; 
	       										echo "<div class='input'>";
	       										echo $this->Form->label('miqat.Status');
	       										echo "&nbsp;&nbsp;";
								            	echo $this->Form->radio('status', [ 
									            ['value' => '0', 'text' => 'Inactive', 'class'=> ''],
		       									['value' => '1', 'text' => 'Active', 'class'=> '']
	       									    ], $attributes);
	       									    echo '</div>'; */
									        ?>
								 </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
		
		
  <script>
  
  $(function() {
			
				var dates = $("#start-date,#end-date").datepicker({
                                        minDate:'-0y',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			}); 
    $(document).ready(function() {
	  $("#miqat").validate({
	     rules: {
	         	name: {required:true},
         		details: {required:true},
         		start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			name: { 
  			 				required: "Miqat Name is required."
  						  },
  					details: { 
  			 				required: "Miqat details is required." 
  						  }, 
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 
});
</script>
  