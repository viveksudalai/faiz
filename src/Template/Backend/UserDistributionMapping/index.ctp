<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-xs-12">
           <h2><?= __('User Distribution Mapping') ?></h2>
        </div>
        <div class="col-lg-8 col-md-12 col-xs-12">
           <!-- <a class="btn btn-primary center-block pull-right" data-toggle="tooltip" href="add">
            <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add New User</span></a>-->
           <?= $this->Html->link(__('New User Distribution Mapping'), ['action' => 'add']) ?>
        </div>
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allUserDistributionMapping" class="table table-striped table-bordered" cellspacing="0" width="100%">
				     	<thead>
				            <tr>
				                <th><?= 'id' ?></th>
				                <th><?= 'user id' ?></th>
				                <th><?= 'distribution id' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php foreach ($userDistributionMapping as $userDistributionMapping): ?>
				            <tr>
				                <td><?= $this->Number->format($userDistributionMapping->id) ?></td>
				                <td><?= $userDistributionMapping->has('user') ? $this->Html->link($userDistributionMapping->user->id, ['controller' => 'Users', 'action' => 'view', $userDistributionMapping->user->id]) : '' ?></td>
				                <td><?= $userDistributionMapping->has('distribution_center') ? $this->Html->link($userDistributionMapping->distribution_center->id, ['controller' => 'DistributionCenter', 'action' => 'view', $userDistributionMapping->distribution_center->id]) : '' ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['action' => 'view', $userDistributionMapping->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userDistributionMapping->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userDistributionMapping->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userDistributionMapping->id)]) ?>
				                </td>
				            </tr>
			            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	 $('#allUserDistributionMapping').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 3 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</script>
  