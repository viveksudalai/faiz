 
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<a href="/backend/users/index" class="btn btn-primary" id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							 <?= $this->Form->create($userDistributionMapping) ?>
						    <fieldset>
						        <legend><?= __('Edit User Distribution Mapping') ?></legend>
						        <?php
						            echo $this->Form->input('user_id', ['options' => $users, 'class'=> 'form-control']);
						            echo $this->Form->input('distribution_id', ['options' => $distributionCenter, 'class'=> 'form-control']);
						        ?>
						    </fieldset>
						     <?= $this->Form->button(__('Submit')) ?>
							 <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 
  