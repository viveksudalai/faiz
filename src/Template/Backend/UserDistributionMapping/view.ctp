<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
	</div>
</div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
    		<h3> <?= __('User Distribution Center Info') ?></h3> 
			  <table id="userDistributionMapping" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<tr>
		            <th><?= __('Mapping Id') ?></th>
		            <td><?= $this->Number->format($userDistributionMapping->id) ?></td>
		        </tr>		     
		        <tr>
		            <th><?= __('User') ?></th>
		            <td><?= $userDistributionMapping->has('user') ? $this->Html->link($userDistributionMapping->user->id, ['controller' => 'Users', 'action' => 'view', $userDistributionMapping->user->id]) : '' ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Distribution Center') ?></th>
		            <td><?= $userDistributionMapping->has('distribution_center') ? $this->Html->link($userDistributionMapping->distribution_center->id, ['controller' => 'DistributionCenter', 'action' => 'view', $userDistributionMapping->distribution_center->id]) : '' ?></td>
		        </tr>
			</table>
		</div>
	</div>
</div>	
 <script>  
     $(document).ready(function() {
   	 $('#userDistributionMapping').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 3 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</script>