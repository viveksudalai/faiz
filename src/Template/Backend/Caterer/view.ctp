<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
	</div>
</div><!-- /.box-header -->
<div class="col-lg-12 col-md-12 col-xs-12">
	<div class="box box-primary">    
		<div class="box-body"> 
  
    <h3><?= h($caterer->name) ?></h3>
    <table id="caterer" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($caterer->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($caterer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($caterer->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($caterer->modified) ?></td>
        </tr>
    </table>
	    <div class="related">
		        <h4><?= __('Related Thaali') ?></h4>
		        <?php if (!empty($caterer->thaali)): ?>
		       <table id="CatererThaali" class="table table-striped table-bordered" cellspacing="0" width="100%">
		            <tr>
		                <th><?= __('Id') ?></th>
		                <th><?= __('Name') ?></th>
		                <th><?= __('Caterer Id') ?></th>
		                <th><?= __('Menu Date') ?></th>
		                <th><?= __('Menu Item') ?></th>
		                <th><?= __('Created') ?></th>
		                <th><?= __('Modified') ?></th>
		                <th class="actions"><?= __('Actions') ?></th>
		            </tr>
		            <?php foreach ($caterer->thaali as $thaali): ?>
		            <tr>
		                <td><?= h($thaali->id) ?></td>
		                <td><?= h($thaali->name) ?></td>
		                <td><?= h($thaali->caterer_id) ?></td>
		                <td><?= h($thaali->menu_date) ?></td>
		                <td><?= h($thaali->menu_item) ?></td>
		                <td><?= h($thaali->created) ?></td>
		                <td><?= h($thaali->modified) ?></td>
		                <td class="actions">
		                    <?= $this->Html->link(__('View'), ['controller' => 'Thaali', 'action' => 'view', $thaali->id]) ?>
		                    <?= $this->Html->link(__('Edit'), ['controller' => 'Thaali', 'action' => 'edit', $thaali->id]) ?>
		                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Thaali', 'action' => 'delete', $thaali->id], ['confirm' => __('Are you sure you want to delete # {0}?', $thaali->id)]) ?>
		                </td>
		            </tr>
		            <?php endforeach; ?>
		        </table>
		        <?php endif; ?>
		    </div>
		</div>
	</div>
</div>