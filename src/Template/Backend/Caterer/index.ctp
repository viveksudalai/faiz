 
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">All Caterer</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			           <?= $this->Html->link('Add Caterer', ['controller' => 'caterer', 'action' => 'add'], ['class' => 'btn btn-primary center-block pull-right']);?>
			        </div>
			        </div>
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allCaterer" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr  class="skyblue-bg">
				            	<th><?= 'id' ?></th>
								<th><?= 'name' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				            </tr>
			        	</thead>
			        <tbody>
						<?php foreach ($caterer as $caterer): ?>
			            <tr>
			            	<td><?= h($caterer->id) ?></td>
			                <td><?= h($caterer->name) ?></td>
			                <td class="actions">
			                    <ul class="action">
			                    	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $caterer->id], ['escape' => false]) ?></li>
				                	     		<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $caterer->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $caterer->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>
			                    </ul>                             
			                                                 
			                </td>
			            </tr>
			            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

  <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#allCaterer').DataTable({
	   	 "bAutoWidth": false , 
	   	  "order": [[ 0, "desc" ]],
         "aoColumnDefs": [
         { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
         { "sWidth": "70%", "aTargets": [1]},
         { "sWidth": "30%", "aTargets": [2], "bSearchable": false, "bSortable": false},
        ]
	   	 });
	} );
</script>
 