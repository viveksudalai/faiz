	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'dashboard', 'action' => 'index']);?>
    </div>
            </div><!-- /.box-header -->  
		<div class="">
         
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create('', ['id' => 'notification', 'name' => 'notification']) ?>
							    <fieldset>
							        <legend><?= __('Email Notifications') ?></legend>
							         <?php
							           echo $this->Form->input('email_type', ['options' => $emailType,'class'=> 'form-control']);
							           echo "<div class='emails'>";
							           echo $this->Form->input('emails', ['class'=> 'form-control']);
							           echo "</div>";
							         //  echo "<div class='input tdate'>";
							         //  echo $this->Form->input('thaali_date', ['class'=>'datepicker form-control','type'=> 'hidden']);
							         //  echo "</div>";
							           echo $this->Form->input('email_sub', ['class'=> 'form-control']);
							           echo $this->Ck->input('email_body', ['type' => 'textarea','class'=> 'form-control']);
							         ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
		
 <script>  
 /*$(function() {
		
		var dates = $("#thaali-date").datepicker({
                             minDate:'-0y',
                       // 	maxDate:'+0d',
                       dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "menu-date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
			
		});
	});*/
							         
 $(document).ready(function() {
	  $('.emails').hide();
	  
	  $( '#email-type' ).change(function() {
			var type = $( this ).val(); 
			if (type != 'Manual') {
				$('.emails').hide();
				//$('.tdate').show();
				
			}
			else { 
				$('.emails').show();
				//$('.tdate').hide();
			 }
 	  });
		
	  $("#notification").validate({
	     rules: {
	    	 email_type: {required:true},
	    	// thaali_date: {required:true},
	    	 email_sub: {required:true},
	    	 email_body: {required:true}
       },
        messages: {
        			email_type: { required: "Email Type is required." },
        		//	thaali_date: {required: "Thaali date is required."},
       	    	 	email_sub: { required: "Email subject is required."},
       	    	 	email_body:{ required: "Email body is required."}
  		 }
	}); 
});
</script>
  