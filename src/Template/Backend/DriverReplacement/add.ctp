
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<a href="/backend/users/index" class="btn btn-primary" id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($driverReplacement) ?>
							    <fieldset>
							        <legend><?= __('Add Driver Replacement') ?></legend>
									     <?php
									            echo $this->Form->input('driver_id', [ 'class'=> 'form-control']);
									            echo $this->Form->input('replace_driver_id', ['options' => $users, 'class'=> 'form-control']);
									            echo $this->Form->input('from_date',[ 'class'=> 'form-control']);
									            echo $this->Form->input('to_date',[ 'class'=> 'form-control']);
									            echo $this->Form->input('status',[ 'class'=> 'form-control']);
									        ?>
								 </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 