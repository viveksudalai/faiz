<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-xs-12">
           <h2><?= __('Driver Replacement') ?></h2>
        </div>
        <div class="col-lg-8 col-md-12 col-xs-12">
           <!-- <a class="btn btn-primary center-block pull-right" data-toggle="tooltip" href="add">
            <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add New User</span></a>-->
              <li><?= $this->Html->link(__('New Driver Replacement'), ['action' => 'add']) ?></li>
        </div>
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allDriverReplacement" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr>
				                <th><?= 'id' ?></th>
				                <th><?= 'driver id' ?></th>
				                <th><?= 'replace driver_id' ?></th>
				                <th><?= 'from date' ?></th>
				                <th><?= 'to date' ?></th>
				                <th><?= 'created' ?></th>
				                <th><?= 'modified' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php foreach ($driverReplacement as $driverReplacement): ?>
				            <tr>
				                <td><?= $this->Number->format($driverReplacement->id) ?></td>
				                <td><?= $this->Number->format($driverReplacement->driver_id) ?></td>
				                <td><?= $driverReplacement->has('user') ? $this->Html->link($driverReplacement->user->id, ['controller' => 'Users', 'action' => 'view', $driverReplacement->user->id]) : '' ?></td>
				                <td><?= h($driverReplacement->from_date) ?></td>
				                <td><?= h($driverReplacement->to_date) ?></td>
				                <td><?= h($driverReplacement->created) ?></td>
				                <td><?= h($driverReplacement->modified) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['action' => 'view', $driverReplacement->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $driverReplacement->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $driverReplacement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverReplacement->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	 $('#allDriverReplacement').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 7 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</
	  