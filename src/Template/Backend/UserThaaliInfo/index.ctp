<div class="userThaaliInfo index large-9 medium-8 columns content">
    <h3><?= __('User Thaali Info') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('active_days') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userThaaliInfo as $userThaaliInfo): ?>
            <tr>
                <td><?= $this->Number->format($userThaaliInfo->id) ?></td>
                <td><?= $userThaaliInfo->has('user') ? $this->Html->link($userThaaliInfo->user->first_name, ['controller' => 'Users', 'action' => 'view', $userThaaliInfo->user->id]) : '' ?></td>
                <td><?= h($userThaaliInfo->active_days) ?></td>
                <td><?= h($userThaaliInfo->created) ?></td>
                <td><?= h($userThaaliInfo->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userThaaliInfo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userThaaliInfo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userThaaliInfo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userThaaliInfo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
