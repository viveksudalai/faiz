 <div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<a href="/backend/role/index" class="btn btn-primary" data-placement='top' title= 'Back' data-toggle="tooltip" id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($role, ['id' => 'role', 'name' => 'role']) ?>
							    <fieldset>
							        <legend><?= __('Add Role') ?></legend>
							         <?php
								            echo $this->Form->input('name', [ 'class'=> 'form-control']);
								            echo '</div>';
	       										$attributes['value'] =   $role->status; 
	       										echo "<div class='input'>";
	       										echo $this->Form->label('role.Status');
	       										echo "&nbsp;&nbsp;";
								            	echo $this->Form->radio('status', [ 
									            ['value' => '0', 'text' => 'Inactive', 'class'=> ''],
		       									['value' => '1', 'text' => 'Active', 'class'=> '', 'checked' => 'checked']
	       									    ], $attributes);
	       									    echo '</div>'; 
								      ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
<script>
  $(document).ready(function() {
	  $("#role").validate({
	     rules: {
	         	name: {required:true}
       },
        messages: {
        			name: { 
  			 				required: "Role Name is required."
  						  }
  		 }
	}); 
});
</script>
 