<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-xs-12">
           <h2><?= __('All Roles') ?></h2>
        </div>
     
        <div class="col-lg-8 col-md-12 col-xs-12">
            <a class="btn btn-primary center-block pull-right" style="margin-top: 10px;display:none;"   data-placement='top' title= 'Add New Role' data-toggle="tooltip"  href="/backend/role/add">
            <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add New Role</span></a>
        </div>
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allRole" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr class="skyblue-bg">
				                <th><?= 'name' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
			        <tbody>
						<?php foreach ($role as $role): ?>
			            <tr>
			                <td><?= h($role->name) ?></td>
			                <td class="actions">
			                <ul class="action">
					                    		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View','aria-hidden' => 'true']), ['controller' => 'role', 'action' => 'view', $role->id], ['escape' => false]) ?></li>
							                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $role->id], ['escape' => false]) ?></li>
				                	     		<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $role->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $role->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>
			                  				</ul>
			                </td>
			            </tr>
			            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

  <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#allRole').DataTable({
	   	 "bAutoWidth": false , 
         "aoColumnDefs": [
         { "sWidth": "70%", "aTargets": [0]},
         { "sWidth": "30%", "aTargets": [1], "bSearchable": false, "bSortable": false},
        ]
	   	 });
	} );
</script>
 