<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Payment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userPayment index large-9 medium-8 columns content">
    <h3><?= __('User Payment') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('amount') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userPayment as $userPayment): ?>
            <tr>
                <td><?= $this->Number->format($userPayment->id) ?></td>
                <td><?= $userPayment->has('user') ? $this->Html->link($userPayment->user->first_name, ['controller' => 'Users', 'action' => 'view', $userPayment->user->id]) : '' ?></td>
                <td><?= $this->Number->format($userPayment->amount) ?></td>
                <td><?= h($userPayment->created) ?></td>
                <td><?= h($userPayment->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userPayment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userPayment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userPayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userPayment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
