<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Vacation Planner'), ['action' => 'edit', $userVacationPlanner->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Vacation Planner'), ['action' => 'delete', $userVacationPlanner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userVacationPlanner->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Vacation Planner'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Vacation Planner'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userVacationPlanner view large-9 medium-8 columns content">
    <h3><?= h($userVacationPlanner->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $userVacationPlanner->has('user') ? $this->Html->link($userVacationPlanner->user->first_name, ['controller' => 'Users', 'action' => 'view', $userVacationPlanner->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($userVacationPlanner->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($userVacationPlanner->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Start Date') ?></th>
            <td><?= h($userVacationPlanner->start_date) ?></td>
        </tr>
        <tr>
            <th><?= __('End Date') ?></th>
            <td><?= h($userVacationPlanner->end_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($userVacationPlanner->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($userVacationPlanner->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Driver Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($userVacationPlanner->driver_notes)); ?>
    </div>
</div>
