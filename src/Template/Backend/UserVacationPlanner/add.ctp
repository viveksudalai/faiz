 


		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
  						<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Users', 'action' => 'index']);?>
					</div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
						<div class="pad-4">
          				 <div class="left">
          				 <?php echo "Adding/modifing vacation information will be considered valid only if the event is scheduled for after ". $min;?>
          				 </div>
        			</div>
							  <?= $this->Form->create($userVacationPlanner, ['id' => 'vacation', 'name' => 'vacation']) ?> 
							     <?php
								            echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]);
								            echo $this->Form->input('start_date', ['class'=>'datepicker form-control','type'=> 'text']);
											echo $this->Form->input('end_date', ['class'=>'datepicker form-control','type'=> 'text']);
											echo $this->Form->input('title', ['class'=> 'form-control']);
											echo $this->Form->input('driver_notes',['class'=> 'form-control']);
										?>
								  <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
  <script>
  
  $(function() {
			
				var dates = $("#start-date, #end-date").datepicker({
                                        minDate:'+2',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#vacation").validate({
	     rules: {
	    	 	driver_notes: {required:true},
	         	title: {required:true},
         		start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			title: { 
  			 				required: "Title Name is required."
  						  },
  					 driver_notes: { 
  			 				required: "Driver notes is required." 
  						  }, 
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 
});
</script>
  