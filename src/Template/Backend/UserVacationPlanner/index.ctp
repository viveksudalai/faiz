<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Vacation Planner'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userVacationPlanner index large-9 medium-8 columns content">
    <h3><?= __('User Vacation Planner') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('start_date') ?></th>
                <th><?= $this->Paginator->sort('end_date') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userVacationPlanner as $userVacationPlanner): ?>
            <tr>
                <td><?= $this->Number->format($userVacationPlanner->id) ?></td>
                <td><?= $userVacationPlanner->has('user') ? $this->Html->link($userVacationPlanner->user->first_name, ['controller' => 'Users', 'action' => 'view', $userVacationPlanner->user->id]) : '' ?></td>
                <td><?= h($userVacationPlanner->title) ?></td>
                <td><?= h($userVacationPlanner->start_date) ?></td>
                <td><?= h($userVacationPlanner->end_date) ?></td>
                <td><?= h($userVacationPlanner->created) ?></td>
                <td><?= h($userVacationPlanner->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userVacationPlanner->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userVacationPlanner->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userVacationPlanner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userVacationPlanner->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
