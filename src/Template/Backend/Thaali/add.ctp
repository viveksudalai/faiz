 
<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		 <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Thaali', 'action' => 'index']);?>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							   <?= $this->Form->create($thaali, ['id' => 'thaali', 'name' => 'thaali']) ?>
							   <fieldset>
							        <legend><?= __('Add Thaali') ?></legend>
							         <?php
							            //echo $this->Form->input('name', [ 'class'=> 'form-control']);
							            echo $this->Form->input('caterer_id', ['options' => $caterer, 'class'=> 'form-control']);
							            echo "<div class='input'>";
							            echo $this->Form->input('menu_date', ['class'=>'datepicker form-control','type'=> 'text']);
							            echo "</div>";
							            echo $this->Form->input('menu_item', [ 'class'=> 'form-control']);
							        ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 <script>
$(function() {
			
				var dates = $("#menu-date").datepicker({
                                        minDate:'-0y',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "menu-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 $(document).ready(function() {
	  $("#thaali").validate({
	     rules: {
	          
         		caterer_id: {required:true},
         		menu_date: {required:true,date: true},
         		menu_item: {required:true}
        },
        messages: {
        			 
  					caterer_id: { 
  			 				required: "Caterer name is required." 
  						  }, 
  					menu_date : { 
  			 				required: "Menu date is required." , date:"Enter valid date"
  						  }, 
  					menu_item : { 
  			 				required: "Menu item is required." 
  						  } 
		}
	}); 
});
</script>