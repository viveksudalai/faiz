 
   <!-- Main content -->
        <section class="content">
          <div class="row">
             <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div class="row">
                 <div class="col-lg-8 col-md-12 col-xs-12">
                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">Scheduled Thaalis</h2>
       			</div>
		        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
		            <?= $this->Html->link('Add Thaali', ['controller' => 'thaali', 'action' => 'add'], ['class' => 'btn btn-primary center-block pull-right']);?>
		        </div>
		        </div>
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allThaali" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr class="skyblue-bg">
				            	<th><?= 'id' ?></th>
				                <th><?= 'menu date' ?></th>
				                <!--<th><?= 'menu name' ?></th>-->
				                <th><?= 'caterer' ?></th>
				                <th><?= 'item' ?></th>
				                <th>Pick Up</th>
				                <th>Delivery</th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
			        	</thead>
			        	<tbody>
							<?php foreach ($thaaliDelivery as $thaali): ?>
				              <tr>
				            	<td><?= h($thaali['id']) ?></td>
				                <td><?= h($thaali['menu_date']) ?></td>
				                <td><?= h($thaali['name']) ?></td>
				                <td><?= h($thaali['menu_item']) ?></td>
				                <td><?= h($thaali['pickup'] > 0 ? $thaali['pickup'] : '0')  ?></td>
				                <td><?= h($thaali['delivery']>0 ? $thaali['delivery'] : '0')   ?></td>
				                <td class="actions">
				               		<ul class="action">
				               			<!--<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['action' => 'view', $thaali['id']], ['escape' => false]) ?></li>-->
				               			<?php if(date('Y-m-d') <= date('Y-m-d', strtotime($thaali['menu_date'])) ){  ?>
				               				 <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $thaali['id']], ['escape' => false]) ?></li>
				                	     	 <li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
                                                 ['action' => 'delete', $thaali['id']],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $thaali['id'])],
                                                 ['class' => 'btn btn-mini']); ?></li>
                                        <?php } ?>         
				               		</ul>	 
				                </td>
				            </tr>
				            <?php endforeach; ?>
			        	</tbody>
			    	</table>
   				  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
 <script>  
     $(document).ready(function() {
   	 $('#allThaali').DataTable({
   	   	"bAutoWidth": true , 
	   	  "order": [[ 0, "desc" ]],
	   	  "ordering": false,
	   	  columnDefs: [
            {   "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
            { "aTargets": [6], "bVisible": true,"bSearchable": false, "bSortable": false }
        ],
        fixedColumns: true,
        "bSort" : true
   	 });
   	 
   	 /*$('#scheduledThaali').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
        { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
            { sWidth: '20%', targets: 1}
        ],
        fixedColumns: true,
        "bSort" : false, 
         "lengthMenu": [[7, 14, 21, 28 -1], [7, 14, 21, 28, "All"]]
   	 });*/
   	 
	} );
	
	
</script>	
 
 
  