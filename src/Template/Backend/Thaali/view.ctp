<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
    	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Thaali', 'action' => 'index']);?>
	</div>
</div><!--  box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		    <h3> <?= __('View Thaali') ?></h3> 
		    <table id="miqat" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <!-- <tr>
					 <th><?= __('Name') ?></th>
					 <td><?= h($thaali->name) ?></td>
				</tr>-->
				<tr>
					<th><?= __('Menu Item') ?></th>
					<td><?= h($thaali->menu_item) ?></td>
				</tr>
				<tr>
					<th><?= __('Caterer') ?></th>
					<td><?= $thaali->has('caterer') ? $this->Html->link($thaali->caterer->name, ['controller' => 'Caterer', 'action' => 'view', $thaali->caterer->id]) : '' ?></td>
				</tr>
				<!--
				<tr>
					<th><?= __('Id') ?></th>
					<td><?= $this->Number->format($thaali->id) ?></td>
			    </tr>
			    -->
			    <tr>
			        <th><?= __('Menu Date') ?></th>
			        <td><?= h($thaali->menu_date->format('l Y/m/d')) ?></td>
			    </tr>
			<!-- <tr>
			        <th><?= __('Created') ?></th>
			        <td><?= h($thaali->created->format('l Y/m/d')) ?></td>
			    </tr>
			    <tr>
				    <th><?= __('Modified') ?></th>
				    <td><?= h($thaali->modified->format('l Y/m/d')) ?></td>
				</tr>
			-->
		    </table>					           
		</div>
	</div>
</div>
  
 

            