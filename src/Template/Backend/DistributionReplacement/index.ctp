<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-xs-12">
           <h2><?= __('All Distribution Center') ?></h2>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-6">
          <a class="btn btn-primary center-block pull-right" data-placement='top' title= 'Add New Distribution Center' data-toggle="tooltip"  href="/backend/distribution_replacement/add">
          <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> New Distribution Replacement</span></a>
        </div>
       
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="distributionReplacement" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr>
				               <tr>
					                <th><?=  'id'?></th>
					                <th><?= 'distribution name' ?></th>
					                <th><?= 'replaced distribution name' ?></th>
					                <th><?= 'from date' ?></th>
					                <th><?= 'to date' ?></th>
					                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
						        <tbody>
						       		<?php foreach ($distributionReplacement as $distribution): ?>
							            <tr>
							                <td><?= $this->Number->format($distribution->id) ?></td>
							                <td><?= h($distribution->distribution_center->name) ?></td>
							                <td><?= h($distribution->distribution_center1->name) ?></td>
							                <td><?= h($distribution->from_date->format('l Y/m/d')) ?></td>
							                <td><?= h($distribution->to_date->format('l Y/m/d')) ?></td> 
							                 
							                <td class="actions">
							                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['action' => 'view', $distribution->id], ['escape' => false]) ?>
				                				<?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $distribution->id], ['escape' => false]) ?></li>
				                				 <?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete']). "",
                                                 ['action' => 'delete', $distribution->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $distribution->id)],
                                                 ['class' => 'btn btn-mini']); ?>
							                     </td>
							            </tr>
							            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	  $('#distributionReplacement').DataTable({
	   	 
	   	  "bAutoWidth": false , 
          "order": [[ 0, "desc" ]],
         "aoColumnDefs": [
         { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
         { "sWidth": "20%", "aTargets": [1]},
         { "sWidth": "30%", "aTargets": [2]},
         { "sWidth": "15%", "aTargets": [3], "bSearchable": true, "bSortable": true},
         { "sWidth": "15%", "aTargets": [4], "bSearchable": true},
          
         { "sWidth": "20%", "aTargets": [5], "bSearchable": false, "bSortable": false},
          ]
	   	 });
	 } );
</script>
 
  