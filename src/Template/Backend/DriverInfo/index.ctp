<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
           <h2><?= __('Driver Info') ?></h2>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12">
           <!-- <a class="btn btn-primary center-block pull-right" data-toggle="tooltip" href="add">
            <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add New User</span></a>-->
            <li><?= $this->Html->link(__('New Driver Info'), ['action' => 'add']) ?></li>
        </div>
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allDrivers" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
					            <tr>
					                <th><?= 'id' ?></th>
					                <th><?= 'user id' ?></th>
					                <th><?= 'insurance provider' ?></th>
					                <th><?= 'insurance policy_no' ?></th>
					                <th><?= 'insurance expiry_date' ?></th>
					                <th><?= 'license number' ?></th>
					                <th><?= 'license expiry_date' ?></th>
					                <th><?= 'vehicle make' ?></th>
					                <th><?= 'vehicle model' ?></th>
					                <th><?= 'vehicle year' ?></th>
					                <th class="actions"><?= __('Actions') ?></th>
					            </tr>
					        </thead>
					        <tbody>
					            <?php foreach ($driverInfo as $driverInfo): ?>
					            <tr>
					                <td><?= $this->Number->format($driverInfo->id) ?></td>
					                <td><?= $driverInfo->has('user') ? $this->Html->link($driverInfo->user->id, ['controller' => 'Users', 'action' => 'view', $driverInfo->user->id]) : '' ?></td>
					                <td><?= h($driverInfo->insurance_provider) ?></td>
					                <td><?= h($driverInfo->insurance_policy_no) ?></td>
					                <td><?= h($driverInfo->insurance_expiry_date) ?></td>
					                <td><?= h($driverInfo->license_number) ?></td>
					                <td><?= h($driverInfo->license_expiry_date) ?></td>
					                <td><?= h($driverInfo->vehicle_make) ?></td>
					                <td><?= h($driverInfo->vehicle_model) ?></td>
					                <td><?= h($driverInfo->vehicle_year) ?></td>
					                <td class="actions">
					                    <?= $this->Html->link(__('View'), ['action' => 'view', $driverInfo->id]) ?>
					                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $driverInfo->id]) ?>
					                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $driverInfo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverInfo->id)]) ?>
					                </td>
					            </tr>
					            <?php endforeach; ?>
        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	 $('#allDrivers').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 10 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</
	  
	 