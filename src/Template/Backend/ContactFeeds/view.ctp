<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contact Feed'), ['action' => 'edit', $contactFeed->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contact Feed'), ['action' => 'delete', $contactFeed->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactFeed->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contact Feeds'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact Feed'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contactFeeds view large-9 medium-8 columns content">
    <h3><?= h($contactFeed->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $contactFeed->has('user') ? $this->Html->link($contactFeed->user->first_name, ['controller' => 'Users', 'action' => 'view', $contactFeed->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($contactFeed->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($contactFeed->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($contactFeed->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($contactFeed->message)); ?>
    </div>
</div>
