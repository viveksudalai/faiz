<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mobile Carrier List'), ['action' => 'edit', $mobileCarrierList->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mobile Carrier List'), ['action' => 'delete', $mobileCarrierList->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mobileCarrierList->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Mobile Carrier List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mobile Carrier List'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mobileCarrierList view large-9 medium-8 columns content">
    <h3><?= h($mobileCarrierList->carrier_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Carrier Name') ?></th>
            <td><?= h($mobileCarrierList->carrier_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($mobileCarrierList->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($mobileCarrierList->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($mobileCarrierList->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Status') ?></h4>
        <?= $this->Text->autoParagraph(h($mobileCarrierList->status)); ?>
    </div>
</div>
