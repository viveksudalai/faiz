<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mobileCarrierList->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mobileCarrierList->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Mobile Carrier List'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="mobileCarrierList form large-9 medium-8 columns content">
    <?= $this->Form->create($mobileCarrierList) ?>
    <fieldset>
        <legend><?= __('Edit Mobile Carrier List') ?></legend>
        <?php
            echo $this->Form->input('carrier_name');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
