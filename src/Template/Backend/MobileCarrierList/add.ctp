	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'mobile_carrier_list', 'action' => 'index']);?>
    </div>
            </div><!-- /.box-header -->  
		<div class="">
         
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($mobileCarrierList, ['id' => 'mobileCarrierList', 'name' => 'mobileCarrierList']) ?>
							    <fieldset>
							        <legend><?= __('Add Mobile Carrier') ?></legend>
							         <?php
							            echo $this->Form->input('carrier_name', [ 'class'=> 'form-control']);
							         ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 <script>
  $(document).ready(function() {
	  $("#mobileCarrierList").validate({
	     rules: {
	    	 carrier_name: {required:true}
       },
        messages: {
        	carrier_name: { 
  			 				required: "Mobile Carrier Name is required."
  						  }
  		 }
	}); 
});
</script>
   