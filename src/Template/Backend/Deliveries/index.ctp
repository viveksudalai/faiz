 
      <!-- Main content -->
        <section class="content">
          <div class="row">
		  
            <div class="col-xs-12">
              <div class="box">
              	 <div class="box-header">
	                  <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                  <h2 class="text-left" style="font-size: 18px; font-weight: 600;">Scheduled Deliveries</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			         <?= $this->Html->link('Add Driver Replacement', ['controller' => 'deliveries', 'action' => 'addDriverReplacement'], ['class' => 'btn btn-primary center-block pull-right']);?>
			         </div>
			        </div>
	                   
	                </div><!-- /.box-header --> 
                <div class="box-body table-responsive">
                	 <table id="allDeliveries" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr class="skyblue-bg">
				              	<th><?= 'Delivery Date' ?></th>
				                <th><?= 'Driver Name' ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php 
								$dDate = '';
								foreach ($driverDeleiveryInfo as $delivery):  //echo '<pre>';print_r($delivery); exit;
								if ($dDate == '' || $dDate != $delivery->delivery_date) {
								 if ($dDate == $delivery->delivery_date) {
										  echo "</td></tr>";
								    }		
							?>
				            <tr>
				            	<td><?= h($delivery->delivery_date->format('l  m/d/Y')) ?></td>
				                <td><?= h(ucwords($delivery->driver_name))."(".$delivery->tot_deliveries.")"?>
				            <?php } else { 
									  
								  ?>
							  <?= ", ". h(ucwords($delivery->driver_name))."(".$delivery->tot_deliveries.")"?> 
							<?php 
								 }
								 $dDate = $delivery->delivery_date;
								 endforeach; 
							?>
				        </tbody>
			    </table>
   			</div> 
   			</div> 	
   			 <div class="box">
	              <div class="box-header">
		              <div class="row">
		                 <div class="col-lg-8 col-md-12 col-xs-12">
		                  <h2 class="text-left" style="font-size: 18px; font-weight: 600;">	Driver Replacements</h2>
		       			</div>
		       		</div>	
			   			<div class="box-body table-responsive">
			                	 <table id="driverReplacement" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<thead>
							            <tr class="skyblue-bg">
							             	<th><?= 'Driver Name' ?></th>
							             	<th><?= 'Replaced Driver Name' ?></th>
							              	<th><?= 'Start Date' ?></th>
							                <th><?= 'End Date' ?></th>
							                <th>Actions</th>
							            </tr>
							        </thead>
							        <tbody>
							            <?php foreach ($driverReplacement as $driver):?>
							            
							            <tr>
							            	<td><?= h(ucwords($driver['driver_name'])) ?></td>
							                <td><?= h(ucwords($driver['replaced_driver_name']))?>
							                <td><?= h($driver['from_date']->format('l Y/m/d')) ?></td>
							                <td><?= h($driver['to_date']->format('l Y/m/d')) ?></td>
							                <td><ul class="action"><li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'deleteDriverReplacement', $driver['id']],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $driver['id'])],
			                                                 ['class' => 'btn btn-mini']); ?></li></ul></td>
							            </tr>
							            <?php endforeach; ?>    
							        </tbody>
						    </table>
			                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	   $('#allDeliveries').DataTable({
	   	 "bAutoWidth": false , 
	   	"ordering": false,
		 "aoColumnDefs": [
         { "sWidth": "10%",  "aTargets": [0]},
         { "sWidth": "20%", "aTargets": [1], "bSearchable": true, "bSortable": false},
        ]
	   	 });

      	$('#driverReplacement').DataTable({
	   	 "bAutoWidth": true,   
	   	"aoColumnDefs": [
		{  "aTargets": [4],"visible": false,"bSearchable": false, "bSortable": false}
		],
	   	 });
	} );
</script>
  