<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Thaali Survey'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="thaaliSurvey index large-9 medium-8 columns content">
    <h3><?= __('Thaali Survey') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('thaali_id') ?></th>
                <th><?= $this->Paginator->sort('rating') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($thaaliSurvey as $thaaliSurvey): ?>
            <tr>
                <td><?= $this->Number->format($thaaliSurvey->id) ?></td>
                <td><?= $thaaliSurvey->has('user') ? $this->Html->link($thaaliSurvey->user->first_name, ['controller' => 'Users', 'action' => 'view', $thaaliSurvey->user->id]) : '' ?></td>
                <td><?= $this->Number->format($thaaliSurvey->thaali_id) ?></td>
                <td><?= $this->Number->format($thaaliSurvey->rating) ?></td>
                <td><?= h($thaaliSurvey->created) ?></td>
                <td><?= h($thaaliSurvey->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $thaaliSurvey->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $thaaliSurvey->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $thaaliSurvey->id], ['confirm' => __('Are you sure you want to delete # {0}?', $thaaliSurvey->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
