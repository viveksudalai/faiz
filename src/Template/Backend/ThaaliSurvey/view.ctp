<?php
//Thaali Survey
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        
    </ul>
</nav>

<div class="box box-primary">
	<div class="preview-box" style="padding:15px";>
			<form name="frmexplist" id="frmexplist" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'thaalisurvey','action'=>'view','_full' => true )); ?>" method="POST">
			<div class="input-group form-group">
				<span class="input-group-addon" id="basic-addon1">
					<i class="fa fa-calendar"></i>
				</span> 
				<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
			</div>				
		    <div class="input-group form-group">
				<span class="input-group-addon" id="basic-addon1">
					<i	class="fa fa-calendar"></i>
				</span>
				<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
			</div>
			<div class="form-group">
				<label class="" for="">&nbsp;</label>
					<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
		    </div>

		</form>
    </div>
</div>

<?php
/**
 * Display User details based on the Selected date filters
 */ 
?>
<div class="box-body table-responsive">
	
	<table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr class="skyblue-bg">
				<th>Name</th>
				<th>Menu Item</th>
				<th>Are you happy with the taste?</th>
				<th>Happy about the quantity?</th>
				<th>Rating</th>
				<th>Created</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(count($survey)!= 0) {
					foreach ($survey as $surveyInfo): 
			?>
			 <tr>
				<td><?= h(ucwords($surveyInfo['user_name'])) ?></td>
				<td><?= h(ucwords($surveyInfo['menu_item'])) ?></td>
				
				<td>
					<?php 
						switch ($surveyInfo['taste']) {
							case '0': echo 'No';break;
							case '1': echo 'Yes'; break;
						
						}
					?> 
				</td>
				<td>
					<?php 
						switch ($surveyInfo['qty']) {
							case '1': echo 'More';break;
							case '2': echo 'Less'; break;
							case '3': echo 'Ok'; break;
						
						}
					?> 
				</td>
				<td><?= $surveyInfo['rating'].' '.h('Star') ?></td>
				<td><?= h( date("d-m-Y",strtotime($surveyInfo['created_date'])))?></td>
			</tr>
			<?php 
					endforeach;  
				}
			 ?>
		</tbody>
	</table>
</div>

<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});
  $(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);
		}
		
		//Date field validation start
		$("#frmexplist").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"This is required.",
				dtto:"This is required."
			}
		}); 
		
		//ends
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#allUsers').DataTable({
			"bAutoWidth": true , 
			"dom": 'Bfrtip',
			"aoColumnDefs": [
				 {  "aTargets": [0]},
				 {  "aTargets": [1]},
				 {  "aTargets": [2]},
				 {  "aTargets": [3], "bSearchable": true, "bSortable": true},
				 {  "aTargets": [4], "bSearchable": true},
				 {  "aTargets": [5], "bSearchable": true},
			],
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Survey_Report".date("d-m-Y"); ?>'},
				{extend: 'pdfHtml5',filename: '<?php echo "Survey_Report".date("d-m-Y"); ?>',
				 title:'<?php echo "Survey Report"." ".date("d-m-Y");?> '},
			],
		});
		
	});
</script>