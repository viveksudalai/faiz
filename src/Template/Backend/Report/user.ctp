<?php
/**
 * This help you you view the report and download the file
 * @returns App\Controller\Backend\ReportController.php
 * 
 */ 
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
       
    </ul>
</nav>


<div class="box box-primary">
	<div class="preview-box" style="padding:15px";>
			<form name="frmexplist" id="frmexplist" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'report','action'=>'user','_full' => true )); ?>" method="POST">
			<div class="input-group form-group">
				<span class="input-group-addon" id="basic-addon1">
					<i class="fa fa-calendar"></i>
				</span> 
				<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
			</div>				
		    <div class="input-group form-group">
				<span class="input-group-addon" id="basic-addon1">
					<i	class="fa fa-calendar"></i>
				</span>
				<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
			</div>
			<div class="input-group form-group">
					
					<?php echo $this->Form->input('user_role',['options' => $role,'class'=>'form-control','placeholder'=>'User Role','label' => false]);?>
			</div>
			<div class="form-group">
				<label class="" for="">&nbsp;</label>
					<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
		    </div>

		</form>
    </div>
</div>


<?php
/**
 * Display User details based on the Selected date filters
 */ 
?>
<div class="box-body table-responsive">
	
	<table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
	  	<thead>
		<tr class="skyblue-bg">
			<th>Name</th>
			<th>ejamaat id</th>
			<th>Address</th>
			<th>Contact Info</th>
			<th>Thaali Info</th>
			<th>Role</th>
			<th>Status</th>
			<th>Created</th>
		</tr>
		</thead>
	<tbody>
		<?php
			if(count($users)!= 0) {
				foreach ($users as $user): 
		?>
		 <tr>
			<td><?= h(ucwords($user['user_name'])) ?></td>
			<td><?= $user['ejamaatid'] ?></td>
			<td><?= h($user['address']."\n".ucwords(trim($user['city']))."\n ,".$user['state_abbrev']."\n".$user['zipcode']) ?></td>
			<td>Home : <?=  $user['home_phone'] ?><br> Mobile :<?=  $user['mobile_phone'] ?><br>Email : <?= h($user['email_address']) ?></td>
			<td>
				Size: <?php 
					switch ($user['thaali_size']) {
						case '0': echo 'None';break;
						case '1': echo 'Small'; 		
								  break;
						case '2': echo 'Medium';
								  break;
						case '3': echo 'Large';
									break;
						case '4': echo 'X-Small';
									break;
						case '5': echo 'X-Large';
									break;
					}
					echo '<br/>';
				?> 
				Delivery Type: <?php 
					switch ($user['delivery_method']) {
						case '1': echo 'Pick Up';break;
						case '2': echo 'Delivery'; break;
					}
					echo '<br/>';
				?> 
				Driver: <?= ucwords($user['driver_name']); echo '<br/>';  ?>
				Notes: <?= ucwords($user['driver_notes'])?>
			</td>
			<td><?= h($user['role_name']) ?></td>
			<td> <?php switch ($user['status']) {
			case '0': echo 'New';break;
			case '1': echo 'Active'; break;
			case '2': echo 'Inactive';break;
			case '3': echo 'Trashed (Unapproved User)';break;
			case '4': echo 'Trashed (Approved User)';break;
			}
			?></td>
			<td><?= h( date("d-m-Y",strtotime($user['created'])))?></td>
		</tr>
		<?php 
				endforeach;  
			}
		 ?>
	</tbody>
</table>
	
</div>

<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});

	
	$(document).ready(function() {
		
		//Date field validation start
		$("#frmexplist").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"This is required.",
				dtto:"This is required."
			}
		}); 
		
		//ends
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#allUsers').DataTable({
			"bAutoWidth": true , 
			"dom": 'Bfrtip',
			"aoColumnDefs": [
				 {  "aTargets": [0], "bSearchable": false, "bSortable": false},
				 {  "aTargets": [1]},
				 {  "aTargets": [2]},
				 {  "aTargets": [3], "bSearchable": true, "bSortable": true},
				 {  "aTargets": [4], "bSearchable": true},
				 {  "aTargets":  [5], "bSearchable": true},
				 {  "aTargets":  [6], "bSearchable": true},
			],
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Users_Report_".date("d-m-Y"); ?>'},
				{extend: 'pdfHtml5',filename: '<?php echo "Users_Report_".date("d-m-Y"); ?>',
				 title:'<?php echo "User Report"." ".date("d-m-Y");?> '},
			],
		});
		
	});
</script>