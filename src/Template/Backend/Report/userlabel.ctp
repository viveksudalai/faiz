<?php
/**
 * This Page is used to print the pdf File for user label report
 *
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
       
    </ul>
</nav>

<div class="box box-primary">
	<div class="preview-box" style="padding:15px";>
			<form name="frmexplist" id="frmexplist" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'report','action'=>'userlabel','_full' => true )); ?>" method="POST">
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-calendar"></i>
					</span> 
					<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i	class="fa fa-calendar"></i>
					</span>
					<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>				
				<div class="form-group">
					<label class="" for="">&nbsp;</label>
						<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
				</div>
		</form>
    </div>
</div>
<div class="box-body table-responsive">
<a href="<?php echo $this->Url->build(array('controller'=>'report','action'=>'pdflabel',$fromdate,$todate,'_full' => true )); ?>" >PDF Download</a>
<table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr class="skyblue-bg">
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
	    <?php
		$count = count($labellist);

		if($count!= 0){
			$i = 1 ;
			 
			echo '<tr>';
			foreach ($labellist as $user):
				$userName = ucwords($user['first_name']).' '.ucwords($user['last_name']);
				$mobilNo = $user['mobile_phone'] ;
				switch ($user['thaali_size']) {
					case '0': $thaaliSize = 'None';break;
					case '1': $thaaliSize = 'Small';break;
					case '2': $thaaliSize = 'Medium';break;
					case '3': $thaaliSize = 'Large';break;
					case '4': $thaaliSize = 'X-Small';break;
					case '5': $thaaliSize = 'X-Large';break;
				}
				$thaaliDate =$user['delivery_date'];
				$driverName = ucwords($user['driver_name']);
				echo '<td>'.$userName.'<br/>'.$mobilNo.'<br/>'.$thaaliSize.' - '.$thaaliDate.'<br/>'.$driverName.'</td>';
				
				
				if($i%3 == 0){
						if($i != $count) {
						echo '</tr><tr>';
					 
					}
				}
				$i++;
			endforeach; 
			$rem = $count%3;
			$rem = 3-$rem;
			if ($rem != 3) {
				for($j=0;$j<$rem;$j++)
				echo "<td></td>";
            }
			echo '</tr>';
			}	
		?> 
	</tbody>
</table>
</div>
<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});

	
	$(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);

			fromDate = $("#dtfrom").val();
			toDate = $("#dtto").val();
		}
		
		var userLabel = "Delivery Address "+ fromDate +" To "+ toDate;
		//Date field validation start
		$("#frmexplist").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"This is required.",
				dtto:"This is required."
			}
		}); 
		
		//ends "visible": false,
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#allUsers').DataTable({
			"bAutoWidth": true,
			"order": [],
			"bSort" : false,
			"dom": 'Bfrtip' 
		}); 
		
	});
</script>

