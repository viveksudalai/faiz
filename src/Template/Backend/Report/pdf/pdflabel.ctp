<?php
/**
 * This Page is used to print the pdf File for user label report
 *
 */
?>

<table cellspacing="2" border= "1" cellpadding="10" width="100%">

	<tbody>
	    <?php
		$count = count($labellist);

		if($count!= 0){
			$i = 1 ;
			
			echo '<tr>';
			foreach ($labellist as $user):
				$userName = ucwords($user['first_name']).' '.ucwords($user['middle_name']).' '.ucwords($user['last_name']);
				$mobilNo = $user['mobile_phone'] ;
				switch ($user['thaali_size']) {
					case '0': $thaaliSize = 'None';break;
					case '1': $thaaliSize = 'Small';break;
					case '2': $thaaliSize = 'Medium';break;
					case '3': $thaaliSize = 'Large';break;
					case '4': $thaaliSize = 'X-Small';break;
					case '5': $thaaliSize = 'X-Large';break;
				}
				$thaaliDate =$user['delivery_date'];
				$driverName = ucwords($user['driver_name']);
				
				if($user['color_code'] != ''){
					$color = $user['color_code'];
				}else{
					
					$color = '#000000';
				}
				
				echo '<td style="color:'.$color.'">'.$userName.'<br/>'.$mobilNo.'<br/>'.$thaaliSize.' - '.$thaaliDate.'<br/>'.$driverName.'</td>';
				if($i%3 == 0){
						if($i != $count) {
						echo '</tr><tr>';
						 
					}
				}
				$i++;
				
			endforeach; 
			$rem = $count%3;
			$rem = 3-$rem;
			if ($rem != 3) {
				for($j=0;$j<$rem;$j++)
				echo "<td>&nbsp;</td>";
            }
			echo '</tr>';
			}	
		?>
		 
	</tbody>
</table>


