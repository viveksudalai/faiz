<?php
/**
 * This help you you view the report and download the file
 * @returns App\Controller\Backend\ReportController.php
 * 
 */ 
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
       
    </ul>
</nav>


<div class="box box-primary">
	<div class="preview-box" style="padding:15px";>
			<form name="frmexplist" id="frmexplist" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'report','action'=>'deliveryreport','_full' => true )); ?>" method="POST">
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-calendar"></i>
					</span> 
					<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i	class="fa fa-calendar"></i>
					</span>
					<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>				
                <div class="input-group form-group">
					
					<?php echo $this->Form->input('thaaliSize',['options' => $thaaliSize,'class'=>'form-control','placeholder'=>'Thaali Size','label' => false]);?>
				</div>				
				<div class="input-group form-group">
					
					<?php echo $this->Form->input('deliverymethod',['options' => $deliveryMethod,'class'=>'form-control','placeholder'=>'Delivery Method','label' => false]);?>
				</div>
				
				<div class="input-group form-group">
						<?php echo $this->Form->input('drivery_list', ['options'=>$driverlist,'empty'=>'Select Driver','class'=>'form-control','placeholder'=>'Driver','label' => false]); ?>
				</div>
				
				<div class="form-group">
					<label class="" for="">&nbsp;</label>
						<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
				</div>
				
		</form>
    </div>
</div>


<?php
/**
 * Display User details based on the Selected date filters
 */ 
?>
<div class="box-body table-responsive">
	
    <?php 
	$small = $medium = $large = $xSmall = $xLarge =0;
	?>
	<table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
	  <thead>
		<tr class="skyblue-bg">
			<th>Delivery Date</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Last Name</th>
			<th>Mobile Phone</th>
			<th>Address</th>
			<th>City</th>
			<th>State</th>
			<th>Zipcode</th>
			<th>Thaali Size</th>
			<th>Delivery Type</th>
			<th>Driver</th>
			<th>Thaali Size ID</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if(count($thaaliData)!= 0) {
				foreach ($thaaliData as $user):
		?>
	    <tr>
			<td><?= h(ucwords($user['delivery_date'])) ?></td>
			<td><?= h(ucwords($user['first_name'])) ?></td>
			<td><?= h(ucwords($user['middle_name'])) ?></td>
			<td><?= h(ucwords($user['last_name'])) ?></td>
			<td><?= $user['mobile_phone'] ?></td>
			<td><?= $user['address'] ?></td>
			<td><?= ucwords(trim($user['city'])) ?></td>
			<td><?= $user['state_abbrev'] ?></td>
			<td><?= $user['zipcode'] ?></td>
			<td>
			<?php 
					switch ($user['thaali_size']) {
						case '0': echo 'None';break;
						case '1': echo 'Small'; 
								  $small = $small + 1;
								  break;
						case '2': echo 'Medium';
								  $medium = $medium + 1;
								  break;
						case '3': echo 'Large';
									$large = $large + 1;
									break;
						case '4': echo 'X-Small';
									$xSmall = $xSmall + 1;
									break;
						case '5': echo 'X-Large';
									$xLarge = $xLarge + 1;
									break;
					}
				?>
			</td>
			<td> 
				<?php 
					switch ($user['delivery_type']) {
						case '1': echo 'Pick Up';break;
						case '2': echo 'Delivery'; break;
					}
				?>
			</td>
			<td><?= ($user['delivery_type'] == '2') ? ucwords($user['driver_name']): 'Pick Up'; ?></td>
			<td><?= $user['thaali_size'] ?></td>
		</tr>
		<?php 
				endforeach; 
			} 
		?>
	</tbody>
</table>
</div>
<?php 
$sizeCount = '';
$sizeCount .= 'Small  :'.$small;
$sizeCount .= '  Medium : '.$medium;
$sizeCount .= '  Large  : '.$large;
$sizeCount .= '  X-Small: '.$xSmall;
$sizeCount .= '  X-Large: '.$xLarge;

?>
<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});

	
	$(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);
		}
		var pdfTitle = "Delivery Report "+ fromDate +" To "+ toDate;
		
		//Date field validation start
		$("#frmexplist").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"This is required.",
				dtto:"This is required."
			}
		}); 
		
		//ends "visible": false,
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#allUsers').DataTable({
			"bAutoWidth": true ,
			"order": [[12, "asc"]],
			"ordering": true,
			"dom": 'Bfrtip',
			"aoColumnDefs": [
				{  "aTargets": [12],"visible": false},
			],
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Delivery_Report_".date("d-m-Y"); ?>',exportOptions:{columns: [0, 1, 2, 3,4, 5,6,7,8,9,10,11]}},
				{extend: 'pdfHtml5',filename: '<?php echo "Delivery_Report_".date("d-m-Y"); ?>',
				 title: pdfTitle,
				 message: '<?php echo  html_entity_decode($sizeCount);?>',
				 exportOptions:{columns: [0, 1, 2, 3,4, 5,6,7,8,9,10, 11]}
				},
			],
		}); 
		
	});
</script>