<?php
/**
 * This help you you view the report and download the file
 * @returns App\Controller\Backend\ReportController.php
 * 
 */ 
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
       
    </ul>
</nav>


<div class="box box-primary">
	<div class="preview-box" style="padding:15px";>
			<form name="contactfeeds" id="contactfeeds" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'report','action'=>'contactfeeds','_full' => true )); ?>" method="POST">
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-calendar"></i>
					</span> 
					<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i	class="fa fa-calendar"></i>
					</span>
					<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>				
               <div class="form-group">
					<label class="" for="">&nbsp;</label>
						<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
				</div>
				
		</form>
    </div>
</div>


<?php
/**
 * Display User details based on the Selected date filters
 */ 
?>
<div class="box-body table-responsive">
	
   
	<table id="feeds" class="table table-striped table-bordered" cellspacing="0" width="100%">
	  <thead>
		<tr class="skyblue-bg">
			<th>Date</th>
			<th>Full Name</th>
			<th>Message</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($contactFeeds as $user):?>
	    <tr>
			<td><?= h($user['created']->format('m/d/Y')) ?></td>
			<td><?= h(ucwords($user ['Users'] ['first_name']." ".$user ['Users'] ['middle_name']." ".$user ['Users'] ['last_name'])) ?></td>
			<td><?= h(ucwords($user['message'])) ?></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
</div>
 
<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});

	
	$(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);
		}
		var pdfTitle = "Contact Feeds "+ fromDate +" To "+ toDate;
		
		//Date field validation start
		$("#contactfeeds").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"This is required.",
				dtto:"This is required."
			}
		}); 
		
		//ends "visible": false,
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#feeds').DataTable({
			"bAutoWidth": true ,
			"ordering": true,
			"dom": 'Bfrtip',
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Contact_Feeds".date("d-m-Y"); ?>',exportOptions:{columns: [0, 1, 2]}},
				{extend: 'pdfHtml5',filename: '<?php echo "Contact_Feeds".date("d-m-Y"); ?>',
				 title: pdfTitle,
				 exportOptions:{columns: [0, 1, 2]}
				},
			],
		}); 
		
	});
</script>