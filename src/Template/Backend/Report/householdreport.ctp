<?php 
//House Hold Summary repot
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
       
    </ul>
</nav>

<div class="box box-primary">
	<div class="preview-box" style="padding:15px";>
			<form name="summary_report" id="summary_report" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'report','action'=>'householdreport','_full' => true )); ?>" method="POST">
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-calendar"></i>
					</span> 
					<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i	class="fa fa-calendar"></i>
					</span>
					<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>				
			
				<div class="input-group form-group">
					
					<?php echo $this->Form->input('deliverymethod',['options' => $deliveryMethod,'class'=>'form-control','placeholder'=>'Delivery Method','label' => false]);?>
				</div>

				<div class="form-group">
					<label class="" for="">&nbsp;</label>
						<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
				</div>
				
		</form>
    </div>
</div>
<?php
/**
 * Display User details based on the Selected date filters
 */ 
?>
<div class="box-body table-responsive">

	<table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
	  <thead>
		<tr class="skyblue-bg">
			<th>Name</th>
			<th>Total Thaalis</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		    $totalCount = 0;
			if(count($summaryreport)!= 0) {
				foreach ($summaryreport as $user):
				$totalCount = $totalCount + $user['thaalicount'];
		?>
	    <tr>
			<td><?= h(ucwords($user['name'])) ?></td>
			<td><?= $user['thaalicount']?></td>
		</tr>
		<?php  endforeach; ?>
		<tr>
			<td><b><?= h(ucwords('Total')) ?></b></td>
			<td><b><?= $totalCount ?></b></td>
		</tr>	
		<?php
				} 
		?>	
	</tbody>
</table>
</div>
<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});

	
	$(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);
		}
		var pdfTitle = "House Hold Summary Report "+ fromDate +" To "+ toDate;
		
		//Date field validation start
		$("#summary_report").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"This is required.",
				dtto:"This is required."
			}
		}); 
		
		//ends "visible": false,
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#allUsers').DataTable({
			"bAutoWidth": true ,
			"ordering": false,
			"dom": 'Bfrtip', 
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Household_Summary_Report_".date("d-m-Y"); ?>',exportOptions:{columns: [0, 1]}},
				{extend: 'pdfHtml5',filename: '<?php echo "Household_Summary_Report_".date("d-m-Y"); ?>',
				 title: pdfTitle,
				 message: 'House Hold Summary Report',
				 exportOptions:{columns: [0, 1]}
				},
			],
		}); 
		
	});
</script>