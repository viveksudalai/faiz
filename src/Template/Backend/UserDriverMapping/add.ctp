
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
					<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'users', 'action' => 'index']);?>
				</div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($userDriverMapping) ?>
							    <fieldset>
							        <legend><?= __('Assign Driver') ?></legend>
							         <?php
							          echo $this->Form->input('id', ['default'=> $userDriverMappingId], [ 'class'=> 'form-control']);
							          echo  $this->Form->input('user_id', ['options' => $userDetails, 'class'=> 'form-control']);
							          
           							  echo $this->Form->input('driver_id', ['options' => $driverDetails, 'default'=> $driverId , 'empty'=>'Choose Driver', ['class'=> 'form-control']]);
							        ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>

 