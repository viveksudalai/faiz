<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-xs-12">
           <h2><?= __('User Driver Mapping') ?></h2>
        </div>
        <div class="col-lg-8 col-md-12 col-xs-12">
           <!-- <a class="btn btn-primary center-block pull-right" data-toggle="tooltip" href="add">
            <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add New User</span></a>-->
           <?= $this->Html->link(__('New User Driver Mapping'), ['action' => 'add']) ?>
        </div>
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allUserDriver" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr>
				                <th><?= 'user id' ?></th>
				                <th><?= 'driver id' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php foreach ($userDriverMapping as $userDriverMapping): ?>
				            <tr>
				                <td><?= $this->Number->format($userDriverMapping->user_id) ?></td>
				                <td><?= $userDriverMapping->has('user') ? $this->Html->link($userDriverMapping->user->id, ['controller' => 'Users', 'action' => 'view', $userDriverMapping->user->id]) : '' ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userDriverMapping->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userDriverMapping->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userDriverMapping->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

  <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#allUserDriver').DataTable({
	   	 "bAutoWidth": false , 
         "aoColumnDefs": [
         { "sWidth": "30%", "aTargets": [0]},
         { "sWidth": "30%", "aTargets": [1]},
         { "sWidth": "30%", "aTargets": [2], "bSearchable": false, "bSortable": false},
        ]
	   	 });
	} );
</script>
  
   