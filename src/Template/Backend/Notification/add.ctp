 


		<div class="">
        	<div class="box-header">
            		<div class="col-md-12 text-right table-upper-row">
  						<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Notification', 'action' => 'index']);?>
					</div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
						 <?= $this->Form->create($notification, ['id' => 'notification', 'name' => 'notification']) ?> 
							      <fieldset>
							        <legend><?= __('General Notifications') ?></legend>
							        <?php
								       echo $this->Form->input('start_date', ['class'=>'datepicker form-control','type'=> 'text']);
									   echo $this->Form->input('end_date', ['class'=>'datepicker form-control','type'=> 'text']);
									   echo $this->Form->input('details',['class'=> 'form-control']);
								 ?>
								</fieldset>
								  <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
  <script>
  
  $(function() {
			
				var dates = $("#start-date, #end-date").datepicker({
					  minDate:'-0y',
                     dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#notification").validate({
	     rules: {
	    	    details: {required:true},
	         	start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			details: { 
  			 				required: "Details notes is required." 
  						  }, 
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 
});
</script>
 
 
