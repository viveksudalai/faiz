<div class="box-header">
      <div class="col-md-12 text-right table-upper-row">
         <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Notification', 'action' => 'index']);?>
      </div>
 </div><!-- /.box-header -->
 
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		    <h3> <?= __('View Notification') ?></h3> 
		   <table id="miqat" class="table table-striped table-bordered" cellspacing="0" width="100%">
		         <tr>
            <th><?= __('Start Date') ?></th>
            <td><?= h($notification->start_date) ?></td>
        </tr>
        <tr>
            <th><?= __('End Date') ?></th>
            <td><?= h($notification->end_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Details') ?></th>
            <td><?= $this->Text->autoParagraph(h($notification->details)); ?></td>
        </tr>
		       
		    </table>
		</div>
	</div>
</div>
  
 
