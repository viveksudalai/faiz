 
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
	              <div class="box-header">
	                  <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">All Notification</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			           <?= $this->Html->link('New Notification', ['controller' => 'notification', 'action' => 'add'], ['class' => 'btn btn-primary center-block pull-right']);?>
			        </div>
			        </div>
	                   
	                </div><!-- /.box-header --> 
              	<div class="box-body table-responsive">
                	 <table id="notification" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr class="skyblue-bg">
				                <th><?= 'start date'?></th>
				                <th><?= 'end date' ?></th>
				                <th><?= 'details' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php foreach ($notification as $info): ?>
				            <tr>
				            	<td><?= $info->start_date->format('l Y/m/d')?></td>
				                <td><?= $info->end_date->format('l Y/m/d') ?></td>
				                <td><?= $info->details ?></td>
				                <td class="actions">
				                	<ul class="action">
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['action' => 'view', $info->id], ['escape' => false]) ?></li>
				                	
			                    	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $info->id], ['escape' => false]) ?></li>
				                	     		<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $info->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $info->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>
			                    </ul>   
				                  </td>
				            </tr>
				            <?php endforeach; ?>
				        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#notification').DataTable({
	   	 "bAutoWidth": true , 
	   	 "ordering": false,
	   	 
	   	 });
	} );
</script>
  
   