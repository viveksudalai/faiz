<div class="box-header">
   	<div class="col-md-12 text-right table-upper-row">
  		<?= $this->Html->link('Edit Profile',['prefix' => 'backend', 'controller' => 'Users', 'action' => 'edit', $user->id]);?>
  		<?php if($user->user_role != 3) { ?>
  		<?= $this->Html->link('Add Vacation',['prefix' => 'backend', 'controller' => 'UserVacationPlanner', 'action' => 'add',  $user->id]);?>
  		<?php } ?>
  		<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Users', 'action' => 'index']);?>
	</div>
</div>
<!-- /.box-header -->
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="box col-xs-12">
         <div class="box-body table-responsive">
                <ul class="nav nav-tabs tab-active">
				        <li class="active"><a data-toggle="tab" href="#user">Basic Information</a></li>
				        <?php if ($user->user_role != 3 ) { ?>
				        	<li><a data-toggle="tab" href="#thaali">Thaali Information</a></li>
				            <li><a data-toggle="tab" href="#thaalicalender">Thaali Calender</a></li>
				        <?php } ?>    
				        	<?php if ($user->user_role == 3 || $user->user_role == 5) { ?> 
				        	 <!--<li><a data-toggle="tab" href="#driver">Vehicle Information</a></li>-->
				        	 <li><a data-toggle="tab" href="#scheduledDeliveries">Scheduled Deliveries</a></li>
				        	 <li><a data-toggle="tab" href="#statistics">Delivery Statistics</a></li>
				        	 <?php } ?>
				         <?php if ($user->user_role != 3 ) { ?>
				         	<!-- <li><a data-toggle="tab" href="#paymentHistory">Payment History</a></li>-->
				         <?php } ?> 	
				        
			    </ul>
			          <div class="tab-content">
						<div id="user" class="tab-pane fade in active tab-space">
							<div class="table-responsive">
							 <table class="table table-bordered">
							   
						          <tr>
						            <th><?= __('Ejamaatid') ?></th>
						            <td><?= h($user->ejamaatid) ?></td>
						        </tr>
						       
						        <tr>
						            <th><?= __('First Name') ?></th>
						            <td><?= h($user->first_name) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Middle Name') ?></th>
						            <td><?= h($user->middle_name) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Last Name') ?></th>
						            <td><?= h($user->last_name) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Email Address') ?></th>
						            <td><?= h($user->email_address) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Secondary Email Addresses') ?></th>
						            <td><?= h($user->secondary_email_address) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Home Phone') ?></th>
						            <td><?= h($user->home_phone) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Mobile Phone') ?></th>
						            <td><?= h($user->mobile_phone) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Mobile Carrier') ?></th>
						            <td><?= h($user->mobile_carrier_list->carrier_name) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Zipcode') ?></th>
						            <td><?= h($user->zipcode) ?></td>
						        </tr>
						        <!--<tr>
						            <th><?= __('Password') ?></th>
						            <td><?= h($user->password) ?></td>
						        </tr>-->
						        
						      
						        <tr>
						            <th><?= __('Address') ?></th>
						            <td><?= h($user->address) ?></td>
						        </tr>
						        
						        <tr>
						            <th><?= __('City') ?></th>
						            <td><?= h($user->city) ?></td>
						        </tr>
						        
						        <tr>
						            <th><?= __('State') ?></th>
						            <td><?= h($user->state->name) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Registration Type') ?></th>
						            <td><?php
						             if($user->registration_type == '0')
						             	echo 'Admin';
						             else if($user->registration_type == '1')
						              	echo 'User';
						            ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Status') ?></th>
						            <td><?php switch ($user->status) {
			          							case '0': echo 'New';break;
			          							case '1': echo 'Active'; break;
			          							case '2': echo 'Inactive';break;
			          					}
			         				 ?></td>
						        </tr>
						         <tr>
						            <th><?= __('Role') ?></th>
						            <td><?= h($user->role->name) ?></td>
						        </tr>
						        <?php if ($user->user_role == 3 || $user->user_role == 5) { ?> 
						        <tr>
						            <th><?= __('Color Code') ?></th>
						            <td style="background-color:<?= h($user->color_code); ?>"><span ><?= h($user->color_code); ?></span></td>
						        </tr>
						        <?php } ?>
						        <tr>
						            <th><?= __('Created') ?></th>
						            <td><?= h($user->created->format('l Y/m/d')) ?></td>
						        </tr>
						        <tr>
						            <th><?= __('Modified') ?></th>
						            <td><?= h($user->modified->format('l Y/m/d')) ?></td>
						        </tr>
						    </table>
						</div>
					</div>	    
						<?php if ($user->user_role == 3 || $user->user_role == 5) { ?> 
							 
								 
								 <div id="scheduledDeliveries" class="tab-pane fade tab-space">
									<div class="box-body table-responsive">
								      
								       <table id="driverThaaliDeliveries" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr class="skyblue-bg">
												<th><?= 'Date' ?></th>
												<th><?= 'User Name' ?></th>
												<th><?= 'Mobile No' ?></th>
												<th><?= 'Address' ?></th>
												<th><?= 'Size' ?></th>
												<th><?= 'Delivery Notes' ?></th>
											</tr>
										</thead>
										<tbody> 
								        <?php foreach ($driverDeleiveryInfo as $delivery): ?>
									    <tr>
											<td><?= $delivery ['delivery_date'] ?></td>
										    <td><?= ucwords($delivery['Users']['first_name']." ".$delivery['Users']['middle_name']." ".$delivery['Users']['last_name']) ?></td>
											<td><?= $delivery ['Users']['mobile_phone'] ?></td>
											<td><?= $delivery ['Users']['address'].", ". $delivery ['Users']['city'].", ". $delivery ['States']['abbrev']."-". $delivery ['Users']['zipcode'] ?></td>
								            <td><?php switch ($delivery['thaali_size']) {
					          							case '0': echo 'None';break;
					          							case '1': echo 'Small (1-2 servings)'; break;
					          							case '2': echo 'Medium (3-4 Servings)';break;
					          							case '3': echo 'Large (5-6 Servings)';break;
					          							case '4': echo 'X-Small (Salawat)';break;
					          							case '5': echo 'X-Large';break;
					          					}
												?>
											</td>
											<td><?= $delivery ['delivery_notes'] ?></td>
								        </tr>  
										<?php endforeach; ?>
								      </table>
								     
								    </div>
								 </div>
							<?php } ?>   
							<?php if ($user->user_role != 3 ) { ?>
						<div id="thaali" class="tab-pane fade tab-space">
							<div class="table-responsive">
							 
									 <table class="table table-bordered">
									 
									    <tr>
								            <th><?= __('Default Thaali Size') ?></th>
								            <td><?php switch ($thaali_size) {
					          							case '0': echo 'None';break;
					          							case '1': echo 'Small (1-2 servings)'; break;
					          							case '2': echo 'Medium (3-4 Servings)';break;
					          							case '3': echo 'Large (5-6 Servings)';break;
					          							case '4': echo 'X-Small (Salawat)';break;
					          							case '5': echo 'X-Large';break;
					          					}
					         				 ?></td>
					         			 
								        </tr>
								          <tr>
								            <th><?= __('Delivery Method') ?></th>
								             <td><?php switch ($delivery_method) {
					          							case '1': echo 'Pick Up'; break;
					          							case '2': echo 'Delivery';break;
					          						}
					         				 ?></td>
								        </tr>
								        <tr>
								            <th><?= __('Active Days') ?></th>
								           
					         		         <td><?php 
					         		                  $active = '';
								                      $activeDays = $active_days; 
								                      if(!empty($activeDays)) {
								                      $activeDays = (explode(",",$activeDays)); 
									                  $days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
									                  foreach ($activeDays as $value) {
									                       $active.=  $days[$value].",";
									                      }
									                     $active = substr($active,0,-1);
									                  }   
								                     echo $active;
					         				 ?></td>
					         		 
								        </tr>
								        <tr>
								            <th><?= __('Distribution Name') ?></th>
								            <td><?= h($distributionName) ?></td>
								        </tr>
								        <tr>
								            <th><?= __('Driver Name') ?></th>
								            <td><?= h($driverName) ?></td>
								        </tr>
								        <tr>
								            <th><?= __('Default Driver Notes') ?></th>
								            <td><?= h($driver_notes) ?></td>
								        </tr>
								      
								    </table>
								</div>
							
							</div>
							
							<div id="thaalicalender" class="tab-pane fade tab-space">
									<div class="box-body table-responsive">
									 <table id="userThaali" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr class="skyblue-bg">
												<th><?= 'Date' ?></th>
												<th><?= 'Delivery Method' ?></th>
												<th><?= 'Size' ?></th>
												<th><?= 'Delivery Notes' ?></th>
												<th><?= 'Menu' ?></th>
												<th><?= 'Caterer' ?></th>
												<th><?= 'Action' ?></th>
											</tr>
										</thead>
										<tbody>
								          									 
									<?php foreach ($thaaliDeleiveryInfo as $thaali):  ?>
									    <tr id = <?= $thaali['id']?>>
											<td><?= $thaali['delivery_date']->format('Y/m/d') ?></td>
										    <td><?php switch ($thaali['delivery_type']) {
					          							case '1': echo 'Pick Up'; break;
					          							case '2': echo 'Delivery';break;
					          						}
					         				 ?></td>
								         
								            <td><?php switch ($thaali['thaali_size']) {
					          							case '0': echo 'None';break;
					          							case '1': echo 'Small (1-2 servings)'; break;
					          							case '2': echo 'Medium (3-4 Servings)';break;
					          							case '3': echo 'Large (5-6 Servings)';break;
					          							case '4': echo 'X-Small (Salawat)';break;
					          							case '5': echo 'X-Large';break;
					          					}
					         				 ?></td>
					         			    <td><?= $thaali['delivery_notes'] ?></td>
								            <td><?= $thaali['ThaaliInfo']['menu_item'] ?></td>
								        
								            <td><?= $thaali ['CatererInfo']['name'] ?></td>
								            <td><?php if(date('Y-m-d') <= date('Y-m-d', strtotime($thaali['delivery_date'])) ){  ?>
								            
								            <a href="javascript:userEdit(<?php echo $thaali['id']?>)">Edit</a>
								            <?php } ?>
								            </td>
								        </tr> 
								   
									 <?php endforeach; ?>
									 <?php foreach ($userVacationPlanner as $vacation):  ?>
										<tr style="background-color:red !important">
											 <td><?= $vacation ['start_date']->format('Y/m/d')?></td>
											 <td><?= $vacation ['end_date']->format('Y/m/d')?></td>
											 <td>Vacation Period</td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
										</tr>
									<?php endforeach; ?>
									<?php foreach ($miqat as $miqat):  ?>
										<tr style="background-color:#51e451 !important">
											 <td><?= $miqat ['start_date']->format('Y/m/d')?></td>
											 <td><?= $miqat ['end_date']->format('Y/m/d')?></td>
											 <td><?= $miqat ['details']?></td>
											 <td></td>
											 <td></td>
											 <td></td>
											  <td></td>
										</tr>
									<?php endforeach; ?>
									 </table>
								    </div>
							</div>
							<!-- <div id="paymentHistory" class="tab-pane fade tab-space">
									<div class="box-body table-responsive">
								      <table id="userPayment" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr class="skyblue-bg">
												<th><?= 'Payment Date' ?></th>
												<th><?= 'Payment Type' ?></th>
												<th><?= 'Amount' ?></th>
												<th><?= 'Status' ?></th>
											</tr>
										</thead>
										<tbody>
								          									 
										<?php foreach ($userPaymentHistory as $payment):  ?>
									    <tr>
											<td><?= $payment['created']->format('d/m/Y') ?></td>
											<td><?php switch ($payment['payment_type']) {	 
					          							case '1': echo 'Credit Card'; break;
					          							case '2': echo 'Debit Card';break;
					          							case '3': echo 'Cheque';break;
					          							case '4': echo 'DD';break;
					          							case '5': echo '';break;
					          					}
					         				 ?></td>
											<td><?= $payment['amount'] ?></td>
										    <td><?php switch ($payment['payment_status']) {	 
														case '0': echo 'Pending'; break;
					          							case '1': echo 'Success'; break;
					          							case '2': echo 'Failed';break;
					          						}
					         				 ?></td>
								         
								        </tr> 
								   
									 <?php endforeach; ?>
									</tbody>
								</table>
								    </div>
						    </div>-->
							<?php } ?>
							<div id="statistics" class="tab-pane fade tab-space">
								<div class="table-responsive">
									Overall Summary
									<table class="table table-striped table-bordered" cellspacing="0" width="100%">
										<tr>
											<td>Status</td>
											<td>Today</td>
											<td>This Week</td>
											<td>This Month</td>
											<td>This Year</td>
											<td>All Time</td>
										</tr>
										<tr>
											<td>Household Tiffins</td>
											<td><?= $todaysDelivery;?> </td>
											<td><?= $thisWeekDelivery;?></td>
											<td><?= $thisMonthDelivery;?></td>
											<td><?= $thisYearDelivery;?></td>
											<td><?= $allTimeDelivery;?></td>
										</tr>
									</table>
									
							  		Monthly Totals for <?= date("Y"); ?>
									<table class="table table-striped table-bordered" cellspacing="0" width="100%">
										<tr>
											<td>Status</td>
											<?php foreach ($monthsDelivery as $key => $value):  ?>
											<td><?= date('M', mktime(0,0,0,$key))  ?></td>
											<?php endforeach; ?> 
										<tr>
										<td>Household Tiffins</td>
										<?php 
											 
											foreach ($monthsDelivery as $months):
												echo "<td>".$months."</td>";					 
											endforeach; 
										?>
										
										</tr>
									</table>
								</div>
							</div>
					     		
			 			</div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.row -->  
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update Request - <span id='delivery-date'></span></h4>
      </div>
        <?= $this->Form->create('', ['id' => 'delivery', 'name' => 'delivery', 'url' => ['controller' => 'Users', 'action' => 'editThaaliDelivery']]); ?>
        <div class="modal-body" ></div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        	<button type="button" id='formsub' data-dismiss="modal" class="btn btn-primary">Save changes</button>
        </div>
       <?= $this->Form->end() ?>  
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


        	<script>  
     $(document).ready(function() {
   	 $('#userThaali').DataTable({
   	   	"bAutoWidth": true , 
	   	  "order": [[ 0, "desc" ]],
	   	  "ordering": true ,
        fixedColumns: true,
        "bSort" : true
   	 });
	 $('#driverThaaliDeliveries').DataTable({
   	   	"bAutoWidth": true , 
	   	  "order": [[ 0, "desc" ]],
	   	  "ordering": false,
	   	 
        fixedColumns: true,
        "bSort" : true
   	 });
	 
	/*  $('#userPayment').DataTable({
   	   	"bAutoWidth": false , 
	   	  "order": [[ 0, "desc" ]],
	   	  "ordering": false,
	   	  columnDefs: [
            { "width": '10%',  "aTargets": [0]},
            { width: '10%',  "aTargets": [1]},
            { width: '10%',  "aTargets": [2]},
			{ width: '10%',  "aTargets": [3]},
        ],
        fixedColumns: true,
        "bSort" : true
   	 });*/

	  $("#formsub").click(function( event ) {
		 // event.preventDefault();
		  var formUrl = $(this).attr('action'); 
		  var post_data = $("#delivery").serialize();  
		  var delivery_id = $('#delivery-id').val();
		  $.ajax({
			    type : 'POST',
			    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'editThaaliDelivery','_full' => true )); ?>',
				data : post_data,
			    success: function(response) {  
			    	  
			    	   $('#'+delivery_id).html(response); 
			    	   alert("Thaali details has been updated successfully");
		    },
		    error: function (xhr, textStatus, errorThrown)	{
             	alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
			});
		});
	 }); 

	 

	function userEdit(id) {
		 var formUrl = $(this).attr('action'); 
		  
		  $.ajax({
			    type : 'POST',
			    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'getUserThaaliDeliveryById','_full' => true )); ?>',
				data : {"id":id},
			     success: function(response) {  
			    	 $('.modal-body').html(response);
		    },
		    error: function (xhr, textStatus, errorThrown)	{
            	alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
			});
		 $('#myModal').modal('toggle');
	}
	 </script>
            
 