

      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
              	<div class="box-header">
	               <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">New Users - This Month</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			            <?= $this->Html->link('Add User', ['controller' => 'users', 'action' => 'add'], ['class' => 'btn btn-primary center-block pull-right']);?>
			        </div>
			        </div>
              </div>
      <div class="box-body table-responsive">
               
		         <table id="newUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
		          <thead>
		             <tr class="skyblue-bg">
			                <th>id</th>
			                <th>name</th>
			                <th>ejamaat id</th>
			                <th>email</th>
			                <th>address</th>
			                <th>role</th>
			                <th>status</th>
			                <th class="actions"><?= __('Actions') ?></th>
			            </tr>
		           </thead>
		          <tbody>
		          
		           <?php foreach ($thisMonthUser as $user): ?>
		            <tr>
		              <td><?= h($user->id) ?></td>
		              <td><?= h(ucwords($user->full_name)) ?></td>
			          <td><?= $user->ejamaatid ?></td>
			          <td><?= h($user->email_address) ?></td>
			          	<td><?= h($user->address.", ".ucwords(trim($user->city)).", ".$user->state->abbrev." ".$user->zipcode) ?></td>
			          <td><?= h($user->role->name) ?></td>
			          <td>
			          <?php switch ($user->status) {
			          		case '0': echo 'New';break;
			          		case '1': echo 'Active'; break;
			          		case '2': echo 'Inactive';break;
			          		case '3': echo 'Trashed (Unapproved User)';break;
			          		case '4': echo 'Trashed (Approved User)';break;
			          		}
			          ?>
		              <td>
		           
			                	<ul class="action">
				                	<?php if($user->status != 1):?> 
				                	<li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-check-circle', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Enable']). "",
                                                 ['action' => 'enable', $user->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to enable this user?', $user->id)],
                                                 ['class' => 'btn btn-mini']); ?></li>
				                	<?php else: ?>
				                	<li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-minus-circle', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Disable']). "",
                                                 ['action' => 'disable', $user->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to disable this user?', $user->id)],
                                                 ['class' => 'btn btn-mini']); ?></li>
                                    <?php endif; ?>
                                    <?php if ($user->user_role != 3) { ?>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-user-plus' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Assign Driver', 'aria-hidden' => 'true']), ['controller' => 'UserDriverMapping', 'action' => 'add', $user->id], ['escape' => false]) ?></li>
				                	<!--<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-building-o' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Assign Distribution Center', 'aria-hidden' => 'true']), ['controller' => 'UserDistributionMapping', 'action' => 'assign', $user->id], ['escape' => false]) ?></li>-->
				                	<?php } ?>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-lock' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Reset Password' , 'aria-hidden' => 'true']), [ 'action' => 'resetPwd', $user->id], ['escape' => false]) ?></li>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['action' => 'view', $user->id], ['escape' => false]) ?></li>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['escape' => false]) ?></li>
				                	<li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete']). "",
                                                 ['action' => 'delete', $user->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)],
                                                 ['class' => 'btn btn-mini']); ?></li>
			                	</ul>
		               </td>
		            </tr>
		           <?php endforeach; ?>  
		          </tbody>
		        </table>
		    </div>   
      		</div>
      		  <div class="box">
                <div class="box-body table-responsive">
                	 <h2 class="text-left" style="font-size: 18px; font-weight: 600;"><?= __('All Users') ?></h2>
     
			        <table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
			          <thead>
			            <tr class="skyblue-bg">
			                <th>id</th>
			                <th>name</th>
			                <th>ejamaat id</th>
			                <th>email</th>
			                <th>address</th>
			                <th>role</th>
			                <th>status</th>
			                <th class="actions"><?= __('Actions') ?></th>
			            </tr>
			        </thead>
			        <tbody>
			            <?php foreach ($users as $user): ?>
			           <tr>
			           		<td><?= h($user->id) ?></td>
			                <td>
			                <?= h(ucwords($user->full_name)) ?></td>
			                <td><?= $this->Number->format($user->ejamaatid) ?></td>
			           		<td><?= h($user->email_address) ?></td>
			           		<td><?= h($user->address.", ".ucwords(trim($user->city)).", ".$user->state->abbrev." ".$user->zipcode) ?></td>
			                <td><?= h($user->role->name) ?></td>
			                <td> <?php switch ($user->status) {
			          		case '0': echo 'New';break;
			          		case '1': echo 'Active'; break;
			          		case '2': echo 'Inactive';break;
			          		case '3': echo 'Trashed (Unapproved User)';break;
			          		case '4': echo 'Trashed (Approved User)';break;
			          		}
			          		?></td>
			               	<td class="actions">
			                	<ul class="action">
				                	 
				                	<?php if($user->status !=1):?> 
				                	<li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-check-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Enable']). "",
                                                 ['action' => 'enable', $user->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to enable this user?', $user->id)],
                                                 ['class' => 'btn btn-mini']); ?></li>
				                	<?php else: ?>
				                	<li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-minus-circle', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Disable']). "",
                                                 ['action' => 'disable', $user->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to disable this user?', $user->id)],
                                                 ['class' => 'btn btn-mini']); ?></li>
                                    <?php endif; ?>
                                    <?php if ($user->user_role != 3) { ?>
                                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-user-plus' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Assign Driver', 'aria-hidden' => 'true']), ['controller' => 'UserDriverMapping', 'action' => 'add', $user->id], ['escape' => false]) ?></li>
				                	<!--<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-building-o' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Assign Distribution Center', 'aria-hidden' => 'true']), ['controller' => 'UserDistributionMapping', 'action' => 'assign', $user->id], ['escape' => false]) ?></li>-->
				                	<?php } ?>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-lock' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Reset Password' , 'aria-hidden' => 'true']), [  'action' => 'resetPwd', $user->id], ['escape' => false]) ?></li>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['action' => 'view', $user->id], ['escape' => false]) ?></li>
				                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o' , 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['escape' => false]) ?></li>
				                	<li><?= $this->Form->postLink(
                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete']). "",
                                                 ['action' => 'delete', $user->id],
  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)],
                                                 ['class' => 'btn btn-mini']); ?></li>
			                	</ul>
			                	</td>
			            </tr>
			            
			         <?php //if ($user->role->id == '3') { 
			         ?>
			            <!-- <tr>
			             <td>driver info</td>
                           <td colspan='7'>
                              <table>
                              	<tr> 
                              		<td>License No</td>
                              		<td>License Exp Date</td>
                              		<td>Vehicle Make</td>
                              		<td>Vehicle Model</td>
                              		<td>Vehicle Year</td>
                              		<td>Insurance Provider</td>
                              		<td>Insurance Policy No</td>
                              		<td>Insurance Exp Date</td>
                              	</tr>
                              	 <?php foreach ($user->driver_info as $driverInfo): ?>
                              	<tr>
                              	    
                              		<td><?= h($driverInfo->license_number) ?></td>
                              		<td><?= h($driverInfo->license_expiry_date) ?></td>
                              		<td><?= h($driverInfo->vehicle_make) ?></td>
                              		<td><?= h($driverInfo->vehicle_model) ?></td>
                              		<td><?= h($driverInfo->vehicle_year) ?></td>
                              		<td><?= h($driverInfo->insurance_provider) ?> </td>
                              		<td><?= h($driverInfo->insurance_policy_no) ?></td>
                              		<td><?= h($driverInfo->insurance_expiry_date) ?></td>
                              	</tr>
                              <?php endforeach; ?>
                              </table>
                           <td>
                        </tr>-->
                          
                		
            <?php //}
             ?>
			            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
 
 
 
    <!--<div class="paginator pull-right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>-->

 <script>  
     $(document).ready(function() {
	   	 $('#allUsers').DataTable({
	   	    "bAutoWidth": false , 
	   	     
	   	     //"order": [[ 0, "desc" ]],
	   	         "aoColumnDefs": [
         { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
         { "sWidth": "15%", "aTargets": [1]},
         { "sWidth": "12%", "aTargets": [2]},
         { "sWidth": "10%", "aTargets": [3], "bSearchable": true, "bSortable": true},
         { "sWidth": "18%", "aTargets": [4], "bSearchable": true},
         { "sWidth": "5%", "aTargets":  [5], "bSearchable": true},
         { "sWidth": "5%", "aTargets": [6], "bSearchable": true, "bSortable": false},
         { "sWidth": "31", "aTargets": [7], "bSearchable": false, "bSortable": false},
          ] 
	   	 });
   	 
	   	 $('#newUsers').DataTable({
	   	 
	   	  "bAutoWidth": false , 
	   	  
          "order": [[ 0, "desc" ]],
         "aoColumnDefs": [
         { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
         { "sWidth": "15%", "aTargets": [1]},
         { "sWidth": "13%", "aTargets": [2]},
         { "sWidth": "15%", "aTargets": [3], "bSearchable": true, "bSortable": true},
         { "sWidth": "18%", "aTargets": [4], "bSearchable": true},
         { "sWidth": "3%", "aTargets":  [5], "bSearchable": true},
         { "sWidth": "4%", "aTargets": [6], "bSearchable": true, "bSortable": false},
         { "sWidth": "30%", "aTargets": [7], "bSearchable": false, "bSortable": false},
          ]
        });
	} );
	 
</script>	

  