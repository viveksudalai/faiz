<?php $this->assign('title', 'Login'); ?>
<div class="container">
    <div class="row">
    	<div class="col-md-offset-1 col-md-10">
        	<div class="row">
            	<div class="col-md-offset-3 col-md-6">
                    <div class="userbox">
                    	<div class="row">
                            <div class="col-md-12">
                                <?= $this->Html->image('admin-logo.png', ['alt' => 'Faizchicago', 'class' => 'logo-left img-responsive']);?>
                            </div>
                        </div>
                        <?php // $this->Flash->render('auth');
                        ?>
                        <?= $this->Flash->render() ?>
						<?= $this->Form->create('Users', ['id' => 'login', 'name' => 'login']) ?>
	                        <div class="input-group">
	                            <?= $this->Form->input('email_address', ['div' => false, 'class'=> 'form-control', 'label' => '', 'placeholder' => 'Email Id', 'autocomplete' => 'off']) ?>
	                            <span class="input-group-addon"><i aria-hidden="true" class="fa user-icon"></i></span>
	                        </div>
	                        <div class="input-group">
	                            <?= $this->Form->input('password', ['div' => false, 'class'=> 'form-control', 'label' => '', 'placeholder' => 'Password', 'autocomplete' => 'off']) ?>
	                            <span class="input-group-addon"><i aria-hidden="true" class="fa lock"></i></span>
	                        </div>
	                         <div class="input-group">
	                          <div class="forget">
	                           <a href="forgotPassword" class="btn btn-primary col-lg-offset-7">Forgot your password?</a>
	                          </div> 
	                         </div>
	                        <div class="form-group">
	                          <div class="login-btn">
	                        	<?= $this->Form->submit('Login', array('class'=> 'btn btn-primary btn-block')); ?>
	                          </div>
	                        </div>
	                   <?= $this->Form->end() ?>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div>
</div>

<script>
 $(document).ready(function() {
	  $("#login").validate({
	     rules: {
         email_address: {
             // simple rule, converted to {required:true}
              required:true 
         }, 
	     password: {
              required:true
         }  
     },
        messages: {
           email_address:"Email address is required.",
       	   password:"Password is required."
		}
	}); 
});
</script>