<?php $this->assign('title', 'Forgot Password'); ?>
<div class="container">
    <div class="row">
    	<div class="col-md-offset-1 col-md-10">
        	<div class="row">
            	<div class="col-md-offset-3 col-md-6">
                    <div class="userbox">
                    	<div class="row">
                            <div class="col-md-12">
                                <?= $this->Html->image('admin-logo.png', ['alt' => 'Faizchicago', 'class' => 'logo-left img-responsive']);?>
                            </div>
                        </div>
                        <?php // $this->Flash->render('auth');
                        ?>
                        <?= $this->Flash->render() ?>
						<?= $this->Form->create('Users', ['id' => 'login', 'name' => 'login']) ?>
							<div class="input-group">
	                            <?= $this->Form->input('ejamaat_id', ['div' => false, 'class'=> 'form-control', 'label' => '', 'placeholder' => 'Ejamaat id', 'autocomplete' => 'off', 'type'=> 'text']) ?>
	                            <span class="input-group-addon"><i aria-hidden="true" class="fa user-icon"></i></span>
	                        </div>
	                        <div class="input-group">
	                            <?= $this->Form->input('email_address', ['div' => false, 'class'=> 'form-control', 'label' => '', 'placeholder' => 'Email Address', 'autocomplete' => 'off','type'=> 'text']) ?>
	                            <span class="input-group-addon"><i aria-hidden="true" class="fa user-icon"></i></span>
	                        </div>
	                        
	                        <div class="form-group">
	                          <div class="login-btn">
	                        	<?= $this->Form->submit('Reset Password', array('class'=> 'btn btn-primary btn-block')); ?>
	                          </div>
	                        </div>
	                   <?= $this->Form->end() ?>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 

<script>
 $(document).ready(function() {
	  $("#login").validate({
	     rules: {
         email_address: {
             // simple rule, converted to {required:true}
              required:true,
              email:true
         }, 
	     ejamaat_id: {
              required:true
         }  
     },
        messages: {
           email_address:"Email address is required.",
       	   ejamaat_id:"Ejamaat id is required."
		}
	}); 
});
</script>