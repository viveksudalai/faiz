<?php ?>
<div class="box-header">
   	<div class="col-md-12 text-right table-upper-row">
  		<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Users', 'action' => 'index']);?>
	</div>
</div>
<!-- /.box-header -->
<!-- Main content -->
<section class="content">
<div class="row">
     <div class="box col-lg-12">
		<div class="box-body table-responsive">          
			<ul class="nav nav-tabs tab-active">
				<li class="active" id="basicinfo"><a data-toggle="tab" href="#userInfo">Basic Information</a></li>
				 <?php if ($user->user_role != 3 ) { ?>
					 <!--li id="thaali-tab"><a data-toggle="tab" href="#thaali" class="thaali-tab">Thaali Information</a></li-->
				 <?php } ?>	 
				<?php if ($user->user_role == 3 || $user->user_role == 5) { ?>
					 <li><a data-toggle="tab" href="#driver">Vehicle Information</a></li>
				<?php } ?> 	 
			</ul>
			<?= $this->Form->create($user, ['id' => 'user', 'name' => 'user']) ?> 
				<div class="tab-content">	
				   <div id="userInfo" class="tab-pane fade in active tab-space">
						 <?php
								echo $this->Form->input('id', [ 'class'=> 'form-control']);
								echo $this->Form->input('ejamaatid', ['type' => 'text','class'=> 'form-control']);
								echo $this->Form->input('first_name', [ 'class'=> 'form-control']);
								echo $this->Form->input('middle_name', [ 'class'=> 'form-control']);
								echo $this->Form->input('last_name', [ 'class'=> 'form-control']);
								echo $this->Form->input('email_address', [ 'class'=> 'form-control']);
								echo $this->Form->input('secondary_email_address', [ 'class'=> 'form-control']);
								echo "<span>Seperate email addresses with a ','	</span>";
								echo $this->Form->input('home_phone', [ 'class'=> 'form-control']);
								echo "<span>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</span>";
								echo $this->Form->input('mobile_phone', [ 'class'=> 'form-control']);
								echo "<span>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</span>";
								echo $this->Form->input('mobile_carrier', ['options' => $mobileCarriers, 'empty' => 'Select Carrier', 'class'=> 'form-control']);
								echo $this->Form->input('zipcode', [ 'class'=> 'form-control']);
								echo $this->Form->input('password',[ 'class'=> 'form-control']);
								echo $this->Form->input('confirm_password', ['type' => 'password','class'=> 'form-control']);
								echo $this->Form->input('address', [ 'class'=> 'form-control']);
								echo $this->Form->input('city', [ 'class'=> 'form-control']);
								echo $this->Form->input('user_state', ['options' => $states,'class'=> 'form-control']);
								echo "<div class='input'>";
								echo $this->Form->label('User.Registration Type');
								echo "&nbsp;&nbsp;";
								echo $this->Form->radio('registration_type', [  
									['value' => '1', 'text' => 'Admin', 'class'=> '', 'checked' => 'checked'],
									['value' => '0', 'text' => 'User', 'class'=> '']
								]);
								echo '</div>';
								echo $this->Form->input('status', ['type'=>'hidden','value'=>0]);
								echo $this->Form->input('user_role', ['options' => $role,'class'=> 'form-control']);
						?>
						 <label for="color-code" class='color-code'>Color Code</label> 
						<div id="cp2" class="input-group colorpicker-component">
							<input type="text" id= "color-code" name= "color_code" value="<?= $user->color_code;?>" class="form-control" />
							<span class="input-group-addon"><i></i></span>
						</div>
						<?php //echo $this->Form->button(__('Submit')) ?>
						  <!--div class="col-lg-8 col-md-12 col-xs-12"-->
							<!--a data-toggle="tab" href="#thaali" class="btn btn-primary center-block pull-right" id="continue">Continue</a-->
						<!--/div-->
					<?php if ($user->user_role != 3 ) { ?>
						<!--div id="thaali" class="tab-pane fade tab-space"-->
							 <?php
									echo $this->Form->input('thaali_size', [ 'options' => $thaaliSize,'class'=> 'form-control']);
									echo $this->Form->input('delivery_method', ['options' => $deliveryMethod,'class'=> 'form-control']);
									echo $this->Form->input('driver_notes', ['class'=> 'form-control']);
									echo $this->Form->input('active_days', [ 'options' => $days,  'class'=> 'form-control', 'multiple'=> 'true']);
								?>
							<?= $this->Form->button(__('Submit')) ?>
							<!--a data-toggle="tab" href="#userInfo" class="btn btn-primary center-block pull-right" id="back">Back</a>
						 </div-->
					<?php } ?>
					
					</div>		
				</div>
				
			<?= $this->Form->end() ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.row -->  
</section>
     
  <script>
 $(document).ready(function() {
	 
      $.validator.setDefaults({ ignore: [] });
	  $("#user").validate({
	     rules: {
	         ejamaatid: {
	             required:true,
	             number:true
	         },
         first_name: {
             required:true,
             alpha:true,
             minlength:1
         },
         middle_name: {
             required:true,
             alpha:true
         },
         last_name: {
             required:true,
             alpha:true
         }, 
         email_address: {
             required:true,
             email:true
         },
       /*  secondary_email_address: {
        	 email:true,
        	 multiemail:true
         },*/
         home_phone: {
             required:true,
             phoneUS: true
         }, 
          mobile_phone: {
             required:true,
             phoneUS: true
         }, 
         mobile_carrier: {
             required:true             
         },
         zipcode: {
             required:true,
             zipcodeUS: true
         }, 
		 password: {
              required:true,
              minlength:5
         },
		 confirm_password: {
              required:true,
			  equalTo: "#password"
         },
          address: {
              required:true
         },
         
       /*  license_number: {
              required:true
         },
          license_expiry_date: {
              required:true
         },
          vehicle_make: {
              required:true
         },
          
	       vehicle_model: {
              required:true
         },
          vehicle_year: {
              required:true
         },
          insurance_provider: {
              required:true
         },
         insurance_policy_no : {
          required:true
         },
         insurance_expiry_date : {
          required:true
         },
         */
          thaali_size: {
              required:true
         },
          delivery_method: {
              required:true
         },
          driver_notes: {
              required:true
         },
          
	       thaali_size: {
              required:true
         },
          delivery_method: {
              required:true
         },
          driver_notes: {
              required:true
         } 
           
     },
        messages: {
        ejamaatid: { 
  			 required: "Ejamaatid is required.",
  			 number:"Accepts number only."
  		},
  		first_name: { 
  			 required: "First name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		middle_name: { 
  			 required: "Middle name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		last_name: { 
  			 required: "Last name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		email_address: { 
  			 required: "Email address is required.",
  			 email:"Enter valid email address."
  		},
  		/*secondary_email_address: { 
 			multiemail:"please use a comma to separate multiple email addresses."
 		},*/
 		
  		home_phone: { 
 			 required: "Home phone is required.",
 			 phoneUS:"Enter a valid Home number"
 		},
  		mobile_phone: { 
  			 required: "Mobile phone is required.",
  			 phoneUS:"Enter a valid Mobile number"
  		},
  		mobile_carrier: {
             required:"Mobile carrier is required."
         },
  		zipcode: { 
			 required: "Zipcode is required.",
			 zipcodeUS:"Enter a valid Zipcode"
		},  
  		password: { 
  			 required: "Password is required." 
  		},
        confirm_password: { 
  			 required: "Confirm Password is required."			 
  		}, 
  		address : { 
  			 required: "Address is required." 
  		},
  		
  		  thaali_size: {
              required: "Thaali size is required." 
         },
          delivery_method: {
               required: "Delivery method is required." 
         },
          driver_notes: {
              required: "Driver notes is required." 
         } 
         
		}
	}); 
	
	
   
 
	
	if($("#insurance_provider" ).val() != '') {
 		$('#driver-info').css('display', 'block');
	}
	
	$(function() {
		
			var dates = $("#insurance-expiry-date, #license-expiry-date").datepicker({
									minDate:'-0y',
							  // 	maxDate:'+0d',
							  dateFormat: 'yy-mm-dd',
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) {
					var option = this.id == "insurance_expiry_date" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
				
			});
		});
	
	/* $("#continue").click(function(){
		$('#thaali-tab').addClass('active');
		$('#basicinfo').removeClass('active');
	});

	$("#back").click(function(){
		$('#thaali-tab').removeClass('active');
		$('#basicinfo').addClass('active');
	}); */

	var role = $( '#user-role'  ).val(); 
		if (role != 3 && role != 5) {
			$('#cp2').hide();
			$('.color-code').hide();
		}
	 

	$(function() {
		$('#cp2').colorpicker();
	});

	$( '#user-role' ).change(function() {
		var role = $( this ).val(); 
		if (role != 3 && role != 5) {
		$('#cp2').hide();
		$('.color-code').hide();
		}
		else { $('#cp2').show(); 
		$('.color-code').show();}

	});
});
</script>
