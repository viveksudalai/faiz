<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
	</div>
</div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		     <h3> <?= __('View Login details') ?></h3>
		   <table id="loginHistory" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <tr>
		            <th><?= __('User') ?></th>
		            <td><?= $loginHistory->has('user') ? $this->Html->link($loginHistory->user->id, ['controller' => 'Users', 'action' => 'view', $loginHistory->user->id]) : '' ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Ip Address') ?></th>
		            <td><?= h($loginHistory->ip_address) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Id') ?></th>
		            <td><?= $this->Number->format($loginHistory->id) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('User Agent') ?></th>
		            <td><?= $this->Number->format($loginHistory->user_agent) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Created') ?></th>
		            <td><?= h($loginHistory->created) ?></td>
		        </tr>
		    </table>
		</div>
	</div>
</div>		

  
 
 