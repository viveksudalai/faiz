<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $loginHistory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $loginHistory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Login History'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="loginHistory form large-9 medium-8 columns content">
    <?= $this->Form->create($loginHistory) ?>
    <fieldset>
        <legend><?= __('Edit Login History') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('ip_address');
            echo $this->Form->input('user_agent');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
