   
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   	  <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'distribution_center', 'action' => 'index']);?>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($distributionCenter, ['id' => 'distributionCenter', 'name' => 'distributionCenter']) ?>
							    <fieldset>
							        <legend><?= __('New Distribution Center') ?></legend>
							         <?php
							            echo $this->Form->input('name', [ 'class'=> 'form-control']);
							            echo $this->Form->input('phone', [ 'class'=> 'form-control']);
								        echo $this->Form->input('mobile', [ 'class'=> 'form-control']);
								        echo $this->Form->input('address', [ 'class'=> 'form-control']);
								        echo '</div>';
       									echo "<div class='input'>";
       									echo $this->Form->label('Status');
       									echo "&nbsp;&nbsp;";
							            echo $this->Form->radio('status', [ 
								            ['value' => '0', 'text' => 'Inactive', 'class'=> ''],
								            ['value' => '1', 'text' => 'Active', 'class'=> '', 'checked' => 'checked']
	       									
       									]);
								       echo '</div>';
							         ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 <script>
	 $(document).ready(function() {
	  $("#distributionCenter").validate({
	     rules: {
	         	name: {required:true},
         		phone: {required:true,number: true},
         		mobile: {required:true,number: true},
         		address: {required:true}
        },
        messages: {
        			name: { 
  			 				required: "Thaali Name is required."
  						  },
  					phone: { 
  			 				required: "Phone number is required." 
  						  }, 
  					mobile : { 
  			 				required: "Mobile number is required."  
  						  }, 
  					address : { 
  			 				required: "Address is required." 
  						  } 
		}
	}); 
});
</script>
  