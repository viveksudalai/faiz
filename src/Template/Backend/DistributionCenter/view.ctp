	 
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                    <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'distribution_center', 'action' => 'index']);?>
                </div>
            </div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		    <h3> <?= __('View Distribution Center') ?></h3>
		   <table id="DistributionCenter" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <tr>
		            <th><?= __('Name') ?></th>
		            <td><?= h($distributionCenter->name) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Phone') ?></th>
		            <td><?= h($distributionCenter->phone) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Mobile') ?></th>
		            <td><?= h($distributionCenter->mobile) ?></td>
		        </tr>
		       <!-- <tr>
		            <th><?= __('Id') ?></th>
		            <td><?= $this->Number->format($distributionCenter->id) ?></td>
		        </tr>-->
		        <tr>
		            <th><?= __('Address') ?></th>
		        	<td><?= $this->Text->autoParagraph(h($distributionCenter->address)); ?></td>
		        </tr>
		         
		         <tr>
						            <th><?= __('Status') ?></th>
						            <td><?php switch ($distributionCenter->status) {
			          						 
			          							case '1': echo 'Active'; break;
			          							case '2': echo 'Inactive';break;
			          					}
			         				 ?></td>
						        </tr>
		       <tr>
		            <th><?= __('Created') ?></th>
		            <td><?= h($distributionCenter->created->format('l Y/m/d'))?></td>
		        </tr>
		        <tr>
		            <th><?= __('Modified') ?></th>
		            <td><?= h($distributionCenter->modified->format('l Y/m/d'))?></td>
		        </tr>
		        
		    </table>
		</div>
	</div>
</div>
