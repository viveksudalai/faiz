 
<div class="thaaliDelivery index large-9 medium-8 columns content">
    <h3><?= __('Thaali Delivery') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('driver_id') ?></th>
                <th><?= $this->Paginator->sort('thaali_id') ?></th>
                <th><?= $this->Paginator->sort('distribution_id') ?></th>
                <th><?= $this->Paginator->sort('delivery_date') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($thaaliDelivery as $thaaliDelivery): ?>
            <tr>
                <td><?= $this->Number->format($thaaliDelivery->id) ?></td>
                <td><?= $this->Number->format($thaaliDelivery->user_id) ?></td>
                <td><?= $thaaliDelivery->has('user') ? $this->Html->link($thaaliDelivery->user->first_name, ['controller' => 'Users', 'action' => 'view', $thaaliDelivery->user->id]) : '' ?></td>
                <td><?= $thaaliDelivery->has('thaali') ? $this->Html->link($thaaliDelivery->thaali->name, ['controller' => 'Thaali', 'action' => 'view', $thaaliDelivery->thaali->id]) : '' ?></td>
                <td><?= $thaaliDelivery->has('distribution_center') ? $this->Html->link($thaaliDelivery->distribution_center->name, ['controller' => 'DistributionCenter', 'action' => 'view', $thaaliDelivery->distribution_center->id]) : '' ?></td>
                <td><?= h($thaaliDelivery->delivery_date) ?></td>
                <td><?= h($thaaliDelivery->created) ?></td>
                <td><?= h($thaaliDelivery->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $thaaliDelivery->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $thaaliDelivery->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $thaaliDelivery->id], ['confirm' => __('Are you sure you want to delete # {0}?', $thaaliDelivery->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
