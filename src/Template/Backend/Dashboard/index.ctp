
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                
               <div class="box-body table-responsive">
                
		          <div class="col-lg-12">
			      <h2 class="text-left" style="font-size: 18px; font-weight: 600;"><?= __('Notifications') ?></h2>
			      <ul class="nav nav-tabs tab-active">
			        <li class="active">
			           <a data-toggle="tab" href="#home">Pending Users <?= ($recentUserCnt> 0) ? "(".count($recentUser).")":""; ?> </a>
			       </li>
			        <!--<li><a data-toggle="tab" href="#menu1">Thaali size change request</a></li>-->
			      </ul>
			      <div class="tab-content">
			        <div id="home" class="tab-pane fade in active tab-space">
			        <div class="table-responsive">
			          <table class="table edit-table">
			            <tbody>
			              <tr>
			                <th scope="col">Name</th>
			                <th scope="col">Ejamaat ID</th>
			                <th scope="col">Email </th>
			                <th scope="col">Registered On</th>
			                <th scope="col">Action</th>
			              </tr>
			              <?php foreach ($recentUser as $user): ?>
			                 <tr>
			                 	<td><?= h($user->full_name) ?></td>
			                 	<td><?= $user->ejamaatid ?></td>
			                 	<td><?= $user->email_address ?></td>
			                 	<td><?= $user->created->format('m/d/Y') ?></td>
			                 	<td>
			                 		<ul class="action">
			                    		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['controller' => 'users', 'action' => 'view', $user->id], ['escape' => false]) ?></li>
					                	<!-- <li><?= $this->Form->postLink(
	                                                 $this->Html->tag('i', '', ['class' => 'fa fa-check-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Approve']). "",
	                                                 ['controller' =>'users', 'action' => 'enable', $user->id],
	  												 ['escape'=>false, 'confirm' => __('Are you sure you want to approve this user?', $user->id)],
	                                                 ['class' => 'btn btn-mini']); ?></li>-->
					                	<li><?= $this->Form->postLink(
	                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete']). "",
	                                                 ['action' => 'delete', $user->id],
	  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)],
	                                                 ['class' => 'btn btn-mini']); ?></li>
			                  		</ul>
			                 	</td>
			                 </tr>
			              <?php endforeach; ?>
			              </tbody>
			          </table>
			        </div>
			        </div>
			        <div id="menu1" class="tab-pane fade tab-space">
			         <div class="table-responsive">
			          <table class="table edit-table">
			            <tbody>
			              <tr>
			                <th scope="col">Name</th>
			                <th scope="col">Current Size</th>
			                <th scope="col">Requested Size </th>
			              </tr>
			              <tr>
			                <td align="left">Mohammed Safakathusein Attarwala </td>
			                <td align="left">Small</td>
			                <td align="left">Large</td>
			             </tr>
			            </tbody>
			          </table>
			         </div>
			        </div>
			      </div>
			    </div>
      	        <div class="col-lg-12 top-spacing">
					  <div class="row">
					 
						<div class="col-lg-6">
						  <h4 class="text-left">Registered Users</h4>
						  <table class="table block-border">
							<thead>
							  <tr class="skyblue-bg">
								<th>Period</th>
								<th>Count</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
								<td align="left">Last 7 Days </td>
								<td align="left"><?= $thisWeekUser ?></td>
							  </tr>
							  <tr>
								<td align="left">Last 14 Days</td>
								<td align="left"><?= $lastTwoWeeksUser ?></td>
							  </tr>
							  <tr>
								<td align="left">Last Month</td>
								<td align="left"><?= $lastMonthUser ?></td>
							  </tr>
							  <tr>
								<td align="left">All time</td>
								<td align="left"><?= $allUsers?></td>
							  </tr>
							</tbody>
						  </table>
						</div>
						<div class="col-lg-6">
						  <h4 class="text-left">Delivered Household Tiffens</h4>
						  <table class="table block-border">
							<thead>
							  <tr class="skyblue-bg">
								<th>Status</th>
								<th>Household Tiffins</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
								<td align="left">Today</td>
								<td align="left"><?= $todaysDelivery ?></td>
							  </tr>
							  <tr>
								<td align="left">This Week</td>
								<td align="left"><?= $thisWeekDelivery ?></td>
							  </tr>
							  <tr>
								<td align="left">This Month</td>
								<td align="left"><?= $thisMonthDelivery ?></td>
							  </tr>
							  <tr>
								<td align="left">This Year</td>
								<td align="left"><?= $thisYearDelivery ?></td>
							  </tr>
							</tbody>
						  </table>
						</div>
						 
						</div>
					</div>
  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
 
     
 <script>  
     $(document).ready(function() {
   	 $('#allUsers, #newUsers').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 6 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</script>	
	