<?php if($loggedIn) : ?>
	 <div class="navbar-collapse collapse main-nav">
		  <div class="container">
		    <ul class="nav navbar-nav">
		           <li><?= $this->Html->link('Dashboard', ['controller' => 'dashboard', 'action' => 'index']);?></li>
		           <li><?= $this->Html->link('Users', ['controller' => 'users', 'action' => 'index']);?></li>
		           <li><?= $this->Html->link('Thaali', ['controller' => 'thaali', 'action' => 'index']);?></li>
		           <li><?= $this->Html->link('Miqats', ['controller' => 'miqat', 'action' => 'index']);?></li>
		           <li><?= $this->Html->link('Deliveries', ['controller' => 'deliveries', 'action' => 'index']);?></li>
		           <li><?= $this->Html->link('Caterer', ['controller' => 'caterer', 'action' => 'index']);?></li>
		          
		           <!-- <li><?= $this->Html->link('Distribution Centers', ['controller' => 'distribution-center', 'action' => 'index']);?></li>-->
		           
		           <li class="dropdown"><?= $this->Html->link('Utilities', ['controller' => 'report', 'action' => 'index'], ['class' => 'dropdown-toggle' , 'data-toggle' => "dropdown"]);?>
		           	   <ul class="dropdown-menu">
		           	   		 <li><?= $this->Html->link('Send Email Notification', ['controller' => 'email_notification', 'action' => 'index']);?></li>
		           	   		 <li><?= $this->Html->link('General Notification', ['controller' => 'notification', 'action' => 'index']);?></li>
		           	   		  <li><?= $this->Html->link('Add Mobile Carrier', ['controller' => 'mobile_carrier_list', 'action' => 'index']);?></li>
		           	   </ul>
		           </li>	
		           
		           <li class="dropdown"><?= $this->Html->link('Report', ['controller' => 'report', 'action' => 'index'], ['class' => 'dropdown-toggle' , 'data-toggle' => "dropdown"]);?>
		           	   <ul class="dropdown-menu">
		           	   		<li><?= $this->Html->link('User Report', ['controller' => 'report', 'action' => 'user']);?></li>
							<li><?= $this->Html->link('Delivery Report', ['controller' => 'report', 'action' => 'deliveryreport']);?></li>
							<li><?= $this->Html->link('Label Report', ['controller' => 'report', 'action' => 'userlabel']);?></li>
							<li><?= $this->Html->link('Household Summary Report', ['controller' => 'report', 'action' => 'householdreport']);?></li>
							<li><?= $this->Html->link('Delivery Summary Report', ['controller' => 'report', 'action' => 'deliverysummary']);?></li>
							<li><?= $this->Html->link('Contact Us Feeds', ['controller' => 'report', 'action' => 'contactfeeds']);?></li>
							<li><?= $this->Html->link('Survey Report', ['controller' => 'thaalisurvey', 'action' => 'view']);?></li>
		           	   </ul>
		           </li>	   
		          
		           <!--<li><?= $this->Html->link('Role', ['controller' => 'role', 'action' => 'index']);?></li>-->
		    </ul>
		  </div>
		</div>
 <?php endif;?>  