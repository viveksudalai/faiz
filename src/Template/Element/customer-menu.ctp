<?php if($loggedIn) : 
$session = $this->request->session();
$user_data = $session->read('Auth.User');
$role = $user_data['user_role'];
if ( $this->request->params['action'] != 'home' ) {
?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
       <ul class="nav navbar-nav">
       <li class="active"> <?php echo $this->Html->link($this->Html->image('dashboard_small.jpg', ['alt' => 'View Menu', 'class' => '']).'My Dashboard',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'dashboard'],
   				 	 ['escapeTitle' => false, 'title' => 'View Menu']
					);
        ?>
        </li>
        <?php if ($role != 3 ) { ?>
        <li>
        <?php echo $this->Html->link($this->Html->image('menus_small.jpg', ['alt' => 'View Menu', 'class' => '']).'View Menu',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'menus'],
   				 	 ['escapeTitle' => false, 'title' => 'View Menu']
					);
        ?>
        </li>
       <li> <?php echo $this->Html->link($this->Html->image('change_size_small.jpg', ['alt' => 'Change Size', 'class' => '']).'Change Size',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'changesize'],
   				 	 ['escapeTitle' => false, 'title' => 'Change Size']
					);
        ?>
        </li>
        <li> <?php echo $this->Html->link($this->Html->image('vacation_small.jpg', ['alt' => 'Vacation Planner', 'class' => '']).'Vacation Planner',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'vacation'],
   				 	 ['escapeTitle' => false, 'title' => 'Vacation Planner']
					);
        ?>
        </li>
        <li> <?php echo $this->Html->link($this->Html->image('survey_small.jpg', ['alt' => 'Survey for Thaali', 'class' => '']).'Survey for Thaali',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'survey'],
   				 	 ['escapeTitle' => false, 'title' => 'Survey for Thaali']
					);
        	?>
        </li>
        <li> <?php echo $this->Html->link($this->Html->image('drivers_small.jpg', ['alt' => 'Drivers', 'class' => '']).'Drivers',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'drivers'],
   				 	 ['escapeTitle' => false, 'title' => 'Drivers']
					);
        ?>
        </li>
         <!-- <li> <?php /* echo $this->Html->link($this->Html->image('6.jpg', ['alt' => 'Drivers', 'class' => '']).'Payment History',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'paymentHistory'],
   				 	 ['escapeTitle' => false, 'title' => 'Payment History']
					);*/
        ?>
        </li>-->
         
        <?php } ?>
        <li> <?php echo $this->Html->link($this->Html->image('contactus_small.jpg', ['alt' => 'Contact Faiz', 'class' => '']).'Contact Faiz',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'contactus'],
   				 	 ['escapeTitle' => false, 'title' => 'Contact Faiz']
					);
        ?>
        </li>
       <?php if ($role == 3 || $role == 5) { ?> 
        
         <li class="dropdown">
         <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Delivery <span class="caret"></span></a> -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <?php echo  $this->Html->image('deliveries_small.png', ['alt' => 'Deliveries', 'class' => '']);?>
            Delivery
          	<span class="caret"></span>
          </a>
         
           <?php 
               /*echo $this->Html->link(
           		      $this->Html->image('6.jpg', ['alt' => 'Deliveries', 'class' => '']).'Delivery',
    			 	 ['#'],
   				 	 ['escapeTitle' => false, 'title' => 'Deliveries', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'role' => 'button',  'aria-haspopup' => 'true', 'aria-expanded' => 'false']
					);*/
           ?>
          <ul class="dropdown-menu">
          <li> <?php echo $this->Html->link($this->Html->image('deliveries_small.png', ['alt' => 'Deliveries', 'class' => '', ]).'Deliveries',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'deliveries'],
   				 	 ['escapeTitle' => false, 'title' => 'Deliveries']
					);
        ?>
        </li>
         <li> <?php echo $this->Html->link($this->Html->image('delivery_statistics_small.png', ['alt' => 'Delivery Statistics', 'class' => '']).'Delivery Statistics',
    			 	 ['prefix' => 'customer','controller' => 'users', 'action' => 'deliveryStatistics'],
   				 	 ['escapeTitle' => false, 'title' => 'Delivery Statistics']
					);
        ?>
        </li>
        </a>
          </ul>
        </li> 
        <?php } ?>   
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php } ?>	 
 <?php endif;?>  
