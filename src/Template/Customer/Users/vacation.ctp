<?php $this->assign('title', 'Vacation Planner');?> 
<div class="main-content">
	<div class="row">
	<h1 class="col-md-6 col-xs-12 col-sm-4">Vacation Planner</h1>
	</div>
	<hr/>
	
<p class="text-error">* Adding/modifying vacation information will be considered valid only if the event is scheduled for after .<?= $min?>.</p>
<div class="row">
<div class="col-md-9 col-sm-10 col-xs-11 vacation-container">
 <?= $this->Form->create($userVacationPlanner, ['id' => 'vacation', 'name' => 'vacation']) ?> 											
	<div class="row">
	    <?= $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]);?>
		 <div class="col-md-4 form-group "> 
		    <label>From Date</label> 
		  	<div class="input-group date">  
		  	    <?php echo $this->Form->input('start_date', ['class'=>'datepicker form-control','type'=> 'text', 'label' => false,'style'=>'width:84%']);?>
		    	<!--span class="add-on" style="padding-left:25px"><?php //echo $this->Html->image('calendar.png'); ?></span-->
		 	</div>
		 </div>   
		 <div class="col-md-4 form-group">  
		  <label>To Date</label> 
		  	<div class="input-group date">
				<?php echo $this->Form->input('end_date', ['class'=>'datepicker form-control','type'=> 'text', 'label' => false,'style'=>'width:84%']); ?>
				<!--span class="add-on" style="padding-left:25px"><?php //echo $this->Html->image('calendar.png'); ?></span-->
		 	</div>
		 </div>   
		<div class="col-md-12 form-group"> 
		 <?php echo $this->Form->input('title', ['class'=> 'form-control']); ?>
		 </div>
		<div class="col-md-12 form-group"> 
		 <?php echo $this->Form->input('driver_notes',['class'=> 'form-control']); ?>
		 </div>
	</div>
	  <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-custom']);?>
	  <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'dashboard'], ['class' => 'btn btn-grey']);?>	  
	  <?= $this->Form->end() ?>
	</div>
</div>
</div> 


		 
  <script>
  
  $(function() {
			
				var dates = $("#start-date, #end-date").datepicker({
					showOn: "both",
					minDate:'+7',
					//maxDate:'+0d',
					dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					buttonImage: "<?php echo $this->Url->image('calendar.png')?>",
					buttonImageOnly: true,
					allowInputToggle: true,
					onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#vacation").validate({
	     rules: {
	    	 	driver_notes: {required:true},
	         	title: {required:true},
         		start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			title: { 
  			 				required: "Title Name is required."
  						  },
  					 driver_notes: { 
  			 				required: "Driver notes is required." 
  						  }, 
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 
});
</script>
  
  
  