<?php $this->assign('title', 'Thali Menu');?>
<div class="main-content menu">
	<div class="row">
		<h1 class="col-md-6 col-xs-12 col-sm-4">Thali Menu</h1>
		<div class="text-right col-md-6 col-xs-12 col-sm-8">
			<span class="text-green">Your Default Size is <?= $thaali_size?> </span> 
		</div>
	 </div>
 	<hr/>
	<div class="row">
	<div class="col-md-12">
	<div id="events-calendar" class="box jplist">
	    <div id="calendar_menu"></div>
	    </div>  
	</div>
	</div>
</div>
 <script>
 if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	 var cView = 'listWeek';
 }
 else 
	 var cView = 'month';
 	 
 $(function() {
   $('#calendar_menu').fullCalendar({
					header: {
					left: 'title',
					center: '',
					right:'prev,next'
					},
					defaultView: cView,
					displayEventTime: false,
					defaultDate: '<?= date("Y-m-d");?>',
					editable: true,
				/*	columnFormat: {
						  month: 'ddd',
						  week: 'MMM dd',
						  day: 'MMM dd'		
					}, */
				   eventRender: function (event, element)
					{
					   element.find('.fc-title').html(element.find('.fc-title').text());	
					   element.find('.fc-list-item-title').html(element.find('.fc-list-item-title').text());
					},
					eventLimit: true,
					events: {
						url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'getCustomercalendar','_full' => true )); ?>',
						type: 'POST', // Send post data
						error: function() {
							alert('There was an error while fetching events.');
						}
					},
	      });
 });

// $('#calendar_menu').fullCalendar('changeView', 'listWeek');
</script>