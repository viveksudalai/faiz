<?php $this->assign('title', 'Delivery Statistics');?> 
<div class="main-content drivers">
	<div class="row">
		<h1 class="col-md-6 col-xs-12 col-sm-4">Delivery Statistics</h1>
	</div>		
 <hr/>
 
		<div class="row">
		    <div class="col-md-11 col-sm-11 col-xs-11 table-responsive">
		    		 
									Overall Summary
									<table class="table table-striped table-bordered" cellspacing="0" width="100%">
										<tr>
											<td>Status</td>
											<td>Today</td>
											<td>This Week</td>
											<td>This Month</td>
											<td>This Year</td>
											<td>All Time</td>
										</tr>
										<tr>
											<td>Household Tiffins</td>
											<td><?= $todaysDelivery;?> </td>
											<td><?= $thisWeekDelivery;?></td>
											<td><?= $thisMonthDelivery;?></td>
											<td><?= $thisYearDelivery;?></td>
											<td><?= $allTimeDelivery;?></td>
										</tr>
									</table>
									
							  		Monthly Totals for <?= date("Y"); ?>
									<table class="table table-striped table-bordered" cellspacing="0" width="100%">
										<tr>
											<td>Status</td>
											<?php foreach ($monthsDelivery as $key => $value):  ?>
											<td><?= date('M', mktime(0,0,0,$key))  ?></td>
											<?php endforeach; ?> 
										<tr>
										<td>Household Tiffins</td>
										<?php 
											 
											foreach ($monthsDelivery as $months):
												echo "<td>".$months."</td>";					 
											endforeach; 
										?>
										
										</tr>
									</table>
								</div>
							</div>
  </div>
 
	 