<?php $this->assign('title', 'Edit Profile');?> 
<div class="main-content edit-profile">
	<h1>Edit Profile</h1>
	<!-- <div class="row">
	<div class="col-md-11 col-sm-11 col-xs-11 grey-container">
		<form role="form" class="form-inline">
		
		<div>
		    <label for="name">Please enter Ejamaat ID below if you wish to autopopulate user information</label>  
		  </div>
		    <input type="text" class="form-control" id="id">
		  <button type="submit" class="btn btn-populate">Auto Populate</button>
		
		</form>
	</div>
</div> -->
<div class="row">
	<div class="col-md-11 col-sm-11 col-xs-11">
	
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#userInfo" aria-controls="userInfo" role="tab" data-toggle="tab">Basic Information</a></li>
	    <?php if ($user->user_role != 3 ) { ?>
	    	<li role="presentation"><a href="#thaali" aria-controls="thaali" role="tab" data-toggle="tab">Thali Information</a></li>
	    <?php } ?>	
	  </ul>
	
	  <!-- Tab panes -->
	<?= $this->Form->create($user, ['id' => 'user', 'name' => 'user'])?>
		<?php echo $this->Form->input('id', [ 'class'=> 'form-control']);
	    	  echo $this->Form->input('ejamaatid', [ 'class'=> 'form-control','disabled', 'type' =>'hidden']); 
	    	  echo $this->Form->hidden('password',[ 'class'=> 'form-control']);
	    ?> 
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="userInfo">
	    <div class="row">
	      <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php  echo $this->Form->input('first_name', [ 'class'=> 'form-control']); ?>
	  </div>
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	   <?php   echo $this->Form->input('middle_name', [ 'class'=> 'form-control']); ?>
	  </div>
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	   <?php echo $this->Form->input('last_name', [ 'class'=> 'form-control']); ?>
	  </div>
	  </div>
	  <div class="row">
	   <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php  echo $this->Form->input('email_address', [ 'class'=> 'form-control']); ?>
	  </div>
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	   <?php   echo $this->Form->input('secondary_email_address', [ 'class'=> 'form-control']); 
	    echo "<div>Seperate email addresses with a ','	</div>";?>
	  </div>
	    <div class="form-group col-md-3 col-sm-4 col-xs-6">
	   <?php echo $this->Form->input('home_phone', [ 'class'=> 'form-control']); 
	   echo "<div>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</div>";
	   ?>
	  </div>
	  </div>
	   <div class="row">
	 
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php echo $this->Form->input('mobile_phone', [ 'class'=> 'form-control']);
			echo "<div>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</div>";
	    ?>
	  </div>
	   <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php  echo $this->Form->input('mobile_carrier', ['options' => $mobileCarriers, 'empty' => 'Select Carrier', 'default'=>$mobile_carrier, 'class'=> 'form-control']);?>
	  </div>
	    <div class="row">
		<div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php  echo $this->Form->input('address', [ 'class'=> 'form-control', 'type' => 'text']); ?>
	  </div>
	  </div>
	
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php   echo $this->Form->input('city', [ 'class'=> 'form-control']); ?>
	  </div>
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php echo $this->Form->input('user_state', ['options' => $states, 'default'=>$state_id, 'label' => 'States', 'class'=> 'form-control']);?>
	  </div>
	  <div class="form-group col-md-3 col-sm-4 col-xs-6">
	    <?php  echo $this->Form->input('zipcode', [ 'class'=> 'form-control']); ?>
	  </div>
	</div>
	<div class="row">
	  <div class="form-group col-md-12 col-sm-12 col-xs-12">
	  <button type="submit" class="btn btn-custom">SAVE</button>
	
	   <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'dashboard'], ['class' => 'btn btn-grey']);?>	
	</div>
	</div>
	 </div>  
	      <?php if ($user->user_role != 3 ) { 
	      	$activeDays = explode(',', $active_days);
	      	echo $this->Form->hidden('thaali_id', ['default'=> $thaali_id, 'class'=> 'form-control']);
	      ?>
	    <div role="tabpanel" class="tab-pane" id="thaali">
	    <div class="row">
	    <div class="form-group col-md-3 col-sm-6 col-xs-8">
	    <?php echo $this->Form->input('thaali_size', [ 'options' => $thaaliSize, 'default'=> $thaali_size,'class'=> 'form-control']);?>
	  </div>
	      <div class="form-group col-md-3 col-sm-6 col-xs-8">
	    <?php echo $this->Form->input('delivery_method', ['options' => $deliveryMethod,  'default'=>$delivery_method, 'class'=> 'form-control']); ?>
	  </div>
	  </div>
	  <div class="row">
	  <div class="form-group col-md-3 col-sm-3 col-xs-8">
	    <?php  echo $this->Form->input('driver_notes', ['default'=>$driver_notes,  'class'=> 'form-control']); ?>
	  </div>
	  <div class="form-group col-md-3 col-sm-3 col-xs-8">
	    <?php //echo $this->Form->input('active_days', [ 'options' => $days,  'class'=> 'form-control', 'multiple'=> 'true', 'default'=>$activeDays]);?>
	     <?php echo $this->Form->input('active_days', array('type'=>'select', 'multiple'=>'checkbox', 'options'=>$days, 'name' => 'active_days', 'default'=>$activeDays));?>
	  </div>
	 
	  </div>
	<div class="row">
	  <div class="form-group col-md-12 col-sm-12 col-xs-12">
	  <button type="submit" class="btn btn-custom">SAVE</button>
	
	   <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'dashboard'], ['class' => 'btn btn-grey']);?>	
	</div>
	</div>
	    </div>
	    
	    <?php } ?>
	     
	  </div>
	</div>
	</div>
</div>
	 <?= $this->Form->end() ?>
  <script>
 $(document).ready(function() {
	  $("#user").validate({
	     rules: {
	         ejamaatid: {
	             required:true,
	             number:true
	         },
         first_name: {
             required:true,
             alpha:true,
             minlength:1
         },
         middle_name: {
             required:true,
             alpha:true
         },
         last_name: {
             required:true,
             alpha:true
         }, 
         email_address: {
             required:true,
             email:true
         },
        /*secondary_email_address: {
        	 multiemail:true
         },*/
          mobile_phone: {
             required:true,
             phoneUS:true
         },
         zipcode: {
             required:true,
             zipcodeUS: true
         }, 
          address: {
              required:true
         },
         ,
		 "active_days[]" : {required:true, minlength:1},
        /* license_number: {
              required:true
         },
          license_expiry_date: {
              required:true
         },
          vehicle_make: {
              required:true
         },
          
	       vehicle_model: {
              required:true
         },
          vehicle_year: {
              required:true
         },
          insurance_provider: {
              required:true
         },
         insurance_policy_no : {
          required:true
         },
         insurance_expiry_date : {
          required:true
         },
         */
          thaali_size: {
              required:true
         },
          delivery_method: {
              required:true
         },
          driver_notes: {
              required:true
         } 
           
     },
        messages: {
        ejamaatid: { 
  			 required: "Ejamaatid is required.",
  			 number:"Accepts number only."
  		},
  		first_name: { 
  			 required: "First name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		middle_name: { 
  			 required: "Middle name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		last_name: { 
  			 required: "Last name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		email_address: { 
  			 required: "Email address is required.",
  			 email:"Enter valid email address."
  		},
  		/*secondary_email_address: { 
 			multiemail:"please use a comma to separate multiple email addresses."
 		},*/
  		mobile_phone: { 
  			 required: "Mobile phone is required.",
  			 phoneUS:"Enter a valid Mobile number"
  		},
  		zipcode: { 
			 required: "Zipcode is required.",
			 zipcodeUS:"Enter a valid Zipcode"
		}, 
  		address : { 
  			 required: "Address is required." 
  		},
  		
  		  thaali_size: {
              required: "Thaali size is required." 
         },
          delivery_method: {
               required: "Delivery method is required." 
         },
          driver_notes: {
              required: "Driver notes is required." 
         },
          thaali_size: {
              required: "Thaali size is required." 
         },
          delivery_method: {
               required: "Delivery method is required." 
         },
          driver_notes: {
              required: "Driver notes is required." 
         }  ,
		 "active_days[]" : { required: "Choose at least one day.",  minlength:"Choose at least one day."}
         
		}
	}); 
	
	
  /* $("#thaaliinfo").validate({
	    
           
     },
        messages: {
       	
  		 
         
		}
	}); */
 
	
	if($("#insurance_provider" ).val() != '') {
 		$('#driver-info').css('display', 'block');
	}
	
	$(function() {
		
			var dates = $("#insurance-expiry-date, #license-expiry-date").datepicker({
									minDate:'-0y',
							  // 	maxDate:'+0d',
							  dateFormat: 'yy-mm-dd',
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) {
					var option = this.id == "insurance_expiry_date" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
				
			});
		});
	});

$( "#show-driver-info" ).click(function() {
	$('#driver-info').css('display', 'block');
});

</script>

 
<style>
#driver-info {display:none}
</style>
 