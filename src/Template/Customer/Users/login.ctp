<?php $this->assign('title', 'Login'); ?>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="login-bg">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 login-logo text-center">
                     <?php echo $this->Html->image('logo-big.png', ['alt' => 'Faizchicago', 'class' => '']); ?>
                    </div>
                  </div>
                  <div class="container-fluid">
                  <div class="row">
                    <div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-12 col-xs-12 login-content">
                          <div class="inner-container row">

                            <div class="text-center">
                              <div class="login-top">
                                 <?php echo $this->Html->image('mem-icon.png', ['alt' => 'Faizchicago', 'class' => '']); ?>
                                <h4>MEMBER LOGIN</h4>
                                 <?= $this->Flash->render() ?>
                              </div>
                              <?= $this->Form->create('Users', ['id' => 'login', 'name' => 'login', 'class' => 'col-md-12 col-sm-12 col-xs-12 text-center']) ?>
                              <div class="form-group">
                                <input type="text" id="email-address" class="form-control" name="email_address" placeholder="Email-Id" autocomplete="off"/>
                              </div>
                             <input type="password" id="password" class="form-control" name="password"  placeholder="Password" autocomplete="off"/>
                              <?= $this->Form->submit('Login', array('class'=> 'btn btn-orange')); ?>  <br/><br/>
                              <?= $this->Html->link('Forgot your password?',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'forgotPassword']);?><br/>
                              <?= $this->Html->link("REGISTER NOW",['prefix' => 'customer', 'controller' => 'Users', 'action' => 'registration'],['class' => 'btn btn-dashboard nh']);?>
					       	  </form>
                            </div>
                       
                           </div>                     
                    </div>

                    <div class="col-md-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12 login-content-right">
                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="5000">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                          <div class="item active">
                           <?php echo $this->Html->image('slider-1.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
                          </div>
                          <div class="item">
                             <?php echo $this->Html->image('slider-2.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
                          </div>
                           <div class="item">
                             <?php echo $this->Html->image('slider-3.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
                          </div>
                        </div>

                        
                      </div>
                      

          <div class="col-md-12 col-sm-12 col-xs-12 menu-header">

           <div class="col-md-6 col-sm-6 col-xs-4 head">THAALI MENU</div>
               <div class="col-md-6 col-sm-6 col-xs-8 text-right date"><?= $weekstart. " to ". $weekend?></div>
            </div>

           <div class="row">
           <?php if (count($thisWeekCalendar) > 0) { ?>
             <div class="menu-section col-md-12 text-center">
              <?php 
							$i = 0;
							$offset = '';
							foreach ($thisWeekCalendar as $thaali):
							if ($i == 0)
								 $offset = "col-md-offset-1";
							else $offset = '';
			 ?>
                <div class="menu-box col-sm-12">

              <h4><?= date('l m/d/Y', strtotime($thaali['date']))?></h4>
              
              <div class="<?= ($thaali['type'] == 'Miqat'? 'menu-err' : '')?> menu-name">
              	<?= ucwords($thaali['title'])?>
              	<?php if ($thaali['type'] == 'Miqat') { ?>
              	<?= ucwords($thaali['details'])?>
              	<?php } ?>
              </div>
              <div class="caterer-info">
              <?php if ($thaali['type'] != 'Miqat') { ?>
              	<?= ($thaali['type'] != 'Miqat'? 'Caterer:' : '')?>  <?= ucwords($thaali['details'])?>
              <?php } ?>
              </div>
            </div>
            <?php $i++; endforeach;?>

             </div>
               <?php } else { ?>
               <div class="menu-section col-md-12 text-center">
   					<div class="menu-box col-sm-12">
 						<h4>No Thaali scheduled for this week.</h4>
               		</div>
   			</div>
            
               <?php } ?>
                    </div>


      
 
                  </div>

                </div>

</div>

              </div>

            </div>

             </div>
           </div>
          
          
 
<script>
 $(document).ready(function() {
	  $("#login").validate({
	     rules: {
         email_address: {
             // simple rule, converted to {required:true}
              required:true 
         }, 
	     password: {
              required:true
         }  
     },
        messages: {
           email_address:"Email address is required.",
       	   password:"Password is required."
		}
	}); 
});
</script>
 