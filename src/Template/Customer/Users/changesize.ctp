<?php $this->assign('title', 'Change Thaali Size');?>
<div class="main-content menu">
	<div class="row">
		<h1 class="col-md-6 col-xs-12 col-sm-4">Change Thali Size</h1>
		<div class="text-right col-md-6 col-xs-12 col-sm-8">
		 <span class="text-green">Your Default Size is <?= $thaali_size?></span> 
		</div>
	</div>
	<hr/>
		 
<div class="row">
<div class="col-md-12">
<div id="events-calendar" class="box jplist">
    <div id="changesize"></div>
      <div id="calendar_menu"></div>
	    </div>  
	</div>
	</div>
</div>
 <div id="size_dialog" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">      
     	<?= $this->Form->create('', ['id' => 'delivery', 'name' => 'delivery', 'url' => ['controller' => 'Users', 'action' => 'editThaaliDelivery']]); ?>
        <div class='title'><b>Change Thali Size</b> </div>
        <div class="modal-body">
      	</div>
      <div class="modal-footer">
        <button type="button" id="formsub" class="btn btn-custom" data-dismiss="modal">SUBMIT</button>
        <button type="button" class="btn btn-grey" data-dismiss="modal">CANCEL</button>
      </div>
        <?= $this->Form->end() ?>  
    </div>

  </div>
</div>
<script>

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	 var cView = 'listWeek';
}
else 
	 var cView = 'month';
	 
	$(document).ready(function() {
	if($('#changesize').length) {  
			var calendar = $('#changesize').fullCalendar({
				header: {
				left: 'title',
				center: '',
				right:'prev,next'
				},
				defaultView: cView,
				displayEventTime: false,
			    defaultDate: '<?= date("Y-m-d")?>',
				editable: true,
				/*columnFormat: {
					  month: 'dddd',  
					  day: 'MMM dd' 
					  },*/
			   eventRender: function (event, element)
				{
					element.find('.fc-title').html(element.find('.fc-title').text());
					element.find('.fc-list-item-title').html(element.find('.fc-list-item-title').text());
				},
				eventLimit: true,
				events: {
					url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'getCustomereditcalendar','_full' => true )); ?>',
					type: 'POST', // Send post data
					error: function() {
						alert('There was an error while fetching events.');
					}
				},
				
			});
		}
	});
 
	 $("#formsub").click(function( event ) {
		 // event.preventDefault();
		  var formUrl = $(this).attr('action'); 
		  var post_data = $("#delivery").serialize();  
		  $.ajax({
			    type : 'POST',
			    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'editThaaliDelivery','_full' => true )); ?>',
				data : post_data,
			    success: function(response) {  
			    	$('#changesize').fullCalendar( 'refetchEvents' );
			        alert("Thaali details has been updated successfully");
		    },
		    error: function (xhr, textStatus, errorThrown)	{
             	alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
			});
		});
	 
	

	function userEdit(id) {
		 var formUrl = $(this).attr('action'); 
		  
		  $.ajax({
			    type : 'POST',
			    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'getUserThaaliDeliveryById','_full' => true )); ?>',
				data : {"id":id},
			     success: function(response) {  
			    	 $('.modal-body').html(response);
		    },
		    error: function (xhr, textStatus, errorThrown)	{
           	alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
			});
		 $('#size_dialog').modal('toggle');
	}
 </script>