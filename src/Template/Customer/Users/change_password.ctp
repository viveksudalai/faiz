<?php $this->assign('title', 'Change Password'); ?>
<div class="main-content">
<h1>Change Password</h1>
<hr/>
<div class="row">
<div class="col-md-8 vacation-container">
 
  										
											
<?= $this->Form->create($user, ['id' => 'user', 'name' => 'user']) ?> 											
	<div class="row">
	    <div class="form-group">
	     <?php
		  echo $this->Form->input('Password', ['type' => 'password', 'id' =>'password', 'name' => 'password', 'class'=> 'form-control']);
		  echo $this->Form->input('Confirm Password', ['type' => 'password', 'id' =>'confirmPassword', 'name' => 'confirmPassword', 'class'=> 'form-control']);
		 ?>
		</div>
	</div>
	  <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-custom']);?>
	  <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'dashboard'], ['class' => 'btn btn-grey']);?>	  
	  <?= $this->Form->end() ?>
	</div>
</div>
</div> 

<script>
 $(document).ready(function() {
	  $("#user").validate({
	     rules: {
         password: {
              required:true,
              minlength:5
         }, 
	     confirmPassword: {
              required:true,
              qualTo: "#password"
         }  
     },
        messages: {
           password:"Password is required.",
       	   confirmPassword: 
       	   	{ 
       	   	  required: "Confirm password is required.",
       	   	  qualTo  : "Password & confirm password should be same"
       	   	} 
		}
	}); 
});
</script>