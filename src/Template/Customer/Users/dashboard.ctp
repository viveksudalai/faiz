<?php $this->assign('title', 'Dashboard');?> 
<div class="main-content dashboard">

<h1>My Dashboard</h1>
<div class="text-right" style="margin-top:-10px">
  Status: <span class="text-green">
  <?php switch ($user->status) {
	    case '0': echo 'New';break;
        case '1': echo 'Active'; break;
        case '2': echo 'Inactive';break;
}
?>
</span> 
Role : <span class="text-green"><?= h($user->role->name) ?></span>
  <!-- Account Status: <span class="text-green">Active</span> -->
</div>

<hr/>
<div class="row">

<div class="col-md-8 col-sm-6 col-xs-12">
<div class="row">
<div class="col-md-8 info-text"><h3><?= ucwords($user->full_name) ?></h3></div>
	<div class="col-md-4 text-right">
		<?= $this->Html->link('Change Password',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'changePassword', $user->id], ['class'=>'btn btn-dashboard']);?>
		<?= $this->Html->link('Edit Profile',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'edit', $user->id], ['class'=>'btn btn-dashboard']);?>
	</div>
</div><br />
<section class="row blue-box">
 <div class="col-md-6">Ejamaat Id : <span class="info-text"><?= h($user->ejamaatid) ?></span></div>
 <div class="col-md-6">Home Phone : <span class="info-text"><?= h($user->home_phone) ?></span></div>
 <div class="col-md-6">Mobile : <span class="info-text"><?= h($user->mobile_phone) ?></span></div>
 <div class="col-md-6">Mobile Carrier : <span class="info-text"><?= h($user->mobile_carrier_list->carrier_name) ?></span></div>
 <div class="col-md-6">Email : <span class="info-text"><span class="info-text"><?= h($user->email_address) ?></span></div>
 <div class="col-md-6">Secondary Email : <span class="info-text"><span class="info-text"><?= h($user->secondary_email_address) ?></span></div>
 
 <div class="col-md-6">Address : 
	 <span class="info-text">
	 <?= h($user->address) ?>,
	 <?= h($user->city) ?>, 
	 <?= h($user->state->name) ?>
	 <?= h($user->zipcode) ?>
	</span>
</div>
<?php if ($user->user_role == 3 || $user->user_role == 5) { ?> 
	<div class="col-md-6">Color Code : 
		<span class="info-text"><span class="info-text" style="background-color:<?= h($user->color_code); ?>">
			&nbsp;&nbsp;&nbsp;
		</span>
	</div>
<?php }?>
</section>
<br />
<div class="row">
	<div class="col-md-9 info-text"><h3>Thaali Request Information</h3></div>
</div>
<br />
	<section class="row blue-box">
		<div class="col-md-6">Default Thali Size  : 
			<span class="info-text">
				<?php switch ($thaali_size) {
					 case '0': echo 'None';break;
					 case '1': echo 'Small (1-2 servings)'; break;
					 case '2': echo 'Medium (3-4 Servings)';break;
					 case '3': echo 'Large (5-6 Servings)';break;
					 case '4': echo 'X-Small (Salawat)';break;
					 case '5': echo 'X-Large';break;
				 }
			   ?>
			</span>
		</div>
	 	
	 	<div class="col-md-6">Delivery Method :
	 		<span class="info-text">
	 			<?php switch ($delivery_method) {
					case '1': echo 'Pick Up'; break;
					case '2': echo 'Delivery';break;
					}
				?>
	 		</span>
	 	</div>
	 	<div class="col-md-6"> Active Days : 
	 		<span class="info-text"> 	
	 			<?php 
					$active = '';
					$activeDays = $active_days; 
                    if(!empty($activeDays)) {
					$activeDays = (explode(",",$activeDays)); 
					$days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
					foreach ($activeDays as $value) {
						$active.=  $days[$value].",";
						}
					$active = substr($active,0,-1);
					}   
					echo $active;
			?>
	 	    </span>
	 	</div>
	  	<div class="col-md-6"> Default Driver Notes : <span class="info-text"><?= ucwords($driver_notes) ?></span></div>
	 	<div class="col-md-6">Driver : <span class="info-text"><?= ucwords($driverName) ?></span></div>
	 	<div class="col-md-6">Monthly Niyaz Contribution :<span class="info-text">NA</span></div>
	 	<div class="col-md-6">Delivery Charge :<span class="info-text"> NA</span></div>
</section>

</div>

<div class="col-md-4 col-sm-6 col-xs-12">
<div class="row"><div class="col-md-12 info-text"><h3>Thaali Statistics</h3></div></div><br />
<section class="row grey-box">
<div class="col-md-12">
  <div class="bold">Weekly Totals</div> <div>(<?= $weekstart. " - ". $weekend?>)</div>
  <hr />
</div>
<?php foreach ($weekDelivery as $key => $value): ?>
<div class="col-md-6"><?= $key?></div><div class="col-md-6 text-right"><?= $value?></div>
<?php endforeach;?>
<br /><br />
<div class="col-md-12 bold" style="margin-top:8px">Overall 
  <hr/>
</div>
<div class="col-md-6">Today</div><div class="col-md-6 text-right"><?= $todaysDelivery?></div>
<div class="col-md-6">This Week</div><div class="col-md-6 text-right"><?= $thisWeekDelivery?></div>
<div class="col-md-6">This Month</div><div class="col-md-6 text-right"><?= $thisMonthDelivery?></div>
<div class="col-md-6">This Year</div><div class="col-md-6 text-right"><?= $thisYearDelivery?></div>
</section>

</div>
</div>

</div>
</div>