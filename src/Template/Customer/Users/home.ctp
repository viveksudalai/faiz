<?php if (!empty($info)) {?>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="announcements-section">
				<div class="horn-circle">
				   <i class="fa fa-bullhorn" aria-hidden="true"></i>
				</div>
			   <div class="announcements-text"> Announcements: <?= $info ?></div>
			</div>
	</div>		
</nav>
<?php } ?>
<div class="main-content home">
	<div class="row">
		<div class="col-md-offset-3 col-md-6 home-container">
			<div class="row">
				 <?php if ($role != 3 ) { ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('menus.jpg', ['alt' => 'View Menu', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'menus'],
   				 	 	 ['escapeTitle' => false, 'title' => 'View Menus', 'class' => '']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('View Menu',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'menus'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'View Menus', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>	
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('changesize.jpg', ['alt' => 'Change Size', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'changesize'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Change Size']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Change Size',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'changesize'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Change size', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('vacation.jpg', ['alt' => 'Vacation Planner', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'vacation'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Vacation Planner']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Vacation Planner',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'vacation'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Vacation Planner', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('edit_profile.jpg', ['alt' => 'Edit Profile', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'edit', ],
   				 	 	 ['escapeTitle' => false, 'title' => 'Edit Profile']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Edit Profile',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'edit'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Edit Profile', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				 <?php if ($role != 3 ) { ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('survey.jpg', ['alt' => 'Survey for Thaali', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'survey'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Survey for Thaali']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Survey for Thaali',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'survey'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Survey for Thaali', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('contactus.jpg', ['alt' => 'Contact Faiz', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'contactus'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Contact Faiz']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Contact Faiz',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'contactus'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Contact Faiz', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				 <?php if ($role != 3 ) { ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('drivers.jpg', ['alt' => 'Drivers', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'drivers'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Drivers']
						);
        				?>
					 
						<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Drivers',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'drivers'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Drivers', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>	
				</div>
			 <?php } ?>
			 <?php if ($role == 3 || $role == 5) { ?> 
			 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('deliveries.png', ['alt' => 'Deliveries', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'deliveries'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Deliveries']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Deliveries',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'deliveries'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Deliveries', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('delivery_statistics.png', ['alt' => 'Delivery Statistics', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'deliveryStatistics'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Delivery Statistics']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Delivery Statistics',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'deliveryStatistics'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Delivery Statistics', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
			 <?php } ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('dashboard.jpg', ['alt' => 'Dashboard', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'dashboard'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Dashboard']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Dashboard',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'dashboard'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Dashboard', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
					<div class="thumbnail"> 
						<?php echo $this->Html->link($this->Html->image('logout.jpg', ['alt' => 'Logout', 'class' => '']).'',
    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'Logout'],
   				 	 	 ['escapeTitle' => false, 'title' => 'Logout']
						);
        				?>
        				<div class = 'caption'>
        					<h4>
	        				<?php echo $this->Html->link('Logout',
	    			 		 ['prefix' => 'customer','controller' => 'users', 'action' => 'Logout'],
	   				 	 	 ['escapeTitle' => false, 'title' => 'Logout', ]
							);
	        				?>
	        				</h4>
						</div>
					</div>
				</div>
				 
			</div>
		</div>
	</div>
</div>