<?php $this->assign('title', 'Deliveries');?> 
<div class="main-content drivers">
	<div class="row">
		<h1 class="col-md-6 col-xs-12 col-sm-4">Deliveries</h1>
	</div>		
 <hr/>
 
 <div class="row">
 <form name="frmexplist" id="frmexplist" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'users','action'=>'deliveries','_full' => true )); ?>" method="POST">
		 
            <div class="input-group form-group">
				  <span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-calendar"></i>
					</span> 
					<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>
				<div class="input-group form-group">
					<span class="input-group-addon" id="basic-addon1">
						<i	class="fa fa-calendar"></i>
					</span>
					<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
				</div>				
				<div class="form-group">
					<label class="" for="">&nbsp;</label>
						<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
				</div>
		</form>
 </div>
		<div class="row">
		    <div class="col-md-11 col-sm-11 col-xs-11 table-responsive">
		    		<table id="driverThaaliDeliveries" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr class="skyblue-bg">
												<th><?= 'Date' ?></th>
												<th><?= 'User Name' ?></th>
												<th><?= 'Mobile No' ?></th>
												<th><?= 'Address' ?></th>
												<th><?= 'Size' ?></th>
												<th><?= 'Delivery Notes' ?></th>
											</tr>
										</thead>
										<tbody> 
								        <?php foreach ($driverDeleiveryInfo as $delivery): ?>
									    <tr>
											<td><?= $delivery ['delivery_date'] ?></td>
										    <td><?= ucwords($delivery['Users']['first_name']." ".$delivery['Users']['middle_name']." ".$delivery['Users']['last_name']) ?></td>
											<td><?= $delivery ['Users']['mobile_phone'] ?></td>
											<td><?= $delivery ['Users']['address'].", ". $delivery ['Users']['city'].", ". $delivery ['States']['abbrev']."-". $delivery ['Users']['zipcode'] ?></td>
								            <td><?php switch ($delivery['thaali_size']) {
					          							case '0': echo 'None';break;
					          							case '1': echo 'Small (1-2 servings)'; break;
					          							case '2': echo 'Medium (3-4 Servings)';break;
					          							case '3': echo 'Large (5-6 Servings)';break;
					          							case '4': echo 'X-Small (Salawat)';break;
					          							case '5': echo 'X-Large';break;
					          					}
												?>
											</td>
											<td><?= $delivery ['delivery_notes'] ?></td>
								        </tr>  
										<?php endforeach; ?>
								      </table>
					 </div>
	 </div>
	  </div>
 </div>
	<script>  
     $(document).ready(function() {
   	
	 $('#driverThaaliDeliveries').DataTable({
   	   	"bAutoWidth": true , 
	   	  "order": [[ 0, "desc" ]],
	   	  "ordering": false,
	   	 
        fixedColumns: true,
        "bSort" : true
   	 });

	 var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);

			fromDate = $("#dtfrom").val();
			toDate = $("#dtto").val();
		}
		
	 
	 $(function() {
			var dates = $("#dtfrom, #dtto")
		   .datepicker({
				//maxDate:'0',
				dateFormat: 'yy-mm-dd',
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) 
				{
					var option = this.id == "dtfrom" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}

			});
		});

		 
		
		
	 });
   	 </script>