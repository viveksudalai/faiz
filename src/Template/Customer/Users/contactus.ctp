<?php $this->assign('title', 'Contact Faiz');?> 
<div class="main-content">
	<div class="row">
		<h1 class="col-md-6 col-xs-12 col-sm-4">Contact - Faiz</h1>
	</div>		
 <hr/>
 
	<div class="row">
		<div class="col-md-9 col-sm-10 col-xs-11 vacation-container">
		  										
													
		<?= $this->Form->create('', ['id' => 'contactus', 'name' => 'contactus']) ?> 											
			<div class="row">
			    <div class="form-group">
				    <label for="emailId">Your Message</label>
			    <?= $this->Form->input('msg',['class'=> 'form-control', 'type' => 'textarea', 'label' => false]); ?>
			    </div>
				</div>
			  <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-custom']);?>
			  <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'dashboard'], ['class' => 'btn btn-grey']);?>	  
			  <?= $this->Form->end() ?>
			</div>
	</div>
 </div>
<script>
	 $(document).ready(function() {
	  $("#contactus").validate({
	     rules: {
	    	 msg: {required:true}
	     },
        messages: {
        	msg: { 
  			 	 required: "Message is required."
  			} 
		}
	}); 
});
</script>