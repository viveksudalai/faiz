<?php $this->assign('title', 'Drivers');?> 
<div class="main-content drivers">
	<div class="row">
			<h1 class="col-md-6 col-xs-12 col-sm-4">Drivers</h1>
				<div class="text-right col-md-6 col-xs-12 col-sm-8">
			 		
				</div>
	 </div>
	  <hr/>
  
	 <div class="row">
		    <div class="col-md-11 col-sm-11 col-xs-11 table-responsive">
		    		<table id="driverList" class="table table-striped table-bordered " cellspacing="0" width="100%">
										<thead>
											<tr class="skyblue-bg">
												<th><?= 'Name' ?></th>
												<th><?= 'Mobile' ?></th>
												<th><?= 'Mobile' ?></th>
												<th><?= 'Address' ?></th>
										    </tr>
										</thead>
										<tbody> 
								        <?php foreach ($driverDetails as $details): ?>
									    <tr>
											<td><?= ucwords($details->full_name) ?></td>
										    <td><?= $details->mobile_phone ?></td>
											<td><?= $details->home_phone ?></td>
											<td><?= $details->address.", ". $details->city.", ". $details->state->name." - ". $details->zipcode ?></td>
								        </tr>  
										<?php endforeach; ?>
								      </table>
					 </div>
	 </div>
 </div>
	<script>  
     $(document).ready(function() {
   	
	 $('#driverList').DataTable({
   	   	"bAutoWidth": true , 
	   	  "order": [[ 0, "desc" ]],
	   	  "ordering": false,
	   	 
        fixedColumns: true,
        "bSort" : true
   	 });
	 });
   	 </script>