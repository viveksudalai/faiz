<?php $this->assign('title', 'Forgot Password'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="login-bg">
			<div class="row">
				<div class="col-md-12 login-logo text-center">
					<?php echo $this->Html->image('logo-big.png', ['alt' => 'Faizchicago', 'class' => '']); ?>
				</div>
			</div>
			
	 		<div class="row">
				<div class="col-md-offset-3 col-md-7 login-content">
					 <div class="login-container">
					    <div class="text-center">WELCOME TO FIAZCHICAGO.COM</div>
					    <div class="inner-container">
					      		<div class="row">
					        		<div class="col-md-4">
					        			<div class="text-center">
					        				<?php echo $this->Html->image('mem-icon.png', ['alt' => 'Faizchicago', 'class' => '']); ?>
					       					<h4>Forgot Password</h4></div>
					       					<?= $this->Flash->render() ?>
					       					<?= $this->Form->create('Users', ['id' => 'login', 'name' => 'login']) ?>
					       					<input type="text" id="ejamaat-id" class="form-control" name="ejamaat_id"  placeholder="Ejamaat ID" autocomplete="off" />
					        				<input type="text" id="email-address" class="form-control" name="email_address" placeholder="Email address" autocomplete="off"/>
					         				 <?= $this->Form->submit('Reset Password', array('class'=> 'btn btn-orange')); ?> <br/>
					         			</div>
					         			<div class="col-md-8">
										 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			                        <!-- Wrapper for slides -->
			                        <div class="carousel-inner" role="listbox">
			                          <div class="item active">
			                           <?php echo $this->Html->image('slider-1.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
			                          </div>
			                          <div class="item">
			                             <?php echo $this->Html->image('slider-2.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
			                          </div>
			                           <div class="item">
			                             <?php echo $this->Html->image('slider-3.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
			                          </div>
			                        </div>
			
			                        
			                      </div>
					        			</div>
								</div>
					 	</div>
					</div>
				</div>
		</div>	
				
		</div>
	</div>
</div>	
 

<script>
 $(document).ready(function() {
	  $("#login").validate({
	     rules: {
         email_address: {
             // simple rule, converted to {required:true}
              required:true,
              email:true
         }, 
	     ejamaat_id: {
              required:true
         }  
     },
        messages: {
           email_address:"Email address is required.",
       	   ejamaat_id:"Ejamaat id is required."
		}
	}); 
});
</script>
<style>
.error {color:red;}
.success{color:green;}
</style>