<div class="box-header">
   	<div class="col-md-12 text-right table-upper-row">
  		<?= $this->Html->link('Back',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'view']);?>
	</div>
</div>
<!-- /.box-header -->
<!-- Main content -->
<section class="content">
<div class="row">
        <div class="box col-lg-12">
            <div class="box-body table-responsive">
                
				     <ul class="nav nav-tabs tab-active">
				        <li class="active"><a data-toggle="tab" href="#userInfo">Basic Information</a></li>
				      	<?php if ($user->user_role == 3 || $user->user_role == 5) { ?>
				        	 <li><a data-toggle="tab" href="#driver">Vehicle Information</a></li>
				       <?php } ?> 	 
				         <?php if ($user->user_role != 3 ) { ?>
				         	<li><a data-toggle="tab" href="#thaali">Thaali Information</a></li>
				         <?php } ?>	
				     </ul>
				     <?= $this->Form->create($user, ['id' => 'user', 'name' => 'user']) ?> 
			          <div class="tab-content">
			          		
						           <div id="userInfo" class="tab-pane fade in active tab-space">
							   			 <?php
										        	echo $this->Form->input('id', [ 'class'=> 'form-control']);
										            echo $this->Form->input('ejamaatid', [ 'class'=> 'form-control','disabled']);
										            echo $this->Form->input('first_name', [ 'class'=> 'form-control']);
										            echo $this->Form->input('middle_name', [ 'class'=> 'form-control']);
										            echo $this->Form->input('last_name', [ 'class'=> 'form-control']);
										            echo $this->Form->input('email_address', [ 'class'=> 'form-control']);
										            echo $this->Form->input('mobile_phone', [ 'class'=> 'form-control']);
										            echo $this->Form->input('home_phone', [ 'class'=> 'form-control']);
										            echo $this->Form->input('address', [ 'class'=> 'form-control']);
										        ?>
										   
										    <?= $this->Form->button(__('Submit')) ?>
										
									</div>
									 <?php if ($user->user_role == 3 || $user->user_role == 5) { ?> 
									 	<div id="driver" class="tab-pane fade tab-space">
							               <?php 
										        echo "<div id='driver-info'>";
		       									echo $this->Form->input('license_number',['default'=>$license_number, 'class'=> 'form-control']);
		       									echo $this->Form->input('license_expiry_date', ['default'=>$license_expiry_date,'class'=>'datepicker form-control','type'=> 'text']);
		       									echo $this->Form->input('vehicle_make',['default'=>$vehicle_make, 'class'=> 'form-control']);
		       									echo $this->Form->input('vehicle_model',['default'=>$vehicle_model, 'class'=> 'form-control']);
		       									echo '<div class="input select required" aria-required="true"><label for="user-role">Vehicle Year</label>';
		       									echo $this->Form->year('vehicle_year', ['minYear' => 1980,'maxYear' => date('Y'), 'value'=>$vehicle_year],[ 'class'=> 'form-control']);
		       									echo "</div>";
		       									echo $this->Form->input('insurance_provider', ['default'=>$insurance_provider, 'class'=> 'form-control']);
		       									echo $this->Form->input('insurance_policy_no', ['default'=>$insurance_policy_no, 'class'=> 'form-control']);
		       									echo $this->Form->input('insurance_expiry_date',['default'=>$insurance_expiry_date, 'class'=>'form-control datepicker','type'=> 'text']);
		       							        echo '</div>';
		       								 ?>
		       								  <?= $this->Form->button(__('Submit')) ?>
								     </div>
								   <?php } ?>   
							
					    <?php if ($user->user_role != 3 ) { ?>
									<div id="thaali" class="tab-pane fade tab-space">
										 
										 
										     <?php
										            $activeDays = explode(',', $active_days);
										            echo $this->Form->input('thaali_size', [ 'options' => $thaaliSize, 'default'=> $thaali_size,'class'=> 'form-control']);
										            echo $this->Form->input('delivery_method', ['options' => $deliveryMethod,  'default'=>$delivery_method, 'class'=> 'form-control']);
										            echo $this->Form->input('driver_notes', ['default'=>$driver_notes,  'class'=> 'form-control']);
										            echo $this->Form->input('active_days', [ 'options' => $days,  'class'=> 'form-control', 'multiple'=> 'true', 'default'=>$activeDays]);
										        ?>
										    <?= $this->Form->button(__('Submit')) ?>
										 	 
									 </div>
						 <?php } ?>	
								</div>
		 <?= $this->Form->end() ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.row -->  
</section>



        	
         
  <script>
 $(document).ready(function() {
	  $("#user").validate({
	     rules: {
	         ejamaatid: {
	             required:true,
	             number:true
	         },
         first_name: {
             required:true,
             alpha:true,
             minlength:1
         },
         middle_name: {
             required:true,
             alpha:true
         },
         last_name: {
             required:true,
             alpha:true
         }, 
         email_address: {
             required:true,
             email:true
         },
          mobile_phone: {
             required:true,
             number:true
         },
         home_phone: {
              required:true,
             number:true
         },
          address: {
              required:true
         },
         
         license_number: {
              required:true
         },
          license_expiry_date: {
              required:true
         },
          vehicle_make: {
              required:true
         },
          
	       vehicle_model: {
              required:true
         },
          vehicle_year: {
              required:true
         },
          insurance_provider: {
              required:true
         },
         insurance_policy_no : {
          required:true
         },
         insurance_expiry_date : {
          required:true
         },
         
          thaali_size: {
              required:true
         },
          delivery_method: {
              required:true
         },
          driver_notes: {
              required:true
         },
          
	       thaali_size: {
              required:true
         },
          delivery_method: {
              required:true
         },
          driver_notes: {
              required:true
         } 
           
     },
        messages: {
        ejamaatid: { 
  			 required: "Ejamaatid is required.",
  			 number:"Accepts number only."
  		},
  		first_name: { 
  			 required: "First name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		middle_name: { 
  			 required: "Middle name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		last_name: { 
  			 required: "Last name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		email_address: { 
  			 required: "Email address is required.",
  			 email:"Enter valid email address."
  		},
  		mobile_phone: { 
  			 required: "Mobile phone is required.",
  			 number:"Accepts numbers only."
  		},
  		home_phone: { 
  			 required: "Home phone is required.",
  			 number:"Accepts numbers only."
  		},
  		
  		address : { 
  			 required: "Address is required." 
  		},
  		
  		  thaali_size: {
              required: "Thaali size is required." 
         },
          delivery_method: {
               required: "Delivery method is required." 
         },
          driver_notes: {
              required: "Driver notes is required." 
         },
          thaali_size: {
              required: "Thaali size is required." 
         },
          delivery_method: {
               required: "Delivery method is required." 
         },
          driver_notes: {
              required: "Driver notes is required." 
         } 
         
		}
	}); 
	
	
  /* $("#thaaliinfo").validate({
	    
           
     },
        messages: {
       	
  		 
         
		}
	}); */
 
	
	if($("#insurance_provider" ).val() != '') {
 		$('#driver-info').css('display', 'block');
	}
	
	$(function() {
		
			var dates = $("#insurance-expiry-date, #license-expiry-date").datepicker({
									minDate:'-0y',
							  // 	maxDate:'+0d',
							  dateFormat: 'yy-mm-dd',
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) {
					var option = this.id == "insurance_expiry_date" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
				
			});
		});
	});

$( "#show-driver-info" ).click(function() {
	$('#driver-info').css('display', 'block');
});

</script>

 
<style>
#driver-info {display:none}
</style>
 