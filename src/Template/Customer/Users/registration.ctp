<?php 
/**
 * User Registration
 */
 ?>
 <?php $this->assign('title', 'Registration');?>
 
 <div class="main-content user-reg">
<h1>Registration</h1>
<div class="row">
<div class="col-md-8 grey-container">
 <?= $this->Form->create($user, ['id' => 'user', 'name' => 'user',  'class'=> 'row']) ?>

<div class="form-group col-md-6 col-sm-6 col-xs-8">
     <?php  echo $this->Form->input('ejamaatid', ['type' => 'text',  'class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('first_name', [ 'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
   <?php echo $this->Form->input('middle_name', [ 'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('last_name', [ 'class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
   <?php echo $this->Form->input('email_address', ['type' => 'email', 'class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">  
    <?php echo $this->Form->input('secondary_email_address', [ 'class'=> 'form-control']);
		 echo "<span>Seperate email addresses with a ','	</span>";
	?>
  </div>
    <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('password', ['type' => 'password','class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('confirm_password', ['type' => 'password','class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
   	<?php echo $this->Form->input('home_phone', [ 'class'=> 'form-control']);?>
  	<div>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</div>
   </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('mobile_phone', [ 'class'=> 'form-control']);?>
   <div>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</div>
  </div>

  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('mobile_carrier', ['options' => $mobileCarriers, 'empty' => 'Select Carrier', 'class'=> 'form-control']);?>
  </div>
 <div class="form-group col-md-6 col-sm-6 col-xs-8">
   <?php echo $this->Form->input('address', [ 'type' => 'text', 'class'=> 'form-control']);?>
  </div>
 <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php  echo $this->Form->input('city', [ 'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
   <?php 	echo $this->Form->input('user_state', ['options' => $states,'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('zipcode', [ 'class'=> 'form-control']); ?>
  </div>
   <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('thaali_size', [ 'options' => $thaaliSize,'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
    <?php echo $this->Form->input('delivery_method', ['options' => $deliveryMethod,'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
   <?php echo $this->Form->input('driver_notes', ['class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-8">
  <?php echo $this->Form->input('active_days', array('type'=>'select', 'multiple'=>'checkbox', 'options'=>$days, 'name' => 'active_days'));?>
   <?php //echo $this->Form->input('active_days', [ 'options' => $days,  'class'=> 'form-control', 'multiple'=> 'true']); ?>
  </div>
  <div class="form-group col-md-12 col-sm-12 col-xs-12">
  <button type="submit" class="btn btn-custom">SUBMIT</button>
   <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'login'], ['class' => 'btn btn-grey']);?>	  
</div>

<?= $this->Form->end() ?>
</div>
</div>
</div>

 
		
 <script>
	 $(document).ready(function() {
		  $("#user").validate({
			 rules: {
				 ejamaatid: {
					 required:true,
					 number:true
				 },
				  
			 first_name: {
				 required:true,
				 alpha:true
			 },
			 middle_name: {
				 required:true,
				 alpha:true
			 },
			 last_name: {
				 required:true,
				 alpha:true
			 }, 
			 email_address: {
				 required:true,
				 email:true
			 },
			/* secondary_email_address: {
	        	 multiemail:true
	         },*/
			 home_phone: {
	             required:true,
	             phoneUS: true
	         }, 
	          mobile_phone: {
	             required:true,
	             phoneUS: true
	         }, 
	         mobile_carrier: {
	             required:true             
	         },
			 zipcode: {
				  required:true,
				  zipcodeUS:true
			 },
			 password: {
				  required:true,
				  minlength:5
			 },
			 confirm_password: {
				  required:true,
				  equalTo: "#password"
			 },
			  address: {
				  required:true
			 },
			 thaali_size: {
				  required:true
			 },
			  delivery_method: {
				  required:true
			 },
			  driver_notes: {
				  required:true
			 } ,
			 "active_days[]" : {required:true, minlength:1}
		 },
			messages: {
				ejamaatid: { 
					 required: "Ejamaatid is required.",
					 number:"Accepts number only."
				},
				first_name: { 
					 required: "First name is required.",
					 alpha:"Accepts alphapets only."
				},
				middle_name: { 
					 required: "Middle name is required.",
					 alpha:"Accepts alphapets only."
				},
				last_name: { 
					 required: "Last name is required.",
					 alpha:"Accepts alphapets only."
				},
				email_address: { 
					 required: "Email address is required.",
					 email:"Enter valid email address."
				},
				/*secondary_email_address: { 
		 			multiemail:"please use a comma to separate multiple email addresses."
		 		},*/
				home_phone: { 
		 			 required: "Home phone is required.",
		 			 phoneUS:"Enter a valid Home number"
		 		},
		  		mobile_phone: { 
		  			 required: "Mobile phone is required.",
		  			 phoneUS:"Enter a valid Mobile number"
		  		},
		  		mobile_carrier: {
		             required:"Mobile carrier is required."
		         },
				zipcode: { 
					 required: "Zipcode is required.",
					 zipcodeUS:"Enter a valid Zipcode"
				}, 
				password: { 
					 required: "Password is required."
				},
				confirm_password: { 
					 required: "Confirm Password is required."			 
				}, 		
				address : { 
					 required: "Address is required." 
				}, 
				thaali_size: {
					  required: "Thaali size is required." 
				 },
				  delivery_method: {
					   required: "Delivery method is required." 
				 },
				  driver_notes: {
					  required: "Driver notes is required." 
				 },
				  thaali_size: {
					  required: "Thaali size is required." 
				 },
				 
				 "active_days[]" : { required: "Choose at least one day.",  minlength:"Choose at least one day."}
			}
		}); 
	});

	$( "#show-driver-info" ).click(function() {
		$('#driver-info').css('display', 'block');
	 });
	$(function() {

		var dates = $("#insurance-expiry-date, #license-expiry-date").datepicker({
								minDate:'-0y',
						  // 	maxDate:'+0d',
						  dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "insurance_expiry_date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
			
		});
	});
</script>
 
 
<style>
#driver-info {display:none}
</style> 

 
