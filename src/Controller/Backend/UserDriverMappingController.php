<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * UserDriverMapping Controller
 *
 * @property \App\Model\Table\UserDriverMappingTable $UserDriverMapping
 */
class UserDriverMappingController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userDriverMapping = $this->paginate($this->UserDriverMapping);

        $this->set(compact('userDriverMapping'));
        $this->set('_serialize', ['userDriverMapping']);
    }

    /**
     * View method
     *
     * @param string|null $id User Driver Mapping id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userDriverMapping = $this->UserDriverMapping->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userDriverMapping', $userDriverMapping);
        $this->set('_serialize', ['userDriverMapping']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
   /* public function add($id = null)
    {
        $userDriverMapping = $this->UserDriverMapping->newEntity();
        if ($this->request->is('post')) {
            $userDriverMapping = $this->UserDriverMapping->patchEntity($userDriverMapping, $this->request->data);
            if ($this->UserDriverMapping->save($userDriverMapping)) {
                $this->Flash->success(__('The user driver mapping has been saved.'));
                return $this->redirect(
                		['controller' => 'Users', 'action' => 'index']
                );
               // return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user driver mapping could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserDriverMapping->Users->find('list', ['limit' => 200]);
        $this->set(compact('userDriverMapping', 'users'));
        $this->set('_serialize', ['userDriverMapping']);
    }
    
    */
    
    public function add($uid = null)
    {
    	$this->loadModel('ThaaliDelivery');
    	$userDriverMapping = $this->UserDriverMapping->newEntity();
    	if ($this->request->is('post')) {
    		$userDriverMapping = $this->UserDriverMapping->patchEntity($userDriverMapping, $this->request->data);
    		$userDriverMapping->id = $this->request->data['id']; 
    		if ($this->UserDriverMapping->save($userDriverMapping)) {
    			// Driver update in thaali delivery table
    			$sdate = date('y-m-d');
    			$driverId 			= $this->request->data['driver_id'];
    			$user_id   			= $this->request->data['user_id'];
    			$query = $this->ThaaliDelivery->query();
    			$query->update()
    			->set(['driver_id' => $driverId])
    			->where(['delivery_date >=' => $sdate, 'user_id' => $user_id])
    			->execute();
    			$this->Flash->success(__('Driver has been assigned to the user.'));
    			return $this->redirect(
    					['controller' => 'Users', 'action' => 'index']
    			);
    			// return $this->redirect(['action' => 'index']);
    		} else {
    			$this->Flash->error(__('Driver could not be assigned. Please, try again.'));
    		}
    	}
    	

    	$userDriverMappingDetails = $this->UserDriverMapping
    	->find()
    	->contain('Users')
    	->where([
    			'UserDriverMapping.user_id' => $uid
    	])->first();
    	
    	$number = count($userDriverMappingDetails);
    	
    	if ($number == 1) {
    		$data = $userDriverMappingDetails->toArray();
    		$userDriverMappingId = $data['id'];
    		$driverId = $data['driver_id'];
    		$userId 			= $data['user_id'];
    		$this->set('userDriverMappingId',$userDriverMappingId);
    		$this->set('driverId',$driverId);
    		$this->set('userId',$userId);
    	}
    	else
    	{
    		$this->set('userDriverMappingId','');
    		$this->set('userId','');
    		$this->set('driverId','');
    	}
    	
    	if ($uid !='')	{
    	 
    	  $userDetails =	$this->UserDriverMapping->Users->find('list',[
    				'keyField' => 'id',
    				'valueField' => function ($e) {
    					return $e->full_name;
    				}, 'conditions' => ['id' =>  $uid]]);
    		
    		$this->set('userDetails', $userDetails);
    	}
    	
    	$driverDetails =  $this->UserDriverMapping->Users->find('list',[
    		   'keyField' => 'id',
               'valueField' => function ($e) {
               return $e->full_name;
               }, 'conditions' => ['user_role in (3,5)', 'status' => '1']]); 
    	
      	$this->set('driverDetails', $driverDetails);
    	
    	$this->set(compact('userDriverMapping',  'users'));
    	$this->set('_serialize', ['userDriverMapping']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Driver Mapping id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userDriverMapping = $this->UserDriverMapping->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userDriverMapping = $this->UserDriverMapping->patchEntity($userDriverMapping, $this->request->data);
            if ($this->UserDriverMapping->save($userDriverMapping)) {
                $this->Flash->success(__('Driver has been assigned to the user.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Driver could not be assigned. Please, try again.'));
            }
        }
        $users = $this->UserDriverMapping->Users->find('list', ['limit' => 200]);
        $this->set(compact('userDriverMapping', 'users'));
        $this->set('_serialize', ['userDriverMapping']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Driver Mapping id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userDriverMapping = $this->UserDriverMapping->get($id);
        if ($this->UserDriverMapping->delete($userDriverMapping)) {
            $this->Flash->success(__('Driver mapping has been deleted.'));
        } else {
            $this->Flash->error(__('Driver could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
