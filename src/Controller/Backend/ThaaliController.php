<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
/**
 * Thaali Controller
 *
 * @property \App\Model\Table\ThaaliTable $Thaali
 */
class ThaaliController extends AppController
{

	public $paginate = [
			'order' => [
					'Thaali.id' => 'DESC'
			]
	];
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Paginator');
	}
	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {	
		$this->paginate = [
    			'contain' => ['Caterer']
    	];
    	$thaali = $this->paginate($this->Thaali);
    	$this->set(compact('thaali'));
    	$this->set('_serialize', ['thaali']);
    	
    	$connection = ConnectionManager::get('default');
    	$results = $connection->execute("SELECT t.id, DATE_FORMAT(t.menu_date, '%W %Y/%m/%e' ) as menu_date, t.menu_item, c.name, pickup, delivery 
    									 FROM thaali t 
										 left join caterer c 
										 	on t.caterer_id = c.id 
										 left join 
										 	(select distinct delivery_date, thaali_id,  
    									 		(select  count(id) as pickup FROM `thaali_delivery` where order_status = '0' and delivery_type='1' and delivery_date = t.delivery_date group by delivery_type, delivery_date) as pickup,  
												(select  count(id) as delivery  FROM `thaali_delivery` where (order_status = '0' OR order_status = '1') and delivery_type='2' and delivery_date = t.delivery_date group by delivery_type, delivery_date ) as delivery
											from thaali_delivery t)td
										 		on t.id = td.thaali_id ORDER BY  t.menu_date DESC")->fetchAll('assoc');
    

    $this->set('thaaliDelivery', $results);
  }

    /**
     * View method
     *
     * @param string|null $id Thaali id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $thaali = $this->Thaali->get($id, [
            'contain' => ['Caterer']
        ]);

        $this->set('thaali', $thaali);
        $this->set('_serialize', ['thaali']);
    }

    
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
   		$this->loadModel('Miqat');
    	$thaali = $this->Thaali->newEntity();
    	$this->loadModel('Users');
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('DriverReplacement');
        if ($this->request->is('post')) {
        	 
        	$thaaliDate = $this->request->data['menu_date'];
        	//validating thaali date
         	$thaaliCount = $this->Thaali->find()->where(['menu_date =' => $thaaliDate])->count(); 
        	if ($thaaliCount == 0 ) { 
        		// Validating Thaali date with Miqat date
        		$miqatCount = $this->Miqat->find()->where(['start_date <=' => $thaaliDate, 'end_date >=' => $thaaliDate])->count();
        		if ($miqatCount == 0) {
        			$thaali = $this->Thaali->patchEntity($thaali, $this->request->data);
        			if ($this->Thaali->save($thaali)) {
        	
        					// insert thaali delivery data
        				//Ger week day
        			 	$WeekDay = date('w', strtotime($thaaliDate));
        				//Get user list based on the week day and User vacation planner
        				
        			 	$query = $this->Users->find('all')
        				->hydrate(false)
        				->select(['Users.id', 'd.driver_id', 't.thaali_size', 'ud.distribution_id','t.delivery_method', 't.driver_notes'])
        				->join([
        						't' => [
        								'table' => 'user_thaali_info',
        								'type' => 'RIGHT',
        								'conditions' =>  'Users.id = t.user_id',
        						],
        						'd' => [
        								'table' => 'user_driver_mapping ',
        								'type' => 'LEFT ',
        								'conditions' => 'Users.id = d.user_id',
        						],
        						'ud' => [
        								'table' => 'user_distribution_mapping  ',
        								'type' => 'LEFT ',
        								'conditions' => 'Users.id = ud.user_id',
        						],
        				])
        				->where([' Users.status' => '1', 'active_days LIKE' => "%$WeekDay%" ]);
        			    
        				//	->andWhere([' Users.id NOT IN' => [$this->UserVacationPlanner->find()->select('user_id')->where(['start_date <=' => $thaaliDate, 'end_date >=' => $thaaliDate])]
        				
        				
        				foreach ($query as $user) {
        				 
        					$ThaaliDelivery = $this->ThaaliDelivery->newEntity();
        					$ThaaliDelivery->user_id =  $user['id'];
        					$ThaaliDelivery->driver_id = $user['d']['driver_id'];
        					$ThaaliDelivery->thaali_id = $thaali->id;
        					$ThaaliDelivery->thaali_size = $user['t']['thaali_size'];
        					$ThaaliDelivery->distribution_id = $user['ud']['distribution_id'];
        					$ThaaliDelivery->delivery_date = $thaaliDate;
        					$ThaaliDelivery->delivery_type = $user['t']['delivery_method'];
        					$ThaaliDelivery->delivery_notes = $user['t']['driver_notes'];
        					$ThaaliDelivery->order_status = $this->__checkUserVacationDetails($user['id'], $thaaliDate);
        					
        					// Checking driver replacment data
        					$driverData = $this->DriverReplacement->find()
        								  ->select('replace_driver_id')->
        								  where(['from_date <=' => $thaaliDate, 'to_date >=' => $thaaliDate, 'driver_id' => $user['d']['driver_id']])->order(['id' => 'DESC'])->first();
        					 if (count($driverData) > 0 ) {
        						foreach ($driverData as $driver) {
        							$driverId = $driver->replace_driver_id;
        							$ThaaliDelivery->driver_id = $driverId;
        						}
        					 }
        					
        					$this->ThaaliDelivery->save($ThaaliDelivery);
        				}
        				//End of thaali delivery data
        				 
        				$this->Flash->success(__('The thaali has been saved.'));
        			 	return $this->redirect(['action' => 'index']);
        			} else {
        				$this->Flash->error(__('The thaali could not be saved. Please, try again.'));
        			}
        		}
        		else
        			$this->Flash->error(__('Already Miqat event creted on this date. So please check the Thaali date.'));
        		    //End of Miqat date validation
        	}
        	else 
        		$this->Flash->error(__('Already Thaali menu is created for the selected date.'));
        	//end of thaali date validation
        }	

        $caterer = $this->Thaali->Caterer->find('list', ['limit' => 200]);
        $this->set(compact('thaali', 'caterer'));
        $this->set('_serialize', ['thaali']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Thaali id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
    	$this->loadModel('Miqat');
    	$thaali = $this->Thaali->newEntity();
    	$this->loadModel('Users');
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('DriverReplacement');
        $thaali = $this->Thaali->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	$thaaliDate = $this->request->data['menu_date'];
        	//validating thaali date
        	$thaaliCount = $this->Thaali->find()->where(['menu_date =' => $thaaliDate, 'id !=' => $id])->count(); 
        	if ($thaaliCount == 0 ) { 
        		// Validating Thaali date with Miqat date
        		$miqatCount = $this->Miqat->find()->where(['start_date <=' => $thaaliDate, 'end_date >=' => $thaaliDate])->count();
        		if ($miqatCount == 0) {
        			$thaali = $this->Thaali->patchEntity($thaali, $this->request->data);
        			if ($this->Thaali->save($thaali)) {
        			
        				$this->Flash->success(__('The thaali has been saved.'));
        				// insert thaali delivery data
        			 	//Ger week day
        				/*$WeekDay = date('w', strtotime($thaaliDate));
        				//Get user list based on the week day and User vacation planner
        				
        				$query = $this->Users->find('all')
        				->hydrate(false)
        				->select(['Users.id', 'd.driver_id', 't.thaali_size', 'ud.distribution_id','t.delivery_method', 't.driver_notes'])
        				->join([
        						't' => [
        								'table' => 'user_thaali_info',
        								'type' => 'RIGHT',
        								'conditions' =>  'Users.id = t.user_id',
        						],
        						'd' => [
        								'table' => 'user_driver_mapping ',
        								'type' => 'LEFT ',
        								'conditions' => 'Users.id = d.user_id',
        						],
        						'ud' => [
        								'table' => 'user_distribution_mapping  ',
        								'type' => 'LEFT ',
        								'conditions' => 'Users.id = ud.user_id',
        						],
        				])
        				->where([' Users.status' => '1', 'active_days LIKE' => "%$WeekDay%" ])
        				->andWhere([' Users.id NOT IN' => [$this->UserVacationPlanner->find()->select('user_id')->where(['start_date <=' => $thaaliDate, 'end_date >=' => $thaaliDate])]
        				]);
        				
        				foreach ($query as $user) {
        					$ThaaliDelivery = $this->ThaaliDelivery->newEntity();
        					$ThaaliDelivery->user_id =  $user['id'];
        					$ThaaliDelivery->driver_id = $user['d']['driver_id'];
        					$ThaaliDelivery->thaali_id = $thaali->id;
        					$ThaaliDelivery->thaali_size = $user['t']['thaali_size'];
        					$ThaaliDelivery->distribution_id = $user['ud']['distribution_id'];
        					$ThaaliDelivery->delivery_date = $thaaliDate;
        					$ThaaliDelivery->delivery_type = $user['t']['delivery_method'];
        					$ThaaliDelivery->delivery_notes = $user['t']['driver_notes'];
        					$ThaaliDelivery->order_status = '0';
        					
        					// Checking driver replacment data
        					$driverData = $this->DriverReplacement->find()
        								  ->select('replace_driver_id')->
        								  where(['from_date <=' => $thaaliDate, 'to_date >=' => $thaaliDate, 'driver_id' => $user['d']['driver_id']]);
        					if ($driverData->count() > 0 ) {
        						foreach ($driverData as $driver) {
        							$driverId = $driver->replace_driver_id;
        							$ThaaliDelivery->driver_id = $driverId;
        						}
        					}
        					
        					$this->ThaaliDelivery->save($ThaaliDelivery);
        				}
        				//End of thaali delivery data
        				 */
        				return $this->redirect(['action' => 'index']);
        			} else {
        				$this->Flash->error(__('The thaali could not be saved. Please, try again.'));
        			}
        		}
        		else
        			$this->Flash->error(__('Already Miqat event creted on this date. So please check the Thaali date.'));
        		    //End of Miqat date validation
        	}
        	else 
        		$this->Flash->error(__('Already Thaali menu is created for the selected date.'));
        	//end of thaali date validation
       }
        $caterer = $this->Thaali->Caterer->find('list', ['limit' => 200]);
        $this->set(compact('thaali', 'caterer'));
        $this->set('_serialize', ['thaali']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Thaali id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $thaali = $this->Thaali->get($id);
        if ($this->Thaali->delete($thaali)) {
            $this->Flash->success(__('The thaali has been deleted.'));
        } else {
            $this->Flash->error(__('The thaali could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function __checkUserVacationDetails($userId, $menuDate) {
    	$this->loadModel('UserVacationPlanner');
    	$userVacation = $this->UserVacationPlanner->find()
    	->select('id')->
    	where(['start_date <=' => $menuDate, 'end_date >=' => $menuDate, 'user_id' => $userId])->count();
    	if ($userVacation == 0)
    		return '0';
    	else return '3';
    }
}
