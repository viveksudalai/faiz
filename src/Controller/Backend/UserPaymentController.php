<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * UserPayment Controller
 *
 * @property \App\Model\Table\UserPaymentTable $UserPayment */
class UserPaymentController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userPayment = $this->paginate($this->UserPayment);

        $this->set(compact('userPayment'));
        $this->set('_serialize', ['userPayment']);
    }

    /**
     * View method
     *
     * @param string|null $id User Payment id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userPayment = $this->UserPayment->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userPayment', $userPayment);
        $this->set('_serialize', ['userPayment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userPayment = $this->UserPayment->newEntity();
        if ($this->request->is('post')) {
            $userPayment = $this->UserPayment->patchEntity($userPayment, $this->request->data);
            if ($this->UserPayment->save($userPayment)) {
                $this->Flash->success(__('The user payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user payment could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserPayment->Users->find('list', ['limit' => 200]);
        $this->set(compact('userPayment', 'users'));
        $this->set('_serialize', ['userPayment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Payment id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userPayment = $this->UserPayment->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userPayment = $this->UserPayment->patchEntity($userPayment, $this->request->data);
            if ($this->UserPayment->save($userPayment)) {
                $this->Flash->success(__('The user payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user payment could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserPayment->Users->find('list', ['limit' => 200]);
        $this->set(compact('userPayment', 'users'));
        $this->set('_serialize', ['userPayment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Payment id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userPayment = $this->UserPayment->get($id);
        if ($this->UserPayment->delete($userPayment)) {
            $this->Flash->success(__('The user payment has been deleted.'));
        } else {
            $this->Flash->error(__('The user payment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
