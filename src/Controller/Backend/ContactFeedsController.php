<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * ContactFeeds Controller
 *
 * @property \App\Model\Table\ContactFeedsTable $ContactFeeds */
class ContactFeedsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $contactFeeds = $this->paginate($this->ContactFeeds);

        $this->set(compact('contactFeeds'));
        $this->set('_serialize', ['contactFeeds']);
    }

    /**
     * View method
     *
     * @param string|null $id Contact Feed id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contactFeed = $this->ContactFeeds->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('contactFeed', $contactFeed);
        $this->set('_serialize', ['contactFeed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contactFeed = $this->ContactFeeds->newEntity();
        if ($this->request->is('post')) {
            $contactFeed = $this->ContactFeeds->patchEntity($contactFeed, $this->request->data);
            if ($this->ContactFeeds->save($contactFeed)) {
                $this->Flash->success(__('The contact feed has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contact feed could not be saved. Please, try again.'));
            }
        }
        $users = $this->ContactFeeds->Users->find('list', ['limit' => 200]);
        $this->set(compact('contactFeed', 'users'));
        $this->set('_serialize', ['contactFeed']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact Feed id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contactFeed = $this->ContactFeeds->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contactFeed = $this->ContactFeeds->patchEntity($contactFeed, $this->request->data);
            if ($this->ContactFeeds->save($contactFeed)) {
                $this->Flash->success(__('The contact feed has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The contact feed could not be saved. Please, try again.'));
            }
        }
        $users = $this->ContactFeeds->Users->find('list', ['limit' => 200]);
        $this->set(compact('contactFeed', 'users'));
        $this->set('_serialize', ['contactFeed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact Feed id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactFeed = $this->ContactFeeds->get($id);
        if ($this->ContactFeeds->delete($contactFeed)) {
            $this->Flash->success(__('The contact feed has been deleted.'));
        } else {
            $this->Flash->error(__('The contact feed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
