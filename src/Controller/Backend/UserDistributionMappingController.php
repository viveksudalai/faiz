<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * UserDistributionMapping Controller
 *
 * @property \App\Model\Table\UserDistributionMappingTable $UserDistributionMapping
 */
class UserDistributionMappingController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'DistributionCenter']
        ];
        $userDistributionMapping = $this->paginate($this->UserDistributionMapping);

        $this->set(compact('userDistributionMapping'));
        $this->set('_serialize', ['userDistributionMapping']);
    }

    /**
     * View method
     *
     * @param string|null $id User Distribution Mapping id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userDistributionMapping = $this->UserDistributionMapping->get($id, [
            'contain' => ['Users', 'DistributionCenter']
        ]);

        $this->set('userDistributionMapping', $userDistributionMapping);
        $this->set('_serialize', ['userDistributionMapping']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userDistributionMapping = $this->UserDistributionMapping->newEntity();
        if ($this->request->is('post')) {
            $userDistributionMapping = $this->UserDistributionMapping->patchEntity($userDistributionMapping, $this->request->data);
            if ($this->UserDistributionMapping->save($userDistributionMapping)) {
                $this->Flash->success(__('The user distribution mapping has been saved.'));
                return $this->redirect(
                		['controller' => 'Users', 'action' => 'index']
                );
                //return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user distribution mapping could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserDistributionMapping->Users->find('list', ['limit' => 200]);
        $distributionCenter = $this->UserDistributionMapping->DistributionCenter->find('list', ['limit' => 200]);
        $this->set(compact('userDistributionMapping', 'users', 'distributionCenter'));
        $this->set('_serialize', ['userDistributionMapping']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Distribution Mapping id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userDistributionMapping = $this->UserDistributionMapping->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userDistributionMapping = $this->UserDistributionMapping->patchEntity($userDistributionMapping, $this->request->data);
            if ($this->UserDistributionMapping->save($userDistributionMapping)) {
                $this->Flash->success(__('The user distribution mapping has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user distribution mapping could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserDistributionMapping->Users->find('list', ['limit' => 200]);
        $distributionCenter = $this->UserDistributionMapping->DistributionCenter->find('list', ['limit' => 200]);
        $this->set(compact('userDistributionMapping', 'users', 'distributionCenter'));
        $this->set('_serialize', ['userDistributionMapping']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Distribution Mapping id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userDistributionMapping = $this->UserDistributionMapping->get($id);
        if ($this->UserDistributionMapping->delete($userDistributionMapping)) {
            $this->Flash->success(__('The user distribution mapping has been deleted.'));
        } else {
            $this->Flash->error(__('The user distribution mapping could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function assign($uid = null)
    {
    	$userDistributionMapping = $this->UserDistributionMapping->newEntity();
    	if ($this->request->is('post')) {
    		$userDistributionMapping = $this->UserDistributionMapping->patchEntity($userDistributionMapping, $this->request->data);
    		$userDistributionMapping->id = $this->request->data['id'];
    		if ($this->UserDistributionMapping->save($userDistributionMapping)) {
    			$this->Flash->success(__('The user distribution mapping has been saved.'));
    			return $this->redirect(
    					['controller' => 'Users', 'action' => 'index']
    			);
    			//return $this->redirect(['action' => 'index']);
    		} else {
    			$this->Flash->error(__('The user distribution mapping could not be saved. Please, try again.'));
    		}
    	}
    	
    	$userDistributionDetails = $this->UserDistributionMapping
    	->find()
    	->contain('Users')
    	->where([
    			'UserDistributionMapping.user_id' => $uid
    	])->first();
    	$number = count($userDistributionDetails);
    	if ($number == 1) {
	    	$data = $userDistributionDetails->toArray();
	    	$id = $data['id'];
	    	$userDistributionId = $data['distribution_id'];
	    	$userId 			= $data['user_id'];
	    	$this->set('userDistributionId',$userDistributionId);
	    	$this->set('userId',$userId);
	    	$this->set('id',$id);
    	}
    	else 
    	{
    		$this->set('userDistributionId','');
    		$this->set('id','');
    	}
    	 if ($uid !='')
    		$userDetails = $this->UserDistributionMapping->Users->find('list')->where(['id' => $uid]);
    	
    	$this->set('userDetails', $userDetails);
    	
    	$users = $this->UserDistributionMapping->Users->find('list', ['limit' => 200]);
    	$distributionCenter = $this->UserDistributionMapping->DistributionCenter->find('list', ['limit' => 200]);
    	$this->set(compact('userDistributionMapping', 'users', 'distributionCenter'));
    	$this->set('_serialize', ['userDistributionMapping']);
    }
}
