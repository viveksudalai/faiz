<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Backend;

use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ReportController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
	}
	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
    }
   
   public function user()
    {
	    $data = array();
		
		$role = array('0' => 'All','2'=>'Admin','3' => 'Driver', '4' => 'Household', '5' => 'Driver & Household');
		$this->set('role', $role);
		
		/* Check post value existing */
		if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
			$toDate = $this->request->data['dtto'];
			
			$UserRole = $this->request->data['user_role'];
			$user_role_id = '';
			if($UserRole > 0){
				$user_role_id = " and us1.user_role = '".$UserRole."'"; 
			}
			
			//load the user table data
			$this->loadModel("Users");
			
			$connection = ConnectionManager::get('default');
			$data = $connection->execute("SELECT us1.id, us1.ejamaatid,concat(us1.first_name,' ',us1.middle_name,' ',us1.last_name) as user_name,
									us1.email_address,us1.address,us1.city,us1.mobile_phone,us1.home_phone, us1.zipcode,us1.status,us1.created,
									r.name as role_name,ust.thaali_size,ust.delivery_method ,st.abbrev as state_abbrev,
									(select concat(first_name,' ',middle_name,' ',last_name) from users where id = usdm.driver_id) as driver_name,ust.driver_notes
									FROM `users` as us1 INNER JOIN role as r on r.id = us1.user_role
									LEFT JOIN states as st on st.id = us1.user_state
									LEFT JOIN user_thaali_info as ust on ust.user_id = us1.id 
									LEFT JOIN user_driver_mapping as usdm on usdm.user_id = us1.id 
									WHERE date(us1.created) >= '".$fromDate."' and date(us1.created) <= '".$toDate."'".$user_role_id)->fetchAll('assoc');
		}	
		
		$this->set('users', $data);
    }
	
	public function deliveryreport()
	{
		$data = array();
		//$size = '';
		$size = " and usdm.thaali_size != '0'";
		$type = '';
		$userDriver = '';
		$fromDate = date('Y-m-d') ;
        $toDate =  date('Y-m-d') ;
		
		//Fetch Driver list
		$this->loadModel("Users");
	//	$users = $this->Users->find('list',['conditions' => ['Users.user_role in (3,5)', 'status' => '1']]);
		$users  = $this->Users->find('list',[
				'keyField' => 'id',
				'valueField' => function ($e) {
					return $e->full_name;
				}, 'conditions' => ['user_role in (3,5)', 'status' => '1']]);
		 
		$this->set('driverlist', $users);
		
		
		//ThaaliSize options
		$thaaliSize = array('-1' => 'All Size','0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
        $this->set('thaaliSize', $thaaliSize);
        
		//Deliverymethods options
        $deliveryMethod = array('0' => 'All Delivery Type','1' => 'Pick Up', '2' => 'Delivery');
        $this->set('deliveryMethod', $deliveryMethod);
		  
		/* Check post value existing */
		if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
            $toDate = $this->request->data['dtto'];
			$driverId = $this->request->data['drivery_list'];
			$getSize = $this->request->data['thaaliSize'];
			$deliveryType = $this->request->data['deliverymethod'];
			
			if($getSize != -1){
				$size = " and usdm.thaali_size ='".$getSize."'";
			}else{
				$size = " and usdm.thaali_size != '0'";
			}
			if($deliveryType != 0){
				$type = " and usdm.delivery_type = '".$deliveryType."'";
			}
			if($driverId != ''){
				$userDriver = " and usdm.driver_id = '".$driverId."'";
			}
		}
        $connection = ConnectionManager::get('default');
		$data = $connection->execute("SELECT us1.id,us1.first_name,us1.middle_name,us1.last_name,
									     us1.address,us1.city,us1.mobile_phone,us1.zipcode,usdm.thaali_size,usdm.delivery_type,usdm.delivery_date,
									     st.name as state_abbrev,(select concat(first_name,' ',last_name) from users where id = usdm.driver_id) as driver_name,usdm.delivery_notes
									     FROM `users` as us1
									     LEFT JOIN states as st on st.id = us1.user_state
										 LEFT JOIN thaali_delivery as usdm on usdm.user_id = us1.id 
										 WHERE usdm.order_status in ('0','1') and date(usdm.delivery_date) >= '".$fromDate."' and date(usdm.delivery_date) <= '".$toDate."'".$size.$type.$userDriver)->fetchAll('assoc');
			
		 
		$this->set('thaaliData', $data);
	}
	
	public function userlabel()
	{
		$data = array();
		$fromDate = date('Y-m-d') ;
        $toDate =  date('Y-m-d') ;
		/* Check post value existing */
		if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
            $toDate = $this->request->data['dtto'];

		}
        $connection = ConnectionManager::get('default');
		$data = $connection->execute("SELECT us1.id,us1.first_name,us1.middle_name,us1.last_name,us1.color_code,
									     us1.address,us1.city,us1.mobile_phone,us1.zipcode,usdm.thaali_size,usdm.delivery_type,usdm.delivery_date,
									     st.name as state_abbrev,(select concat(first_name,' ',last_name) from users where id = usdm.driver_id) as driver_name,usdm.delivery_notes
									     FROM `users` as us1
									     LEFT JOIN states as st on st.id = us1.user_state
										 LEFT JOIN thaali_delivery as usdm on usdm.user_id = us1.id 
										 WHERE usdm.order_status in ('0','1') and date(usdm.delivery_date) >= '".$fromDate."' and date(usdm.delivery_date) <= '".$toDate."' ORDER BY usdm.delivery_date DESC")->fetchAll('assoc');
			
		 
		$this->set('labellist', $data);
		$this->set('fromdate',$fromDate);
		$this->set('todate',$toDate);
	}
	
	
	public function pdflabel($fromDate=null, $toDate=null) { 
		$connection = ConnectionManager::get('default');
		$data = $connection->execute("SELECT us1.id,us1.first_name,us1.middle_name,us1.last_name,
										 us1.address,us1.city,us1.mobile_phone,us1.zipcode,usdm.thaali_size,usdm.delivery_type,usdm.delivery_date,
										 st.name as state_abbrev,(select color_code from users where id = usdm.driver_id) as color_code,
										 (select concat(first_name,' ',last_name) from users where id = usdm.driver_id) as driver_name,usdm.delivery_notes
										 FROM `users` as us1
										 LEFT JOIN states as st on st.id = us1.user_state
										 LEFT JOIN thaali_delivery as usdm on usdm.user_id = us1.id 
										 WHERE usdm.order_status in ('0','1') and date(usdm.delivery_date) >= '".$fromDate."' and date(usdm.delivery_date) <= '".$toDate."' ORDER BY usdm.delivery_date DESC")->fetchAll('assoc');
		$this->set('labellist',$data); 
	     $filename = 'userlabel';
      $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.pdf')
			->template('pdf/pdflabel')
            ->options(['config' => [
				'orientation' => 'landscape',
				'size' => 'A4',
                'filename' => $filename,
                'render' => 'download',
        ]]);   
	}
	
	/*
	 * House Hold Thaali Summary ReportController
	 *
	 * @var $data array
	 * @var $type int 
	 * @var $deliveryType int
	 * @var $fromDate
	 * @var $toDate 
	 * @var $connection object
	 *
	 * @return $data array
	 *
	 **/
    
	public function householdreport()
	{
		$data = array();
		$type = '';
		$deliveryType = 0;
	
		$deliveryMethod = array('0' => 'All Delivery Type','1' => 'Pick Up', '2' => 'Delivery');
		$this->set('deliveryMethod', $deliveryMethod);
	
		$fromDate = date('Y-m-d') ;
		$toDate =  date('Y-m-d') ;
		/* Check post value existing */
		if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
			$toDate = $this->request->data['dtto'];
			$deliveryType = $this->request->data['deliverymethod'];
		}
	
		if($deliveryType != 0){
			$type = " and td.delivery_type = '".$deliveryType."'";
		}
	
		$connection = ConnectionManager::get('default');
		$data = $connection->execute("SELECT concat(us.first_name,' ',us.middle_name,' ',us.last_name) as name,
									 count(*) as thaalicount FROM users as us
									 inner join thaali_delivery as td on us.id = td.user_id
		                             WHERE us.user_role != 3 and td.order_status = '1' and
									 date(td.created) >= '".$fromDate."' and date(td.created) <= '".$toDate."' ".$type." group by td.user_id order by name")->fetchAll('assoc');

		$this->set('summaryreport',$data);
	}
	
	/*
	 * Drvier Summary ReportController
	 *
	 * @var $data array
	 * @var $userDriver array
	 * @var $driverId int
	 * @var $fromDate
	 * @var $toDate 
	 * @var $connection object
	 *
	 * @return $data array
	 *
	 **/
	 
	public function deliverysummary()
	{
		$data = array();
	    $userDriver = '';
		$driverId = '';
		
		//Fetch Driver list
		$this->loadModel("Users");
		$users = $this->Users->find('list',['conditions' => 'Users.user_role in (3,5)']);
		$this->set('driverlist', $users);
		
		$fromDate = date('Y-m-d') ;
        $toDate =  date('Y-m-d') ;
		
		/* Check post value existing */
		if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
            $toDate = $this->request->data['dtto'];
	        $driverId = $this->request->data['drivery_list'];
		}
	
		if($driverId != ''){
				$userDriver = " and us.id = '".$driverId."'";
		}
		
		
		$connection = ConnectionManager::get('default');
		$data = $connection->execute("SELECT concat(us.first_name,' ',us.middle_name,' ',us.last_name) as name, us.id , 
									(SELECT count(td.id) FROM thaali_delivery td WHERE td.driver_id = us.id and 
									 td.order_status = '1' and td.delivery_date between '".$fromDate."' and '".$toDate."' GROUP BY td.driver_id) as delivery_count 
									 from users us where us.status ='1' and us.user_role in (3, 5) ".$userDriver)->fetchAll('assoc');

					 
		$this->set('driverreport',$data);
	}
	
	public function contactfeeds() {
		$this->loadModel('ContactFeeds');
		$data = array();
		$fromDate = date('Y-m-d') ;
		$toDate =  date('Y-m-d') ;
	
		/* Check post value existing */
		if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
			$toDate = $this->request->data['dtto'];
		}
		$query = $this->ContactFeeds->find()
		->select(['Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode', 'ContactFeeds.created', 'ContactFeeds.message'])
		->join([
				'c' => [
						'table' => 'users',
						'alias' => 'Users',
						'type' => 'LEFT',
						'conditions' => 'ContactFeeds.user_id = Users.id',
						'fields' => ['Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode']
				],
				 
		],
		[ 'date(ContactFeeds.created) >=' => $fromDate, 'date(ContactFeeds.created) <=' => $toDate ]);
		$this->set('contactFeeds',$query);
	}
}
