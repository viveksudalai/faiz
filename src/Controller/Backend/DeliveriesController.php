<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Backend;

use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DeliveriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // Driver Thaali delivery information
		$this->loadModel('ThaaliDelivery');
		$this->loadModel('DriverReplacement');
		
		$driverDeleiveryInfo = $this->ThaaliDelivery->find('all',[
       'fields' => [
            'delivery_date' => 'ThaaliDelivery.delivery_date',
            'driver_name' => 'CONCAT(users.first_name, " ", users.middle_name, " ", users.last_name)',
            'tot_deliveries' => 'COUNT(ThaaliDelivery.id)'
        ],
        'join' => [
            'table' => 'users', 
            'type' => 'LEFT',
            'conditions' => 'users.id = ThaaliDelivery.driver_id'
        ],
		'conditions' => ['ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.thaali_size !=' => '0', 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]],
        'group' => ['ThaaliDelivery.driver_id', 'ThaaliDelivery.delivery_date'],
		'order' =>['delivery_date' => 'DESC']
		]);
		
		$this->set('driverDeleiveryInfo', $driverDeleiveryInfo);
		
		$driverReplacement = $this->DriverReplacement->find('all')
		 ->select(['id' => 'DriverReplacement.id',
		 		   'driver_name' => 'CONCAT(users.first_name, " ", users.middle_name, " ", users.last_name)',
       		       'replaced_driver_name' => 'CONCAT(user.first_name, " ", user.middle_name, " ", user.last_name)',
				   'from_date' => 'from_date',
				   'to_date' => 'to_date'				 
		])
		->join([
				'u' => [
						'table' => 'users',
						'alias' => 'users',
						'type' => 'INNER ',
						'conditions' => 'DriverReplacement.driver_id = users.id',
						'fields' => ['users.first_name, users.middle_name, users.last_name']
		
				],
				'u1' => [
						'table' => 'users',
						'alias' => 'user',
						'type' => 'INNER ',
						'conditions' => 'DriverReplacement.replace_driver_id  = user.id',
						'fields' => ['user.first_name, user.middle_name, user.last_name']
				],
		])
		->where([' DriverReplacement.status' => '1'] )
		->order(['DriverReplacement.id' => 'DESC']);
		
		$this->set('driverReplacement', $driverReplacement);
		
		// debug($driverReplacement);
	 
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Driver Replacement id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteDriverReplacement($id = null)
    {
    	$this->loadModel('DriverReplacement');
    	$this->request->allowMethod(['post', 'delete']);
    	$driverReplacement = $this->DriverReplacement->get($id);
    	if ($this->DriverReplacement->delete($driverReplacement)) {
    		$this->Flash->success(__('The driver replacement has been deleted.'));
    	} else {
    		$this->Flash->error(__('The driver replacement could not be deleted. Please, try again.'));
    	}
    
    	return $this->redirect(['action' => 'index']);
    }
   
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addDriverReplacement()
    {
    	$this->loadModel('DriverReplacement');
    	$this->loadModel('Users');
    	$this->loadModel('ThaaliDelivery');
    	$driverReplacement = $this->DriverReplacement->newEntity();
    	if ($this->request->is('post')) {
    		$driverReplacement = $this->DriverReplacement->patchEntity($driverReplacement, $this->request->data);
    		if ($this->DriverReplacement->save($driverReplacement)) {
    			$driverId 			= $this->request->data['driver_id'];
    			$replaceDriverId 	= $this->request->data['replace_driver_id'];
    			$fromDate 			= $this->request->data['from_date'];
    			$toDate   			= $this->request->data['to_date'];
    			// Driver update in thaali delivery table
    			$query = $this->ThaaliDelivery->query();
    			$query->update()
    			->set(['driver_id' => $replaceDriverId])
    			->where(['delivery_date <=' => $fromDate, 'delivery_date >=' => $toDate, 'driver_id' => $driverId])
    			->execute();
    			
    			$this->Flash->success(__('The driver replacement has been saved.'));
    
    			return $this->redirect(['action' => 'index']);
    		} else {
    			$this->Flash->error(__('The driver replacement could not be saved. Please, try again.'));
    		}
    	}
    	
    	$us  = $this->Users->find('list',[
    		   'keyField' => 'id',
               'valueField' => function ($e) {
               return $e->full_name;
               }, 'conditions' => ['user_role in (3,5)', 'status' => '1']]); 
    	
    	$this->set('us', $us); 
    	$this->set(compact('driverReplacement', 'users'));
    	$this->set('_serialize', ['driverReplacement']);
     }
    
   
}
