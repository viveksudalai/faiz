<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;
/**
 * UserVacationPlanner Controller
 *
 * @property \App\Model\Table\UserVacationPlannerTable $UserVacationPlanner
 */
class UserVacationPlannerController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userVacationPlanner = $this->paginate($this->UserVacationPlanner);

        $this->set(compact('userVacationPlanner'));
        $this->set('_serialize', ['userVacationPlanner']);
    }

    /**
     * View method
     *
     * @param string|null $id User Vacation Planner id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userVacationPlanner = $this->UserVacationPlanner->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userVacationPlanner', $userVacationPlanner);
        $this->set('_serialize', ['userVacationPlanner']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($userId = null)
    {
        $userVacationPlanner = $this->UserVacationPlanner->newEntity();
        $this->loadModel('ThaaliDelivery');
        $now = Time::now();
        $min = $now->modify('+2 days')->i18nFormat('MM/dd/yyyy');
        $this->set('user_id', $userId);
        $this->set('min', $min);
        if ($this->request->is('post')) {
        	 $startDate = $this->request->data['start_date'];
        	$startDate = $this->request->data['start_date'];
    		$endDate = $this->request->data['end_date'];
    		
    		$vacationCount = $this->UserVacationPlanner->find()
    		->where(['user_id' =>  $userId , 'OR' => [['start_date <=' => $startDate, 'start_date >=' => $endDate], ['end_date >=' => $startDate, 'end_date <=' => $endDate]]])->count();
    		
    		if ($vacationCount == 0 ) {
    			$now = Time::now();
        		$minDate = $now->modify('+2 days')->i18nFormat('yyyy-MM-dd'); 
	        	 
		        	if ($minDate <= $startDate ) {
	        	    $userVacationPlanner = $this->UserVacationPlanner->patchEntity($userVacationPlanner, $this->request->data);
		            if ($this->UserVacationPlanner->save($userVacationPlanner)) {
		                $this->Flash->success(__('The user vacation planner has been added succesfully.'));
		                //Update User Thaali order status 
			       		$query = $this->ThaaliDelivery->query();
			       		$query->update()
			       		->set(['order_status' => '3'])
			       		->where(['delivery_date >=' =>  $startDate, 'delivery_date <=' =>  $endDate,  'user_id' => $userId ])
			       		->execute();
			       		debug($query);
			       		// End of Update User Thaali order status
		                return $this->redirect(['controller' => 'users', 'action' => 'view', $this->request->data['user_id']]);
		            }
	            }
	        	 else {
	        	 	$this->Flash->error(__('Adding/modifing vacation information will be considered valid only if the event is scheduled for after '. $min));
	            }
    		}
    		else {
    				$this->Flash->error(__('The selected date already added in the user Vacation Planner list.'));
    		}      
        }
        $users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
        $this->set(compact('userVacationPlanner', 'users'));
        $this->set('_serialize', ['userVacationPlanner']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Vacation Planner id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userVacationPlanner = $this->UserVacationPlanner->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userVacationPlanner = $this->UserVacationPlanner->patchEntity($userVacationPlanner, $this->request->data);
            if ($this->UserVacationPlanner->save($userVacationPlanner)) {
                $this->Flash->success(__('The user vacation planner has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user vacation planner could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
        $this->set(compact('userVacationPlanner', 'users'));
        $this->set('_serialize', ['userVacationPlanner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Vacation Planner id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userVacationPlanner = $this->UserVacationPlanner->get($id);
        if ($this->UserVacationPlanner->delete($userVacationPlanner)) {
            $this->Flash->success(__('The user vacation planner has been deleted.'));
        } else {
            $this->Flash->error(__('The user vacation planner could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
