<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Backend;
use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\I18n\Date;
use Cake\I18n\Time;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DashboardController extends AppController
{

     /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    	$this->loadModel('Users');
    	$this->loadModel('ThaaliDelivery');
		
    	// to fetch unapproved user details
		$recentUser = $this->Users->find('all', [
        'conditions' => array('Users.status' => '0'),				
    	'order' => 'Users.created DESC'
 		]);
		$recentUserCnt = $this->Users->find('all', [
				'conditions' => array('Users.status' => '0'),
				'order' => 'Users.created DESC'
		])->count();
		$this->set('recentUserCnt', $recentUserCnt);
		$this->set('recentUser', $recentUser);
		// to get the count of registered user in this week
		$thisWeekUser = $this->Users->find('all', [
				'conditions' => array('Users.created >' => new \DateTime('-7 days'))
				
		])->count();
		$this->set('thisWeekUser', $thisWeekUser);
		
		// to get the count of registered user in the last two weeks
		$lastTwoWeeksUser = $this->Users->find('all', [
				'conditions' => array('Users.created >' => new \DateTime('-13 days'))
		])->count();
		$this->set('lastTwoWeeksUser', $lastTwoWeeksUser);
		
		/*'conditions' => array( 'Users.created >= ' => 'select date_add(last_day(date_sub(now(),interval 2 MONTH)),interval 1 day)) ',
				'Users.created <= ' => 'select last_day(date_sub(now(),interval 1 MONTH)))' ) ]);
		*/
		// to get the count of registered user in the last month
		$connection = ConnectionManager::get('default');
		$lastMonthUser = $connection->execute('SELECT id FROM users WHERE date(created) between (select date_add(last_day(date_sub(now(),interval 2 MONTH)),interval 1 day))
                 and  (select last_day(date_sub(now(),interval 1 MONTH)))')->count();
		
		$this->set('lastMonthUser', $lastMonthUser);
		
		
		// to get the count of today's thaali
		 $now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
		$todaysDelivery = $this->ThaaliDelivery->find('all', [
				'conditions' => array('ThaaliDelivery.delivery_date' => $now, 'ThaaliDelivery.thaali_size !=' => '0', 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
		])->count();
		$this->set('todaysDelivery', $todaysDelivery);
		
		//For get this week
		
		 $sdate = date('y-m-d', strtotime("sunday last week"));
		// $edate = date('y-m-d', strtotime("saturday this week")); 
		$edate = date('y-m-d');
		
		//For get this month

		 $m_sdate = date('y-m-01');
		 $m_edate = date("y-m-t"); 
		
		//For get this year
		 $y_sdate = date('y-01-01');
		 $y_edate = date('Y-12-31'); 
        

		// to get the count of this week thaali 
		$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array('ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate, 'ThaaliDelivery.order_status' => '1' , 'ThaaliDelivery.thaali_size !=' => '0')
		])->count();
		$this->set('thisWeekDelivery', $thisWeekDelivery);
		
		// to get the count of this month thaali 
		$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array('ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1', 'ThaaliDelivery.thaali_size !=' => '0')
		])->count();
		$this->set('thisMonthDelivery', $thisMonthDelivery);
		
		// to get the count of this year thaali 
		$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array('ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate, 'ThaaliDelivery.order_status' => '1' , 'ThaaliDelivery.thaali_size !=' => '0')
		])->count();
		$this->set('thisYearDelivery', $thisYearDelivery);
		
		
		// to get the count of all users from the begining
		$allUsers= $this->Users->find('all',[
			'conditions' => array(  'OR' => [['status' => '0'], ['status' => '1']])
		])->count();
		$this->set('allUsers', $allUsers);
		
	 
    }
    
    public function approve($id = null) {
    	
    	$usersTable = TableRegistry::get('Users');
		$users = $usersTable->get($id); //  

		$users->status = '1';
		
		if ($usersTable->save($users)) {
			$this->Flash->success(__('The user has been approved.'));
		} else {
			$this->Flash->error(__('The user could not be approved. Please, try again.'));
		}
		
		return $this->redirect(['action' => 'index']);
    
    }

    public function delete($id = null)
    {
    	$usersTable = TableRegistry::get('Users');
		$user= $usersTable->get($id);
    	if ($user->status == 0)
    		$user->status = 3;
    	else if ($user->status == 1 || $user->status == 2)
    		$user->status = 4;
    
    	if ($usersTable->save($user)) {
    		$this->Flash->success(__('The user has been deleted.'));
    	} else {
    		$this->Flash->error(__('The user could not be deleted. Please, try again.'));
    	}
    
    	return $this->redirect(['action' => 'index']);
    }
}
