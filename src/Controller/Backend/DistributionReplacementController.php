<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * DistributionReplacement Controller
 *
 * @property \App\Model\Table\DistributionReplacementTable $DistributionReplacement
 */
class DistributionReplacementController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['DistributionCenter', 'DistributionCenter1']
        ];
        $distributionReplacement = $this->paginate($this->DistributionReplacement);

        $this->set(compact('distributionReplacement'));
        $this->set('_serialize', ['distributionReplacement']);
        
        
       
    }

    /**
     * View method
     *
     * @param string|null $id Distribution Replacement id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
    	
        $distributionReplacement = $this->DistributionReplacement->get($id, [
            'contain' => ['DistributionCenter', 'DistributionCenter1']
        ]);
        
  //      $distributionReplacement = $distributionReplacement->toArray();
       // echo $distributionReplacement->distribution_center1->name;
//echo '<pre>';print_r($distributionReplacement);exit;

        $this->set('distributionReplacement', $distributionReplacement);
        $this->set('_serialize', ['distributionReplacement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    	$distributionCenter = $this->DistributionReplacement->DistributionCenter->find('list', ['limit' => 200]);
    	$this->set('distributionCenter', $distributionCenter);
        $distributionReplacement = $this->DistributionReplacement->newEntity();
        if ($this->request->is('post')) {
            $distributionReplacement = $this->DistributionReplacement->patchEntity($distributionReplacement, $this->request->data);
            if ($this->DistributionReplacement->save($distributionReplacement)) {
                $this->Flash->success(__('The distribution replacement has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The distribution replacement could not be saved. Please, try again.'));
            }
        }
        $distributionCenter = $this->DistributionReplacement->DistributionCenter->find('list', ['limit' => 200]);
        $this->set(compact('distributionReplacement', 'distributionCenter'));
        $this->set('_serialize', ['distributionReplacement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Distribution Replacement id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $distributionReplacement = $this->DistributionReplacement->get($id, [
            'contain' => []
        ]);
        $distributionCenter = $this->DistributionReplacement->DistributionCenter->find('list', ['limit' => 200]);
        $this->set('distributionCenter', $distributionCenter);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $distributionReplacement = $this->DistributionReplacement->patchEntity($distributionReplacement, $this->request->data);
            if ($this->DistributionReplacement->save($distributionReplacement)) {
                $this->Flash->success(__('The distribution replacement has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The distribution replacement could not be saved. Please, try again.'));
            }
        }
        $distributionCenter = $this->DistributionReplacement->DistributionCenter->find('list', ['limit' => 200]);
        $this->set(compact('distributionReplacement', 'distributionCenter'));
        $this->set('_serialize', ['distributionReplacement']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Distribution Replacement id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $distributionReplacement = $this->DistributionReplacement->get($id);
        if ($this->DistributionReplacement->delete($distributionReplacement)) {
            $this->Flash->success(__('The distribution replacement has been deleted.'));
        } else {
            $this->Flash->error(__('The distribution replacement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
