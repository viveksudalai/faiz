<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
/**
 * ThaaliSurvey Controller
 *
 * @property \App\Model\Table\ThaaliSurveyTable $ThaaliSurvey */
class ThaaliSurveyController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Thaalis']
        ];
        $thaaliSurvey = $this->paginate($this->ThaaliSurvey);

        $this->set(compact('thaaliSurvey'));
        $this->set('_serialize', ['thaaliSurvey']);
    }

    /**
     * View method
     *
     * @param string|null $id Thaali Survey id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view()
    {
          $data = array();
		  $fromDate = date('Y-m-d') ;
          $toDate =  date('Y-m-d') ;
		 
		  if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
			$toDate = $this->request->data['dtto'];
		  }
		 
		  $connection = ConnectionManager::get('default');
		  
		  $data = $connection->execute("select ts.rating as rating , ts.thaali_taste as taste,ts.thaali_qty  as qty,ts.created as created_date,
									  (select concat(first_name,' ',middle_name,' ',last_name) from users where id = ts.user_id) as user_name,
									  (select menu_item from thaali where id = ts.thaali_id ) as menu_item 
									  from thaali_survey as ts WHERE date(ts.created) >= '".$fromDate."'and date(ts.created) <= '".$toDate."'")->fetchAll('assoc');
		  
		 $this->set('survey', $data);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
   /*  public function add()
    {
        $thaaliSurvey = $this->ThaaliSurvey->newEntity();
        if ($this->request->is('post')) {
            $thaaliSurvey = $this->ThaaliSurvey->patchEntity($thaaliSurvey, $this->request->data);
            if ($this->ThaaliSurvey->save($thaaliSurvey)) {
                $this->Flash->success(__('The thaali survey has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The thaali survey could not be saved. Please, try again.'));
            }
        }
        $users = $this->ThaaliSurvey->Users->find('list', ['limit' => 200]);
        $thaalis = $this->ThaaliSurvey->Thaalis->find('list', ['limit' => 200]);
        $this->set(compact('thaaliSurvey', 'users', 'thaalis'));
        $this->set('_serialize', ['thaaliSurvey']);
    } */

    /**
     * Edit method
     *
     * @param string|null $id Thaali Survey id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /* public function edit($id = null)
    {
        $thaaliSurvey = $this->ThaaliSurvey->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $thaaliSurvey = $this->ThaaliSurvey->patchEntity($thaaliSurvey, $this->request->data);
            if ($this->ThaaliSurvey->save($thaaliSurvey)) {
                $this->Flash->success(__('The thaali survey has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The thaali survey could not be saved. Please, try again.'));
            }
        }
        $users = $this->ThaaliSurvey->Users->find('list', ['limit' => 200]);
        $thaalis = $this->ThaaliSurvey->Thaalis->find('list', ['limit' => 200]);
        $this->set(compact('thaaliSurvey', 'users', 'thaalis'));
        $this->set('_serialize', ['thaaliSurvey']);
    } */

    /**
     * Delete method
     *
     * @param string|null $id Thaali Survey id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
   /*  public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $thaaliSurvey = $this->ThaaliSurvey->get($id);
        if ($this->ThaaliSurvey->delete($thaaliSurvey)) {
            $this->Flash->success(__('The thaali survey has been deleted.'));
        } else {
            $this->Flash->error(__('The thaali survey could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    } */
}
