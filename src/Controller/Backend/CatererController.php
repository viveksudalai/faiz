<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * Caterer Controller
 *
 * @property \App\Model\Table\CatererTable $Caterer
 */
class CatererController extends AppController
{
	public $paginate = [
			'order' => [
					'Caterer.id' => 'desc'
			]
	];
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Paginator');
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $caterer = $this->paginate($this->Caterer);

        $this->set(compact('caterer'));
        $this->set('_serialize', ['caterer']);
    }

    /**
     * View method
     *
     * @param string|null $id Caterer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $caterer = $this->Caterer->get($id, [
            'contain' => ['Thaali']
        ]);

        $this->set('caterer', $caterer);
        $this->set('_serialize', ['caterer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $caterer = $this->Caterer->newEntity();
        if ($this->request->is('post')) {
            $caterer = $this->Caterer->patchEntity($caterer, $this->request->data);
            if ($this->Caterer->save($caterer)) {
                $this->Flash->success(__('The caterer has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The caterer could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('caterer'));
        $this->set('_serialize', ['caterer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Caterer id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $caterer = $this->Caterer->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $caterer = $this->Caterer->patchEntity($caterer, $this->request->data);
            if ($this->Caterer->save($caterer)) {
                $this->Flash->success(__('The caterer has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The caterer could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('caterer'));
        $this->set('_serialize', ['caterer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Caterer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $caterer = $this->Caterer->get($id);
        if ($this->Caterer->delete($caterer)) {
            $this->Flash->success(__('The caterer has been deleted.'));
        } else {
            $this->Flash->error(__('The caterer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
