<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * MobileCarrierList Controller
 *
 * @property \App\Model\Table\MobileCarrierListTable $MobileCarrierList */
class MobileCarrierListController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $mobileCarrierList = $this->paginate($this->MobileCarrierList);

        $this->set(compact('mobileCarrierList'));
        $this->set('_serialize', ['mobileCarrierList']);
    }

    /**
     * View method
     *
     * @param string|null $id Mobile Carrier List id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mobileCarrierList = $this->MobileCarrierList->get($id, [
            'contain' => []
        ]);

        $this->set('mobileCarrierList', $mobileCarrierList);
        $this->set('_serialize', ['mobileCarrierList']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mobileCarrierList = $this->MobileCarrierList->newEntity();
        if ($this->request->is('post')) {
            $mobileCarrierList = $this->MobileCarrierList->patchEntity($mobileCarrierList, $this->request->data);
            if ($this->MobileCarrierList->save($mobileCarrierList)) {
                $this->Flash->success(__('The mobile carrier list has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mobile carrier list could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mobileCarrierList'));
        $this->set('_serialize', ['mobileCarrierList']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mobile Carrier List id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mobileCarrierList = $this->MobileCarrierList->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mobileCarrierList = $this->MobileCarrierList->patchEntity($mobileCarrierList, $this->request->data);
            if ($this->MobileCarrierList->save($mobileCarrierList)) {
                $this->Flash->success(__('The mobile carrier list has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mobile carrier list could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('mobileCarrierList'));
        $this->set('_serialize', ['mobileCarrierList']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mobile Carrier List id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mobileCarrierList = $this->MobileCarrierList->get($id);
        if ($this->MobileCarrierList->delete($mobileCarrierList)) {
            $this->Flash->success(__('The mobile carrier list has been deleted.'));
        } else {
            $this->Flash->error(__('The mobile carrier list could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
