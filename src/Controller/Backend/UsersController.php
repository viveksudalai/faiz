<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use PhpParser\Node\Stmt\Else_;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\View\Helper\FormHelper;
use Cake\Mailer\Email;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	public $paginate = [
			'conditions' => ['Users.status != ' => 0],
			'order' => [
					'Users.id' => 'desc'
			]
	];
 
	var $role = array('Role');

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()	{
       	$this->paginate = [
    			'contain' => ['Role','DriverInfo', 'States']
    	];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
        $sdate = date('y-m-01');
         
        // to get the list of registered user in this  month
        
        //  $currentMonthStartDate =
        $this->paginate = [
        		'contain' => ['Role', 'States'],
        		'conditions' => [ 'Users.created >= ' =>  $sdate, 'Users.status ' => 1],
        		'order' => ['created' => 'DESC']
        ];
        
        $thisMonthUser = $this->paginate($this->Users);
        
        $this->set('thisMonthUser', $thisMonthUser);
        
    }
    
    public function beforeFilter(Event $event)	{
    	$this->Auth->allow(['register', 'forgotPassword']);
    	// Change layout for Ajax requests
	    if ($this->request->is('ajax')) {
	      $this->viewBuilder()->layout('ajax'); 
	     
	    }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)	{
        $this->loadModel('ThaaliDelivery');
		$this->loadModel('UserVacationPlanner');
		$this->loadModel('UserPayment');
		$this->loadModel('Miqat');
		$user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'States', 'MobileCarrierList']
        ]);
		
	   
	    // User Thaali delivery information
		$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
        				->hydrate(false)
        				->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
        				->join([
        						't' => [
        								'table' => 'thaali',
										'alias' => 'ThaaliInfo',
        								'type' => 'LEFT ',
        								'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
										'fields' => ['ThaaliInfo.menu_item']
										
        						],
        						'c' => [
        								'table' => 'caterer',
										'alias' => 'CatererInfo',
        								'type' => 'LEFT ',
        								'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
										'fields' => ['CatererInfo.name']
        						],
        				])
        				->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
						->order(['delivery_date' => 'DESC']);
		$this->set('thaaliDeleiveryInfo', $thaaliDeleiveryInfo);
     
		// Driver Thaali delivery information
		$driverDeleiveryInfo = $this->ThaaliDelivery->find('all')
        				->hydrate(false)
        				->select(['delivery_date', 'delivery_type', 'delivery_notes', 'thaali_size', 'Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode', 'States.abbrev'])
        				->join([
        						
        						'c' => [
        								'table' => 'users',
										'alias' => 'Users',
        								'type' => 'LEFT ',
        								'conditions' => 'Users.id = ThaaliDelivery.user_id',
										'fields' => ['Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode']
        						],
								's' => [
        								'table' => 'states',
										'alias' => 'States',
        								'type' => 'LEFT ',
        								'conditions' => 'States.id = Users.user_state',
										'fields' => ['States.abbrev']
										
        						],
        				])
        				->where([' ThaaliDelivery.driver_id' => $id, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
						->order(['delivery_date' => 'DESC']);
		$this->set('driverDeleiveryInfo', $driverDeleiveryInfo);
		
		// Driver Delivery Statistics
		
		//For get this week
		
		 $sdate = date('y-m-d', strtotime("sunday last week"));
		 //$edate = date('y-m-d', strtotime("saturday this week")); 
		 $edate = date('y-m-d'); 
		
		//For get this month

		 $m_sdate = date('y-m-01');
		 $m_edate = date("y-m-t"); 
		
		//For get this year
		 $y_sdate = date('y-01-01');
		 $y_edate = date('Y-12-31'); 

		 // to get the count of today's delivery
		 $now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
		$todaysDelivery = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', 'ThaaliDelivery.delivery_date' => $now, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
		])->count();
		$this->set('todaysDelivery', $todaysDelivery);

		// to get the count of this week delivery
		$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id ,  'ThaaliDelivery.thaali_size !=' => '0',' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate,    'ThaaliDelivery.order_status' => '1')
		])->count();
		//debug($thisWeekDelivery);
		$this->set('thisWeekDelivery', $thisWeekDelivery);
		
		// to get the count of this month delivery 
		$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1')
		])->count();
		$this->set('thisMonthDelivery', $thisMonthDelivery);
		
		// to get the count of this year delivery 
		$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
		])->count();
		$this->set('thisYearDelivery', $thisYearDelivery);
		
		// to get the count of All time delivery
		$allTimeDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2',  'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
		])->count();
		$this->set('allTimeDelivery', $allTimeDelivery);
		// End of Driver Delivery Statistics
		
		// count by month
		
		$query = $this->ThaaliDelivery->find();
		$monthsDelivery  = $query
						    ->select([ 
										'count' => $query->func()->count('id'),
										'mon' => 'MONTH(delivery_date)'
									])
							->where(['driver_id' => $id,  'delivery_type' => '2', 'delivery_date >=' => $y_sdate, 'delivery_date <=' => $y_edate,  'order_status' => '1'])
							->group('mon');

	    $res    = array(1 => '0', 2 => '0', 3 => '0', 4 => '0', 5 => '0', 6 => '0', 7 => '0', 8 => '0', 9 => '0', 10 => '0', 11 => '0', 12 => '0');
		
		foreach ($monthsDelivery as $monthsDelivery):  
			$res[$monthsDelivery['mon']] =  $monthsDelivery['count'];
		endforeach;
		 
		$this->set('monthsDelivery', $res);
		  
		// User Payment information
		$userPaymentHistory = $this->UserPayment->find('all')
							->hydrate(false)
							->select(['created', 'payment_type', 'amount', 'payment_status'])
							
							->where([' UserPayment.user_id' => $id] )
							->order(['created' => 'DESC']);
			$this->set('userPaymentHistory', $userPaymentHistory);
     
		
		// Get Miqat list
		$miqat = $this->Miqat->find('all')
        				->hydrate(false)
        				->select(['start_date', 'end_date', 'details'])
        				->where([' status' => '1'] )
						->order(['start_date' => 'DESC']);
		//echo "<pre>";print_r($miqat); 
		 $this->set('miqat', $miqat);
		 
		//User Vacation list 
		$userVacationPlanner = $this->UserVacationPlanner->find('all')
        				->hydrate(false)
        				->select(['start_date', 'end_date'])
        				->where([' user_id' => $id] )
						->order(['start_date' => 'DESC']);
		 
		 $this->set('userVacationPlanner', $userVacationPlanner);
		  
		 
	   // Get Assigned Distribution Name
       $query = $this->Users->find()
       ->hydrate(false)
       ->select(['u.name'])
       ->join([
       		'c' => [
       				'table' => 'user_distribution_mapping',
       				'type' => 'LEFT',
       				'conditions' => [ 'Users.id' => $id, 'c.user_id = Users.id'  ]
       		],
       		'u' => [
       				'table' => 'distribution_center',
       				'type' => 'INNER ',
       				'conditions' => 'u.id = c.distribution_id',
       		]
       ],
       [' Users.id' => $id]);
       
       if ($query->count() > 0 ) {
       		$query = $query->toArray();
      		$this->set('distributionName', $query[0]['u']['name']);
       }
       else 
       {
       		$this->set('distributionName', '');
       }	
       
       // Get Assigned Driver Name
       $query = $this->Users->UserDriverMapping->find()
     
        ->contain([
    		'Users' => function ($q) use($id) {
    		return $q
    		 
    		 ->where(['UserDriverMapping.user_id' => $id]);
    		}
    		]);
        if ($query->count() > 0 ) { 
		foreach ($query as $driver) {
      		 $driverId = $driver->driver_id;
      	}
		
		if ($driverId > 0) {
			$query = $this->Users->get($driverId);
		}
        $query = $query->toArray();
      	$this->set('driverName', $query['first_name']." ".$query['middle_name']." ".$query['last_name']);
        }
        else 
        	$this->set('driverName', '');
        
       if(!empty($user->user_thaali_info) > 0 ) {
        	foreach ($user->user_thaali_info as $thaaliInfo) {
        	$this->set('thaali_size', $thaaliInfo->thaali_size);
        	$this->set('delivery_method',$thaaliInfo->delivery_method);
        	$this->set('driver_notes', $thaaliInfo->driver_notes);
        	$this->set('active_days', $thaaliInfo->active_days);
        }	
        }
        else
        {
        	$this->set('thaali_size','');
        	$this->set('delivery_method','');
        	$this->set('driver_notes', '');
        	$this->set('active_days', '');
        }
        
        $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
        $this->set('thaaliSize', $thaaliSize);
         
        $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
        $this->set('deliveryMethod', $deliveryMethod);
        
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()	{
    	$role = $this->Users->Role->find('list', ['limit' => 200]);
		
    	$this->set('role', $role);
    	$states = $this->Users->States->find('list', ['limit' => 200]);
    	$this->set('states', $states);
    	
    	$mobileCarriers = $this->Users->MobileCarrierList->find('list', ['limit' => 200]);
        $this->set('mobileCarriers', $mobileCarriers);
    	
         $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
         $this->set('thaaliSize', $thaaliSize);
         
         $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
         $this->set('deliveryMethod', $deliveryMethod);
		 
		 $days = array('1' =>'Monday',  '2' =>  'Tuesday', '3' => 'Wednesday', '4' =>  'Thursday', '5' =>  'Friday');
         $this->set('days', $days);
		
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) { 
            $user = $this->Users->patchEntity($user, $this->request->data);
           if ($this->Users->save($user)) {
               
           	    $distributionInfo = $this->Users->UserDistributionMapping->newEntity();
         		$distributionInfo->user_id = $user->id;
         		$distributionInfo->distribution_id 	= 1;
         		$this->Users->UserDistributionMapping->save($distributionInfo);
				
				//Insert Thaali
				$UserThaaliInfo = $this->Users->UserThaaliInfo->newEntity();			
				$UserThaaliInfo->user_id = $user->id; 
				$UserThaaliInfo->thaali_size		= $this->request->data['thaali_size'];
				$UserThaaliInfo->delivery_method 	= $this->request->data['delivery_method'];
				$UserThaaliInfo->driver_notes  		= $this->request->data['driver_notes'];
				$UserThaaliInfo->active_days 		= implode(',', $this->request->data['active_days']);
				$this->Users->UserThaaliInfo->save($UserThaaliInfo);
				
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['controller' => 'dashboard', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
		
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)	{
    	$user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'UserThaaliInfo']
        ]);
    	$noDays = USER_THAALI_UPDATE_DAYS;
        $role = $this->Users->Role->find('list', ['limit' => 200]);
        $states = $this->Users->States->find('list', ['limit' => 200]);

        $mobileCarriers = $this->Users->MobileCarrierList->find('list', ['limit' => 200]);
        $this->set('mobileCarriers', $mobileCarriers);
         
        $userStatus = array('0' => 'Unapproved', '1' => 'active', '2' => 'Inactive', '3' => 'Trashed - Unapproved User', '4' => 'Trashed- Approved User');
        $this->set('userStatus', $userStatus);
         
        $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
        $this->set('thaaliSize', $thaaliSize);
         
        $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
        $this->set('deliveryMethod', $deliveryMethod); 
         
        $days = array('1' =>'Monday',  '2' =>  'Tuesday', '3' => 'Wednesday', '4' =>  'Thursday', '5' =>  'Friday');
        $this->set('days', $days);
         
        if (!$this->request->is(['post'])) {
         	$this->set('role', $role);
         	$this->set('role_id',$user->role); 
         	$this->set('states', $states);
         	$this->set('state_id',$user->state);
         	$this->set('mobile_carrier',$user->mobile_carrier);
         	$this->set('registration_type', $user->registration_type);
         	$this->set('status', $user->status);
         }	
        
        if(!empty($user->user_thaali_info) > 0 ) {
        	foreach ($user->user_thaali_info as $thaaliInfo) {
        	$this->set('thaali_id', $thaaliInfo->id);
        	$this->set('thaali_size', $thaaliInfo->thaali_size);
        	$this->set('delivery_method',$thaaliInfo->delivery_method);
        	$this->set('driver_notes', $thaaliInfo->driver_notes);
        	$this->set('active_days', $thaaliInfo->active_days);
        	}	
        }
        else {
        	$this->set('thaali_id','');
        	$this->set('thaali_size','');
        	$this->set('delivery_method','');
        	$this->set('driver_notes', '');
        	$this->set('active_days', '');
        }
            
     	if ($this->request->is(['patch', 'post', 'put'])) {
         		$user = $this->Users->patchEntity($user, $this->request->data);
         		if ($this->Users->save($user)) {
         			    //Update User Thaali information
         			    $UserThaaliInfo = $this->Users->UserThaaliInfo->newEntity();
	         			
	         			if ($user->user_role != 3 && isset($this->request->data['thaali_id']))	{
		         			$UserThaaliInfo->id = $this->request->data['thaali_id'];
		         			$UserThaaliInfo->user_id = $user->id; 
		         			$UserThaaliInfo->thaali_size		= $this->request->data['thaali_size'];
		         	    	$UserThaaliInfo->delivery_method 	= $this->request->data['delivery_method'];
		         			$UserThaaliInfo->driver_notes  		= $this->request->data['driver_notes'];
		         			$UserThaaliInfo->active_days 		= implode(',', $this->request->data['active_days']);
		         		    
		         		    if($this->request->data['thaali_size'] != '') {
		         				$this->Users->UserThaaliInfo->save($UserThaaliInfo);
		         				$this->__updateUserThaaliDeliveryInfo ($user->id, $noDays, 'update' );
		         			}
		         		   
		         		} 
		         		//$this->Flash->success(__('The user details has been updated successfully.'));
         	            return $this->redirect(['action' => 'index']);
         	   }  	 
         	   else  {
		         $this->Flash->error(__('The user could not be saved. Please, try again.'));
		       }
        }
     $this->set(compact('user'));
     $this->set('_serialize', ['user']);
    } 
    
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)	{
    	$this->loadModel('ThaaliDelivery');
    	$sdate = date('y-m-d');
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($user->status == 0)
        	$user->status = 3;
        else if ($user->status == 1 || $user->status == 2)
        	$user->status = 4;
        
        if ($this->Users->save($user)) {
        	//Update User Thaali order status
        	$query = $this->ThaaliDelivery->query();
        	$query->update()
        	->set(['order_status' => '2'])
        	->where(['delivery_date >' =>  $sdate, 'user_id' => $id ])
        	->execute();
        	// End of Update User Thaali order status
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function login()	{
    	$this->viewBuilder()->layout('login');
    	 
    	if ($this->request->is('post')) {
    
    		$user = $this->Auth->identify();
    		if ($user['user_role'] == '1' || $user['user_role'] == '2') {
    			if ($user['status'] == '1'){
    				$this->Auth->setUser($user);
    				//return $this->redirect($this->Auth->redirectUrl());
    				return $this->redirect(['controller' => 'dashboard']);
    			
    			}
    			else if ($user['status'] == '0')
    				$this->Flash->error(__('Your account is not activated, try again'));
    			else if ($user['status'] == '2')
    				$this->Flash->error(__('Your account has been disabled'));
    			else
    				$this->Flash->error(__('Invalid username or password, try again'));
    		}
    		else if ($user['user_role'] > '2' ) {
    			$this->Flash->error(__('Access denied'));
    		}
    		else
    			$this->Flash->error(__('Invalid username or password, try again'));
    	}
    }
    
    
	
	 public function enable($id = null) {
		
		// Checking user driver mapping
		$this->loadModel('UserDriverMapping');
		$this->loadModel('Thaali');
		$this->loadModel('ThaaliDelivery');
		$this->loadModel('UserVacationPlanner');
		$this->loadModel('DriverReplacement');
		
		$user = $this->Users->get($id);
		$userRole = $user->user_role;
		 
			$count  = $this->UserDriverMapping->find('all', [
					'conditions' => array( ' UserDriverMapping.user_id' => $id)
			])->count();
			
			if ($userRole != 3 && $count == 0) {
				$this->Flash->error(__('Driver is not assigned for this user. Please assign driver first.'));
			}
			else {	
				$user = $this->Users->get($id);
				$currStatus = $user->status;
				$user->status = '1';
				$to    = $user->email_address;
				if ($this->Users->save($user)) {
					$cc 	= array();
					$secondaryEmail = '';
					
					if ($user->secondary_email_address != ''){
						$secondaryEmail  = $user->secondary_email_address; 
						$secondaryEmail  = explode(',', $secondaryEmail);
					}
					 
					 $msg = 'User account has been activated';
					 
					$this->Flash->success(__($msg));
					
					$sub = 'Faizchicago - Account Status';
					$this->__sendUserEmailNotification ($to, $sub, $msg , $secondaryEmail) ;
					if($userRole != 3){  
						 $this->__updateUserThaaliDeliveryInfo($id); }
					
				} else {
					$this->Flash->error(__('The user could not be enabled. Please try again.'));
				}  
			}
		   return $this->redirect(['action' => 'index']);
    }
   
    public function disable($id = null)	{
    	$this->loadModel('ThaaliDelivery');
    	$user = $this->Users->get($id);
    	$user->status = '2';
    	$sdate = date('Y-m-d');
       	if ($this->Users->save($user)) {
       		//Update User Thaali order status 
       		$query = $this->ThaaliDelivery->query();
       		$query->update()
       		->set(['order_status' => '2'])
       		->where(['delivery_date >' =>  $sdate, 'user_id' => $id ])
       		->execute();
       		// End of Update User Thaali order status
    		$this->Flash->success(__('The user has been disabled.'));
    		$to	=  $user->email_address;
    		$sub = 'Faizchicago - Account status';
    		$msg = 'Your account has been deactivated.';

    		$cc 	= array();
    		$secondaryEmail = '';
    		 
    		if ($user->secondary_email_address != ''){
    			$secondaryEmail  = $user->secondary_email_address; 
    		    $secondaryEmail  = explode(',', $secondaryEmail);
    		}
    		
    	$this->__sendUserEmailNotification ($to, $sub, $msg, $secondaryEmail);
    	} else {
    		$this->Flash->error(__('The user could not be disable. Please try again.'));
    	}
    	 
    	return $this->redirect(['action' => 'index']);
    }
   
    
    
    public function resetPwd($userId)	{
    	if (empty($userId)) {
    			return null;
    		}
    		// Generate a random string 100 chars in length.
    		$hash = $this->__rand_string( 5 );
    		$user = $this->Users->get($userId);
    		 
    		$user->password = $hash;
    		$user->modifiedated_at     = date('Y-m-d H:i:s');
    		$to	=  $user->email_address;
    		if ($this->Users->save($user)) {
    		    $sub = 'Faizchicago - Password Reset';
    			$msg = 'We have reset your password as per your request. Your new password is '.$hash;
    			$this->__sendUserEmailNotification ($to, $sub, $msg);
    			$this->Flash->success(__('Password has been reset and send to your registered email address. Your new password is '.$hash));
    			//return $this->redirect(['action' => 'index']);
    		}
    		else {
    			$this->Flash->error(__('The password could not be generated. Please try again.'));
    		}
    		//$this->autoRender = false;
    		return $this->redirect(['action' => 'index']);
    }

    //Customer Change Password
    public function updatePassword() {
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	$user = $this->Users->get($id, [
    			'contain' => []
    	]);
    	 
    	if ($this->request->is(['patch', 'post', 'put'])) {
    		$user = $this->Users->patchEntity($user, $this->request->data);
    		if ($this->Users->save($user)) {
    			$this->Flash->success(__('Your password has been updated successfully.'));
    			$to	=  $user->email_address;
    			$sub = 'Faizchicago - Password Update';
    			$msg = 'Your password has been updated.';
    			$this->__sendUserEmailNotification ($to, $sub, $msg);
    			return $this->redirect(['action' => 'view', $id]);
    		}
    		else {
    			$this->Flash->error(__('Cannot update your password. Please try again.'));
    		}
    	}
    	 
    	$this->set(compact('user'));
    	$this->set('_serialize', ['user']);
    }
    
    
    public function forgotPassword()	{
    		if ($this->request->is('post')) {
	    		 $ejamaat_id = $this->request->data('ejamaat_id');
	    		 $email = $this->request->data('email_address');
	    		 $userCount = $this->Users->find()->where(['ejamaatid ' => $ejamaat_id, 'email_address' => $email])->count();
	    		 
	    		 if ($userCount == 1) {
	    		 	$userDetails = $this->Users->find()->where(['ejamaatid ' => $ejamaat_id, 'email_address' => $email])->first();
	    		 	$userStatus = $userDetails->status;
	    		 	switch ($userStatus)	{
	    		 	
	    		 	case '0' :
	    		 				$this->Flash->error(__('Your account is not activated, try again'));
	    		 				break;
	    		 	case '1' :
	    		 				$this->__resetPassword($userDetails->id);
	    		 				break;
	    		 	case '2' :
	    		 				$this->Flash->error(__('Your account has been disabled'));
	    		 				break;
	    		 	case '3' :
	    		 				$this->Flash->error(__('Your account has been disabled'));
	    		 				break;
	    		 	case '4' :
	    		 				$this->Flash->error(__('Your account has been disabled'));
	    		 				break;
	    		 	}		
	    		 }
	    		 else 
	    		 	$this->Flash->error(__('Invalid Ejamaat Id or Email address, try again'));
	    	}
    }
    
    public function logout()	{
    	$this->Flash->success('You are logged out');
    	return $this->redirect($this->Auth->logout());
    }
    
    
    function __rand_string( $length )	{
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$str ='';
    	$size = strlen( $chars );
    	for( $i = 0; $i < $length; $i++ ) {
    		$str .= $chars[ rand( 0, $size - 1 ) ];
    	}
    
    	return $str;
    }
    
    public function __resetPassword ($userId)	{
    	if (empty($userId)) {
    		return null;
    	}
    	// Generate a random string 100 chars in length.
    	$hash = $this->__rand_string( 5 );
    	$user = $this->Users->get($userId);
    	 
    	$user->password = $hash;
        $user->modifiedated_at     = date('Y-m-d H:i:s');
        $to	=  $user->email_address;
    	if ($this->Users->save($user)) {
    		 $sub = 'Faizchicago - Password Reset';
    		 $msg = 'We have reset your password as per your request. Your new password is '.$hash; 
    		 $this->__sendUserEmailNotification ($to, $sub, $msg);
    		 $this->Flash->success(__('Your password has been reset and send to your registered email address. Your new password is '.$hash));
    	}
    	else {
    		$this->Flash->error(__('The password could not be generated. Please try again.'));
    	}
    }
    
    public function __updateUserThaaliDeliveryInfo ($userId, $days = null, $type= null) { 
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('DriverReplacement');
    	
        // Update delivery details from next day onwards
    	//Getting user information
    	$query = $this->Users->find('all')
    	->hydrate(false)
    	->select(['Users.id', 'd.driver_id', 't.thaali_size', 'ud.distribution_id','t.delivery_method', 't.driver_notes', 't.active_days'])
    	->join([
    			't' => [
    					'table' => 'user_thaali_info',
    					'type' => 'RIGHT',
    					'conditions' =>  'Users.id = t.user_id',
    			],
    			'd' => [
    					'table' => 'user_driver_mapping ',
    					'type' => 'LEFT ',
    					'conditions' => 'Users.id = d.user_id',
    			],
    			'ud' => [
    					'table' => 'user_distribution_mapping  ',
    					'type' => 'LEFT ',
    					'conditions' => 'Users.id = ud.user_id',
    			],
    	])
    	->where([' Users.id' => $userId]);
    	
    	foreach ($query as $info) {
    		$user_id =  $info['id'];
    		$driver_id = $info['d']['driver_id'];
    		$thaali_size = $info['t']['thaali_size'];
    		$distribution_id = $info['ud']['distribution_id'];
    		$delivery_type = $info['t']['delivery_method'];
    		$delivery_notes = $info['t']['driver_notes'];
    		$userWeekDays = $info['t']['active_days'];
    	}	
    	
    	if ($days > 0 )
    		$sdate = date('Y-m-d',  strtotime("+$days days"));
    	else
    	{
    		$sdate = date('Y-m-d',  strtotime("+1 days"));
    		$query = $this->ThaaliDelivery->query();
    		$query->update()
    		->set(['order_status' => '0'])
    		->where(['delivery_date >=' =>  $sdate, 'user_id' => $userId ])
    		->execute();
    	}
    		
    	 
    	// Getting Thaali information for the upcoming days
    	$connection = ConnectionManager::get('default');
     	$thaali = $connection->execute('SELECT id AS `id`,menu_date FROM thaali Thaali
								WHERE  Thaali.menu_date >="'.$sdate.'"  AND (DAYOFWEEK(Thaali.menu_date))-1 in ('.$userWeekDays.') ');
  
    	//Deleting old upcoming data thaali information from thaali delivery table
     
    
    	$query = $this->ThaaliDelivery->query();
    	$query->delete()
    	->where(['delivery_date >=' =>  $sdate, 'user_id' =>  $userId ])
    	->execute();
    	// end of thaali delivery deletion
     
    	
    	//inserting thaali delivery information
    	foreach ($thaali as $info) {
    		$ThaaliDelivery = $this->ThaaliDelivery->newEntity();
    		$ThaaliDelivery->user_id =  $user_id ;
    		$ThaaliDelivery->driver_id = $driver_id;
    		$ThaaliDelivery->thaali_size = $thaali_size;
    		$ThaaliDelivery->distribution_id = $distribution_id;
    		$ThaaliDelivery->delivery_type = $delivery_type;
    		$ThaaliDelivery->delivery_notes = $delivery_notes;
    		$ThaaliDelivery->order_status = $this->__checkUserVacationDetails($userId, $info['menu_date']);
    		$ThaaliDelivery->thaali_id = $info['id'];
    	  	$ThaaliDelivery->delivery_date = $info['menu_date'];
    	 
    		// Checking driver replacment data
    		$driverData = $this->DriverReplacement->find()
    		->select('replace_driver_id')->
    		where(['from_date <=' => $info['menu_date'], 'to_date >=' => $info['menu_date'], 'driver_id' => $driver_id])->order(['id' => 'DESC'])->first();
    		if (count($driverData)> 0 ) {
				foreach ($driverData as $driver) {
    				$driverId = $driver->replace_driver_id;
    				$ThaaliDelivery->driver_id = $driverId;
    			}
    		 }
    		
    		 $this->ThaaliDelivery->save($ThaaliDelivery);
    		
          } 
          
          if ($days == 7) {
          	$msg = 'The user details has been updated successfully.The Thaali changes will be effet from after '.$days.' days.';
          	 
          }
          else {
          	$msg =   'The user details has been updated successfully and user thaali information has been modified from tomorrow onwards.';
          }
          $this->Flash->success(__($msg));
    	
    	$user  = $this->Users->get($userId);
    	$cc 	= array();
    	$secondaryEmail = '';
    	$to    = $user->email_address;
    	if ($user->secondary_email_address != ''){
    		$secondaryEmail  = $user->secondary_email_address;
    		$secondaryEmail  = explode(',', $secondaryEmail);
    	}
    	$sub = 'Faizchicago - Thaali Status';
    	$msg = "Your account has been activated and Thaali has been scheduled.";
    	$this->__sendUserEmailNotification ($to, $sub, $msg , $secondaryEmail) ;
    	
  
    }

    public function editThaaliDelivery() {
    	// if ($this->request->is('ajax')) {
    		$this->autoRender = false;
    		 
			$this->loadModel('ThaaliDelivery');  
		 	$id					= $this->request->data['delivery_id']; 
			$thaaliSize 		= $this->request->data['thaali_size'];
			$deliveryMethod 	= $this->request->data['delivery_method'];
			$driverNotes  		= $this->request->data['driver_notes'];
			 
			$query = $this->ThaaliDelivery->query();
    			$query->update()
    			->set(['thaali_size' => $thaaliSize, 'delivery_type' => $deliveryMethod, 'delivery_notes' => $driverNotes] )
    			->where(['id ' => $id])
    			->execute();

    		    $thaaliDelivery	= $this->ThaaliDelivery->get($id);
    		    $thaaliDelivery = $thaaliDelivery->toArray();
    		 
    			$delDate 			= $thaaliDelivery['delivery_date'];
    		 	$thaaliSize			= $thaaliDelivery['thaali_size'];
    		 	$deliveryMethod 	= $thaaliDelivery['delivery_type'];
    		 	$driverNotes  		= $thaaliDelivery['delivery_notes'];
    			$userId = $thaaliDelivery['user_id'];
    	 	switch ($thaaliSize) {
    				case '0': $thaaliSize = 'None';break;
    				case '1': $thaaliSize = 'Small (1-2 servings)'; break;
    				case '2': $thaaliSize = 'Medium (3-4 Servings)';break;
    				case '3': $thaaliSize =  'Large (5-6 Servings)';break;
    				case '4': $thaaliSize =  'X-Small (Salawat)';break;
    				case '5': $thaaliSize =  'X-Large';break;
    			}
    			
    			switch ($deliveryMethod) {
    				case '1': $deliveryMethod = 'Pick Up'; break;
    				case '2': $deliveryMethod = 'Delivery';break;
    			}
    			
    			$user =  $this->Users->get($userId);
    		 
    			$to	=  $user->email_address;
    		    $sub = 'Faizchicago - Thaali delivery';
    	        $msg = 'Your Thaali info has been updated as per your request <br/> Date :'.$delDate.'<br/> Thaali Size :'.$thaaliSize. '<br/> Delivery Method :'.$deliveryMethod. '<br/> Driver Notes :'.$driverNotes. '>'; 
			    if ($user->secondary_email_address != ''){
			    		$secondaryEmail  = $user->secondary_email_address;
			    		$secondaryEmail  = explode(',', $secondaryEmail);
			    	}
    		     $this->__sendUserEmailNotification ($to, $sub, $msg, $secondaryEmail);
    			$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    			->hydrate(false)
    			->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
    			->join([
    					't' => [
    							'table' => 'thaali',
    							'alias' => 'ThaaliInfo',
    							'type' => 'LEFT ',
    							'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    							'fields' => ['ThaaliInfo.menu_item']
    			
    					],
    					'c' => [
    							'table' => 'caterer',
    							'alias' => 'CatererInfo',
    							'type' => 'LEFT ',
    							'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    							'fields' => ['CatererInfo.name']
    					],
    			])
    			->where([' ThaaliDelivery.id' => $id] );
    			foreach ($thaaliDeleiveryInfo as $thaali):
    			echo "<td>".$thaali['delivery_date']->format('d/m/Y')."</td>";
    			echo "<td>";
    			switch ($thaali['delivery_type']) {
    				case '1': echo 'Pick Up'; break;
    				case '2': echo 'Delivery';break;
    			}
    			echo "</td>";
    			echo "<td>";
    			switch ($thaali['thaali_size']) {
    				case '0': echo 'None';break;
    				case '1': echo 'Small (1-2 servings)'; break;
    				case '2': echo 'Medium (3-4 Servings)';break;
    				case '3': echo 'Large (5-6 Servings)';break;
    				case '4': echo 'X-Small (Salawat)';break;
    				case '5': echo 'X-Large';break;
    			}
    			echo "</td>";
    			echo "<td>". $thaali['delivery_notes']."</td>";
    			echo "<td>".$thaali['ThaaliInfo']['menu_item']."</td>";
    			echo "<td>".$thaali ['CatererInfo']['name']."</td>";
    			echo "<td><a href='javascript:userEdit(". $thaali['id'].")'>Edit</a></td>";
    			endforeach;
		// }	 
			$this->autoRender = false;
    }
    
   /* public function userThaaliDeliveryById ($id = null) {
    	
    	if ($this->request->is('ajax')) {
    		$this->loadModel('ThaaliDelivery');
    		 
    		$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
        				->hydrate(false)
        				->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
        				->join([
        						't' => [
        								'table' => 'thaali',
										'alias' => 'ThaaliInfo',
        								'type' => 'LEFT ',
        								'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
										'fields' => ['ThaaliInfo.menu_item']
										
        						],
        						'c' => [
        								'table' => 'caterer',
										'alias' => 'CatererInfo',
        								'type' => 'LEFT ',
        								'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
										'fields' => ['CatererInfo.name']
        						],
        				])
        				->where([' ThaaliDelivery.id' => $id] );
			 foreach ($thaaliDeleiveryInfo as $thaali):
				 echo "<td>".$thaali['delivery_date']->format('d/m/Y')."</td>";
	        	 echo "<td>";
	        	 switch ($thaali['delivery_type']) {
        		 	case '1': echo 'Pick Up'; break;
        		 	case '2': echo 'Delivery';break;
        		 }
        		 echo "</td>";
        		 echo "<td>";
        		 switch ($thaali['thaali_size']) {
	        		 case '0': echo 'None';break;
	        		 case '1': echo 'Small (1-2 servings)'; break;
	        		 case '2': echo 'Medium (3-4 Servings)';break;
	        		 case '3': echo 'Large (5-6 Servings)';break;
	        		 case '4': echo 'X-Small (Salawat)';break;
	        		 case '5': echo 'X-Large';break;
        		 }
        		 echo "</td>";
        		 echo "<td>". $thaali['delivery_notes']."</td>";
        		 echo "<td>".$thaali['ThaaliInfo']['menu_item']."</td>";
        		 echo "<td>".$thaali ['CatererInfo']['name']."</td>";
        		 echo "<td><a href='javascript:userEdit(". $thaali['id'].")'>Edit</a></td>";
        	  endforeach;  
    	
    	}
    	
    	$this->autoRender = false;
    }*/
     
    public function getUserThaaliDeliveryById ($id = null) {
    	 
    	if ($this->request->is('ajax')) {
    		
    		$id			= $this->request->data['id'];
    		$this->loadModel('ThaaliDelivery');
    		$thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
    		$deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
    		$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    		->hydrate(false)
    		->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
    		->join([
    				't' => [
    						'table' => 'thaali',
    						'alias' => 'ThaaliInfo',
    						'type' => 'LEFT ',
    						'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    						'fields' => ['ThaaliInfo.menu_item']
    
    				],
    				'c' => [
    						'table' => 'caterer',
    						'alias' => 'CatererInfo',
    						'type' => 'LEFT ',
    						'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    						'fields' => ['CatererInfo.name']
    				],
    		])
    		->where([' ThaaliDelivery.id' => $id] );
    		$this->set('thaaliDeleiveryInfo', $thaaliDeleiveryInfo);
    		$this->set('thaaliSize', $thaaliSize);
    		$this->set('deliveryMethod', $deliveryMethod);
    	 }
    }
    
    public function __sendUserEmailNotification ($to,  $sub, $msg, $secondaryEmail=''){
    	$email = new Email('default');
    	if ($secondaryEmail != '') { 
    		$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
    		->emailFormat('both')
    		->to($to)
    		->addBcc($secondaryEmail)
    		->subject($sub)
    		->send($msg);
    	}
    else 
    {
    	$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
    	->emailFormat('both')
    	->to($to)
    	->subject($sub)
    	->send($msg);
    }
    }
    
    public function __checkUserVacationDetails($userId, $menuDate) { 
    	$this->loadModel('UserVacationPlanner');
    	$userVacation = $this->UserVacationPlanner->find()
    	->select('id')->
    	where(['start_date <=' => $menuDate, 'end_date >=' => $menuDate, 'user_id' => $userId])->count();
    	if ($userVacation == 0)
    		return '0';
    	else return '3';
    }
     
}