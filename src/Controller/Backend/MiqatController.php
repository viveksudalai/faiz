<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Mailer\Email;
/**
 * Miqat Controller
 *
 * @property \App\Model\Table\MiqatTable $Miqat
 */
class MiqatController extends AppController
{

	public $paginate = [
			'order' => [
					'Miqat.start_date' => 'desc'
			]
	];
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Paginator');
	}
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    	$miqat = $this->paginate($this->Miqat);
		$this->set(compact('miqat'));
        $this->set('_serialize', ['miqat']);
    }

    /**
     * View method
     *
     * @param string|null $id Miqat id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $miqat = $this->Miqat->get($id, [
            'contain' => []
        ]);

        $this->set('miqat', $miqat);
        $this->set('_serialize', ['miqat']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $miqat = $this->Miqat->newEntity();
        $this->loadModel('Users');
        $this->loadModel('Thaali');
        
        $users = $this->Users->find()
        ->select(['email_address', 'secondary_email_address'])
        ->where(['status =' => '1'])->toArray();
        
        $userEmail = array();
        $secondaryEmail = '';
        
        foreach ($users as $user):
        array_push($userEmail, $user->email_address);
        $secondaryEmail  = $user->secondary_email_address.",";
        endforeach;
     	$secondaryEmail  =  rtrim( $secondaryEmail,',');
        $secondaryEmail  = explode(',', $secondaryEmail);
        $userEmails = array_merge($userEmail, $secondaryEmail);
  		
  		if ($this->request->is('post')) {
         	$miqatStartDate = $this->request->data['start_date'];
	        $miqatEndDate = $this->request->data['end_date'];
	       
	        // Get the current time.
	        $now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
	       // echo $validFrom = $now->addDays(2)->i18nFormat('yyyy-MM-dd');
	        $selectedTime = new Time($miqatStartDate, 'America/Chicago');
	        $dayDiff =   $selectedTime->isWithinNext(2);
	     
	      //  if ($dayDiff >= 1 ) { 
	        //validating thaali date
	        $miqatCount = $this->Miqat->find()
	                             ->where(['start_date <=' => $miqatStartDate, 'end_date >=' => $miqatStartDate])
	                             ->orWhere(['start_date >=' => $miqatEndDate, 'end_date <=' => $miqatEndDate])
	                             ->count();
	      
	        if ($miqatCount == 0 ) {
	        	// Validating Miqatdate with Thaali  date
	        	$thaaliCount = $this->Thaali->find()
	                             ->where(['menu_date <=' => $miqatStartDate, 'menu_date >=' => $miqatStartDate])
	                             ->orWhere(['menu_date >=' => $miqatEndDate, 'menu_date <=' => $miqatEndDate])
	                             ->count();
	        	if ($thaaliCount == 0) {
	        		$miqat = $this->Miqat->patchEntity($miqat, $this->request->data);
	        		
	        		if ($this->Miqat->save($miqat)) {
	        			$email = new Email('default');
	        			$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
	        			->emailFormat('both')
	        			->to('faizchicago@royalcyber.org')
	        		     ->addBcc( $userEmails)
	        			->subject('Faizchicago - Miqat Details')
	        			->send('Miqat event from '.$miqatStartDate.' to '.$miqatEndDate);
	        			
	        			$this->Flash->success(__('The miqat has been created and email sent to all the active users.'));
	        		
	        			return $this->redirect(['action' => 'index']);
	        		} else {
	        			$this->Flash->error(__('The miqat could not be saved. Please, try again.'));
	        		}
	        	}
	        	else
	        		$this->Flash->error(__('Already Thaali menu is created in the selected period. Kindly check again.'));
	        	//End of Miqat date validation
	        }
	        else
	        	$this->Flash->error(__('Already Miqat event is created for the selected period.Kindly check again.'));
	        //end of thaali date validation
        // }        else     	$this->Flash->error(__('Adding/modifying Miqat Events needs to be completed at least 48 hours in advance.'));
        }
        
        
        $this->set(compact('miqat'));
        $this->set('_serialize', ['miqat']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Miqat id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $miqat = $this->Miqat->get($id, [
            'contain' => []
        ]);
         $this->loadModel('Thaali');
         $this->loadModel('Users');
		 $users = $this->Users->find()
        ->select(['email_address', 'secondary_email_address'])
        ->where(['status =' => '1'])->toArray();
        
        $userEmail = array();
        $secondaryEmail = '';
        foreach ($users as $user):
        array_push($userEmail, $user->email_address);
        $secondaryEmail  = $user->secondary_email_address.",";
        endforeach;
     	$secondaryEmail  =  rtrim( $secondaryEmail,',');
        $secondaryEmail  = explode(',', $secondaryEmail);
        $userEmails = array_merge($userEmail, $secondaryEmail);
  		
         if ($this->request->is(['patch', 'post', 'put'])) {
         	$miqatStartDate = $this->request->data['start_date'];
         	$miqatEndDate = $this->request->data['end_date'];
         	
         	// Get the current time.
         	$now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
         	// echo $validFrom = $now->addDays(2)->i18nFormat('yyyy-MM-dd');
         	$selectedTime = new Time($miqatStartDate, 'America/Chicago');
          	$dayDiff =   $selectedTime->isWithinNext(2); 
         	
         	//if ($dayDiff == 1 ) {	     
         		//validating thaali date
          
	        $miqatCount = $this->Miqat->find()
	                             ->where(['start_date <=' => $miqatStartDate, 'end_date >=' => $miqatStartDate])
	                             ->orWhere(['start_date >=' => $miqatEndDate, 'end_date <=' => $miqatEndDate])
	                             ->andWhere(['id !=' => $id])
	                             ->count();
	      
	        if ($miqatCount == 0 ) {
	        	// Validating Miqatdate with Thaali  date
	        	$thaaliCount = $this->Thaali->find()
	                             ->where(['menu_date <=' => $miqatStartDate, 'menu_date >=' => $miqatStartDate])
	                             ->orWhere(['menu_date >=' => $miqatEndDate, 'menu_date <=' => $miqatEndDate])
	                             ->count();
	        	if ($thaaliCount == 0) {
	        		$miqat = $this->Miqat->patchEntity($miqat, $this->request->data);
	        		if ($this->Miqat->save($miqat)) {
						$email = new Email('default');
	        			$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
	        			->emailFormat('both')
	        			->to('faizchicago@royalcyber.org')
	        		     ->addBcc( $userEmails)
	        			->subject('Faizchicago - Miqat Details')
	        			->send('Miqat event from '.$miqatStartDate.' to '.$miqatEndDate);
	        			
	        			$this->Flash->success(__('The miqat has been updated and email sent to all the active users.'));
	        		    return $this->redirect(['action' => 'index']);
	        		} else {
	        			$this->Flash->error(__('The miqat could not be saved. Please, try again.'));
	        		}
	        	}
	        	else
	        		$this->Flash->error(__('Already Thaali menu is created in the selected period. Kindly check again.'));
	        	//End of Miqat date validation
	        }
	        else
	        	$this->Flash->error(__('Already Miqat event is created for the selected period.Kindly check again.'));
	        //end of thaali date validation
         	// }   else  $this->Flash->error(__('Adding/modifying Miqat Events needs to be completed at least 48 hours in advance.'));
        }
        
        $this->set(compact('miqat'));
        $this->set('_serialize', ['miqat']);
    
    } 

    /**
     * Delete method
     *
     * @param string|null $id Miqat id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $miqat = $this->Miqat->get($id);
        if ($this->Miqat->delete($miqat)) {
            $this->Flash->success(__('The miqat has been deleted.'));
        } else {
            $this->Flash->error(__('The miqat could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
