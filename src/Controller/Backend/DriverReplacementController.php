<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * DriverReplacement Controller
 *
 * @property \App\Model\Table\DriverReplacementTable $DriverReplacement
 */
class DriverReplacementController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $driverReplacement = $this->paginate($this->DriverReplacement);

        $this->set(compact('driverReplacement'));
        $this->set('_serialize', ['driverReplacement']);
    }

    /**
     * View method
     *
     * @param string|null $id Driver Replacement id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $driverReplacement = $this->DriverReplacement->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('driverReplacement', $driverReplacement);
        $this->set('_serialize', ['driverReplacement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $driverReplacement = $this->DriverReplacement->newEntity();
        if ($this->request->is('post')) {
            $driverReplacement = $this->DriverReplacement->patchEntity($driverReplacement, $this->request->data);
            if ($this->DriverReplacement->save($driverReplacement)) {
                $this->Flash->success(__('The driver replacement has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The driver replacement could not be saved. Please, try again.'));
            }
        }
        $users = $this->DriverReplacement->Users->find('list', ['limit' => 200]);
        $this->set(compact('driverReplacement', 'users'));
        $this->set('_serialize', ['driverReplacement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Driver Replacement id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $driverReplacement = $this->DriverReplacement->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $driverReplacement = $this->DriverReplacement->patchEntity($driverReplacement, $this->request->data);
            if ($this->DriverReplacement->save($driverReplacement)) {
                $this->Flash->success(__('The driver replacement has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The driver replacement could not be saved. Please, try again.'));
            }
        }
        $users = $this->DriverReplacement->Users->find('list', ['limit' => 200]);
        $this->set(compact('driverReplacement', 'users'));
        $this->set('_serialize', ['driverReplacement']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Driver Replacement id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driverReplacement = $this->DriverReplacement->get($id);
        if ($this->DriverReplacement->delete($driverReplacement)) {
            $this->Flash->success(__('The driver replacement has been deleted.'));
        } else {
            $this->Flash->error(__('The driver replacement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
