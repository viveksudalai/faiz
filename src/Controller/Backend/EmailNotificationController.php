<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Caterer Controller
 *
 * @property \App\Model\Table\CatererTable $Caterer
 */
class EmailNotificationController extends AppController
{
	 
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Paginator');
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
    
		$emailType = unserialize(EMAIL_TYPE);
    	$this->set('emailType', $emailType );
    	
    	if ($this->request->is('post')) {
    		$this->loadModel('Users');
    		$this->loadModel('UserThaaliInfo');
    		
    		$emailType			= $this->request->data['email_type'];
    		//$thaaliDate 		= $this->request->data['thaali_date'];
    		$emailSub 	        = ucwords($this->request->data['email_sub']);
    		$emailMeg  		= $this->request->data['email_body'];
    		
    		if ($emailType == 'General' ) {
    			$users = $this->Users->find()
    			->select(['email_address', 'secondary_email_address'])
    			->where(['status =' => '1']);
    			
    		}
    		else if ($emailType == 'All Households' ) {
    			$users = $this->Users->find()
    			->select(['email_address', 'secondary_email_address'])
    			->where(['status =' => '1', 'user_role !=' => 3]);
    		}
    		else if ($emailType == 'Pick Up Households' ) {
    			$users = $this->Users->find('all')
        				->select(['Users.email_address', 'Users.secondary_email_address'])
        				->join([
        						't' => [
        								'table' => 'user_thaali_info',
        								'type' => 'RIGHT',
        								'conditions' =>  'Users.id = t.user_id',
        						],
        				])
        				->where(['Users.status' => '1', 'delivery_method' => '1']);
        			 	$users =  (object)($users);
        				 
        		}
    		else if ($emailType == 'Delivery Households' ) {
    			$users = $this->Users->find('all')
        				 ->select(['Users.email_address', 'Users.secondary_email_address'])
        				->join([
        						't' => [
        								'table' => 'user_thaali_info',
        								'type' => 'RIGHT',
        								'conditions' =>  'Users.id = t.user_id',
        						],
        				])
        				->where(['Users.status' => '1', 'delivery_method' => '2']);
        				 $users =  (object)($users);
        				 
    		}
    		else if ($emailType == 'Drivers' ) {
    			$users = $this->Users->find()
    			->select(['email_address', 'secondary_email_address'])
    			->where(['status =' => '1', 'OR' => [['user_role' => 3], ['user_role' => 5]]]);
    		}
    		else if ($emailType == 'Admins' ) {
    			 $users = $this->Users->find()
    			->select(['email_address', 'secondary_email_address'])
    			->where(['status =' => '1', 'OR' => [['user_role' => 1], ['user_role' => 2]]]);
    		}
    		else if ($emailType == 'Manual') {
    		 		$emails 	= $this->request->data['emails'];
    		}
    		 
    		$userEmail = array();
    		$secondaryEmail = '';
    		
    		if($emailType != 'Manual') {
	    		foreach ($users as $user):
	    		array_push($userEmail, $user->email_address);
	    		if($user->secondary_email_address != '')
	    			$secondaryEmail  = $user->secondary_email_address.",";
	    		endforeach;
	    		$secondaryEmail  =  rtrim( $secondaryEmail,',');
	    		$secondaryEmail  = explode(',', $secondaryEmail);
	    		$userEmails = array_merge($userEmail, $secondaryEmail);
    		}
    		else {
    			  $userEmails  = explode(',', $emails);
    		}
    		
    	 
    		$email = new Email('default');
    		$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
    		->emailFormat('both')
    		->to('faizchicago@royalcyber.org')
    		->addBcc( $userEmails)
    		->subject($emailSub)
    		->send($emailMeg);
    		
    		$this->Flash->success(__('Email sent successfully.'));
    	    $this->redirect(['action' => 'index']);
    		 
    	}
  }
}
