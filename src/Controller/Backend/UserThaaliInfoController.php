<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * UserThaaliInfo Controller
 *
 * @property \App\Model\Table\UserThaaliInfoTable $UserThaaliInfo
 */
class UserThaaliInfoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userThaaliInfo = $this->paginate($this->UserThaaliInfo);

        $this->set(compact('userThaaliInfo'));
        $this->set('_serialize', ['userThaaliInfo']);
    }

    /**
     * View method
     *
     * @param string|null $id User Thaali Info id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userThaaliInfo = $this->UserThaaliInfo->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userThaaliInfo', $userThaaliInfo);
        $this->set('_serialize', ['userThaaliInfo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userThaaliInfo = $this->UserThaaliInfo->newEntity();
        if ($this->request->is('post')) {
            $userThaaliInfo = $this->UserThaaliInfo->patchEntity($userThaaliInfo, $this->request->data);
            if ($this->UserThaaliInfo->save($userThaaliInfo)) {
                $this->Flash->success(__('The user thaali info has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user thaali info could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserThaaliInfo->Users->find('list', ['limit' => 200]);
        $this->set(compact('userThaaliInfo', 'users'));
        $this->set('_serialize', ['userThaaliInfo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Thaali Info id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userThaaliInfo = $this->UserThaaliInfo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userThaaliInfo = $this->UserThaaliInfo->patchEntity($userThaaliInfo, $this->request->data);
            if ($this->UserThaaliInfo->save($userThaaliInfo)) {
                $this->Flash->success(__('The user thaali info has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user thaali info could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserThaaliInfo->Users->find('list', ['limit' => 200]);
        $this->set(compact('userThaaliInfo', 'users'));
        $this->set('_serialize', ['userThaaliInfo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Thaali Info id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userThaaliInfo = $this->UserThaaliInfo->get($id);
        if ($this->UserThaaliInfo->delete($userThaaliInfo)) {
            $this->Flash->success(__('The user thaali info has been deleted.'));
        } else {
            $this->Flash->error(__('The user thaali info could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
