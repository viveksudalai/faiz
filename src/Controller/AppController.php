<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
	public $helpers = ['CkEditor.Ck'];
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
	 	$this->loadComponent('Auth', [
			'authenticate' => [
				'Form' => [
						
						'fields' => [
							'username' => 'email_address',
							'password' => 'password'
						]
				]
			]
			 
		]); 
	 	
	 	/* prior to 3.1 that u can use scope or contain . finder is the latest one
	 	 * /*'scope' => [
								'Users.status' => 1,
						]
	 	 */
		// Allow the display action so our pages controller
		// continues to work.
		 $this->Auth->allow(['display']); 
		
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
       }
        
        // Login check
        
        if($this->request->session()->read('Auth.User')) {
        	$session = $this->request->session();
        	$user_data = $session->read('Auth.User');
        	$role = $user_data['user_role'];
        	$prefix = $this->request->params['prefix'];
        	
        	 if (( $role == 1 || $role == 2 ) && $prefix == 'backend') {
        		$this->set('loggedIn', true);  
        	}
         
        	else if ( $role > 0 && $prefix == 'customer') {
        		$this->set('loggedIn', true);
        	}
        	else {
        		$this->redirect($this->Auth->logout());
        	}
        	 
        	
        }   else {
        	$this->set('loggedIn', false);
        }   
        
        
    }
    
    public function isAuthorized($user)
    {
    	// Admin can access every action
    	/*if (isset($user['role']) && $user['role'] === 'admin') {
    		return true;
    		 
    	}*/
    	if ($user) {
    		return true;
    	}
    	// Default deny
    	return false;
    }
    
    /* 
     ,	
			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login'
				],
			 'loginRedirect' => [
				'controller' => 'Users',
				'action' => 'view'
			] 
     * /
     */
}
