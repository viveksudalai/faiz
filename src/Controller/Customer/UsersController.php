<?php
namespace App\Controller\Customer;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use PhpParser\Node\Stmt\Else_;
use App\Model\Entity\UserThaaliInfo;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\View\Helper\FormHelper;
use Cake\Mailer\Email;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	var $role = array('Role');
	
	
    public function beforeFilter(Event $event) {
    	$this->Auth->allow(['registration', 'forgotPassword']);
    	$this->viewBuilder()->layout('customer');
    }

    public function index()	{
        //$this->set('users', $this->Users->find('all'));
    	return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    }
    
    public function login()	{
    	$this->viewBuilder()->layout('customer-login');
    	
    	//For get this week
     	
    	$weekstart = date('m/d/Y', strtotime("sunday last week"));
    	$weekend = date('m/d/Y', strtotime("saturday this week"));
    	$this->set('weekstart', $weekstart);
    	$this->set('weekend', $weekend);
    	
    	$thisWeekCalendar = $this->__getThisWeekCalandar();
    	$this->set('thisWeekCalendar', $thisWeekCalendar);
    	if ($this->request->is('post')) {
    		$user = $this->Auth->identify();
    		//if ($user['user_role'] > '2') {
    			if ($user['status'] == '1'){
    				$this->Auth->setUser($user);
    				return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    			}
    			else if ($user['status'] == '0')
    				$this->Flash->error(__('Your account is not activated, try again'));
    			else if ($user['status'] == '2')
    				$this->Flash->error(__('Your account has been disabled'));
    			else
    				$this->Flash->error(__('Invalid username or password, try again'));
    		/*} else {
    			$this->Flash->error(__('Invalid username or password, try again'));
    		}*/
    	}
    }
    
    //Customer LogOut
    public function logout() {
    	$this->Flash->success('You are logged out');
    	return $this->redirect($this->Auth->logout());
    }
    
    //Customer Change Password
    public function changePassword() {
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	$user = $this->Users->get($id, [
    			'contain' => []
    	]);
    	 
    	if ($this->request->is(['patch', 'post', 'put'])) {
    		$user = $this->Users->patchEntity($user, $this->request->data);
    		if ($this->Users->save($user)) {
    			$this->Flash->success(__('Your password has been updated successfully.'));
    			return $this->redirect(['action' => 'view']);
    		}
    		else {
    			$this->Flash->error(__('The user could not be saved. Please, try again.'));
    		}
    	}
    	 
    	$this->set(compact('user'));
    	$this->set('_serialize', ['user']);
    }
    
    public function __resetPassword ($userId) {
    
    	if (empty($userId)) {
    		return null;
    	}
    	// Generate a random string 100 chars in length.
    	$hash = $this->__rand_string( 5 );
    	$user = $this->Users->get($userId);
    
    	$user->password = $hash;
    	$user->modifiedated_at     = date('Y-m-d H:i:s');
    	$to	=  $user->email_address;
    	if ($this->Users->save($user)) {
    		 
    		$email = new Email('default');
    		$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
    		->to($to)
    		->subject('Faizchicago - Password Reset')
    		->send('We have reset your password as per your request. Your new password is '.$hash);
    
    
    		$this->Flash->success(__('Your password has been reset and send to your registered email address.'));
    		//return $this->redirect(['action' => 'index']);
    	}
    	else {
    		$this->Flash->error(__('Password could not be disable. Please, try again.'));
    	}
    }
    
    public function forgotPassword() {
    	$this->viewBuilder()->layout('customer-login');
    	if ($this->request->is('post')) {
    		$ejamaat_id = $this->request->data('ejamaat_id');
    		$email = $this->request->data('email_address');
    		$userCount = $this->Users->find()->where(['ejamaatid ' => $ejamaat_id, 'email_address' => $email])->count();
    		 
    		if ($userCount == 1) {
    			$userDetails = $this->Users->find()->where(['ejamaatid ' => $ejamaat_id, 'email_address' => $email])->first();
    			$userStatus = $userDetails->status;
    			switch ($userStatus)	{
    
    				case '0' :
    					$this->Flash->error(__('Your account is not activated, try again'));
    					break;
    				case '1' :
    					$this->__resetPassword($userDetails->id);
    					break;
    				case '2' :
    					$this->Flash->error(__('Your account has been disabled'));
    					break;
    				case '3' :
    					$this->Flash->error(__('Your account has been Trashed'));
    					break;
    				case '4' :
    					$this->Flash->error(__('Your account has been Trashed'));
    					break;
    			}
    		}
    		else
    			$this->Flash->error(__('Invalid Ejamaat Id or Email address, try again'));
    		 
    	}
    }
    
    function __rand_string( $length ) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$str ='';
    	$size = strlen( $chars );
    	for( $i = 0; $i < $length; $i++ ) {
    		$str .= $chars[ rand( 0, $size - 1 ) ];
    	}
    
    	return $str;
    }
     
    public function registration()	{
    	
        $states = $this->Users->States->find('list', ['limit' => 200]);
    	$this->set('states', $states);
    	$mobileCarriers = $this->Users->MobileCarrierList->find('list', ['limit' => 200]);
        $this->set('mobileCarriers', $mobileCarriers);
    	
         $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
         $this->set('thaaliSize', $thaaliSize);
         
         $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
         $this->set('deliveryMethod', $deliveryMethod);
		 
		 $days = array('1' =>'Monday',  '2' =>  'Tuesday', '3' => 'Wednesday', '4' =>  'Thursday', '5' =>  'Friday');
         $this->set('days', $days);
		
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) { 
            $user = $this->Users->patchEntity($user, $this->request->data);
			$user->registration_type = 2;
			$user->status = 0;
		    $user->user_role = 3;
		//if ($this->data['User']['password'] == $this->Auth->password($this->data['User']['confirm_password']))
		//{
			if ($this->Users->save($user)) {
				
				//Insert Thaali
				$UserThaaliInfo = $this->Users->UserThaaliInfo->newEntity();			
				$UserThaaliInfo->user_id = $user->id; 
				$UserThaaliInfo->thaali_size		= $this->request->data['thaali_size'];
				$UserThaaliInfo->delivery_method 	= $this->request->data['delivery_method'];
				$UserThaaliInfo->driver_notes  		= $this->request->data['driver_notes'];
				$UserThaaliInfo->active_days 		= implode(',', $this->request->data['active_days']);
				$this->Users->UserThaaliInfo->save($UserThaaliInfo);
				
				
				switch ($this->request->data['thaali_size']) {
					case '0': $size = 'None';break;
					case '1': $size =  'Small (1-2 servings)'; break;
					case '2': $size =  'Medium (3-4 Servings)';break;
					case '3': $size =  'Large (5-6 Servings)';break;
					case '4': $size =  'X-Small (Salawat)';break;
					case '5': $size =  'X-Large';break;
				}
				
				switch ($this->request->data['delivery_method']) {
					case '1': $deliveryMethod = 'Pick Up'; break;
					case '2': $deliveryMethod = 'Delivery';break;
				}
				
				
				$st = $this->Users->States->find()
				->select('abbrev')
				->where(['id' => $this->request->data['user_state']]);
				 
				
				foreach($st as $s):
				$userState = $s->abbrev;
				endforeach;
				
				$msg = "Name: ".$this->request->data['first_name']." ". $this->request->data['middle_name']." ".$this->request->data['last_name']."<br/>";
				$msg .= "<b>Address</b>: "."<br/>";
				$msg .=  $this->request->data['address']."<br/>";
				$msg .=  $this->request->data['city'].", ";
				$msg .=  $userState." - ";
				$msg .=  $this->request->data['zipcode']."<br/><br/>";
				$msg .=  "<b>Contact Info</b>"."<br/>";
				$msg .=  "Email: ".$this->request->data['email_address']."<br/>";
				$msg .=  "Home Phone: ".$this->request->data['home_phone']."<br/>";
				$msg .=  "Mobile Phone: ".$this->request->data['mobile_phone']."<br/>";
				$msg .=  "Thaali Size: ".$size."<br/>";
				$msg .=  "Delivery Method: ".$deliveryMethod."<br/>";
				
				/*New User Registration*/
				$customerEmail = $this->request->data['email_address'];
				$customerContent = "Dear ".$this->request->data['first_name']." ". $this->request->data['middle_name']." ".$this->request->data['last_name'].",<br/>";
				$customerContent .= "<br/>";
				$customerContent .= "Thank you for registaring with us. Admin will contact you soon";
				
				
				$email = new Email('default');
				$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
				->emailFormat('both')
				->to('vivek.t@royalcyber.com')
				->subject('Faizchicago - New User Registration')
				->send($msg);
				
				/*Sending Email To Cusotmer*/
				$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
				->emailFormat('both')
				->to($customerEmail)
				->subject('Thanks For Registration - Faizchicago')
				->send($customerContent);
				
				//return $this->redirect(['action' => 'index']);
				$this->Flash->success(__('You have been registered successfully.'));
				return $this->redirect(['controller' => 'Users', 'action' => 'login']);
			} else {
				$this->Flash->error(__('There is the problem in user registration. Please, try again.'));
			}
		//}	
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
   }

   
    public function home() {
    	$user = $this->Auth->identify();
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$role = $user_data['user_role'];
    	$this->set('role', $role);
    	// Getting Notification
    	$this->loadModel('Notification');
    	$edate = date('Y-m-d');
    	$notification = $this->Notification->find()
    	->select('details')
    	->where(['start_date <=' => $edate, 'end_date >=' => $edate]);
    	$info = '';
    
    	foreach ($notification as $not):
    	$info .= $not->details." ";
    	endforeach;
    	$this->set('info', $info);
    }
    
    
    
    public function dashboard() {
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('UserPayment');
    	$this->loadModel('Miqat');
    	
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$this->set('noDays', $noDays);
    	
    	$user = $this->Users->get($id, [
    			'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
    	]);
    	  
    	// Get Assigned Driver Name
    	$query = $this->Users->UserDriverMapping->find()
    	 
    	->contain([
    			'Users' => function ($q) use($id) {
    				return $q
    				 
    				->where(['UserDriverMapping.user_id' => $id]);
    			}
    	]);
    	if ($query->count() > 0 ) {
    		foreach ($query as $driver) {
    			$driverId = $driver->driver_id;
    		}
    	
    		if ($driverId > 0) {
    			$query = $this->Users->get($driverId);
    		}
    		$query = $query->toArray();
    		$this->set('driverName', $query['first_name']." ".$query['middle_name']." ".$query['last_name']);
    	}
    	else
    		$this->set('driverName', '');
    	
    	 
    	
    	if(!empty($user->user_thaali_info) > 0 ) {
    		foreach ($user->user_thaali_info as $thaaliInfo) {
    			$this->set('thaali_size', $thaaliInfo->thaali_size);
    			$this->set('delivery_method',$thaaliInfo->delivery_method);
    			$this->set('driver_notes', $thaaliInfo->driver_notes);
    			$this->set('active_days', $thaaliInfo->active_days);
    		}
    	}
    	else
    	{
    		$this->set('thaali_size','');
    		$this->set('delivery_method','');
    		$this->set('driver_notes', '');
    		$this->set('active_days', '');
    	}
    	$thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
    	$this->set('thaaliSize', $thaaliSize);
    	 
    	$deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
    	$this->set('deliveryMethod', $deliveryMethod);
    	
    	// Household Delivery Statistics
    	 
    	//For get this week
    	 
    	$sdate = date('y-m-d', strtotime("sunday last week"));
    	//$edate = date('y-m-d', strtotime("saturday this week"));
    	$edate = date('y-m-d');
    	 
    	//For get this month
    	 
    	$m_sdate = date('y-m-01');
    	$m_edate = date("y-m-t");
    	 
    	//For get this year
    	$y_sdate = date('y-01-01');
    	$y_edate = date('Y-12-31');
    	$weekstart = date('m/d/Y', strtotime("sunday last week"));
    	$weekend = date('m/d/Y');
    	$this->set('weekstart', $weekstart);
    	$this->set('weekend', $weekend);
    	// to get the count of today's delivery
    	$now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
    	$todaysDelivery = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', 'ThaaliDelivery.delivery_date' => $now, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
    	])->count();
    	$this->set('todaysDelivery', $todaysDelivery);
    	 
    	// to get the count of this week delivery
    	$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id ,  'ThaaliDelivery.thaali_size !=' => '0',' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate,    'ThaaliDelivery.order_status' => '1')
    	])->count();
    	//debug($thisWeekDelivery);
    	$this->set('thisWeekDelivery', $thisWeekDelivery);
    	 
    	// to get the count of this month delivery
    	$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1')
    	])->count();
    	$this->set('thisMonthDelivery', $thisMonthDelivery);
    	 
    	// to get the count of this year delivery
    	$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
    	])->count();
    	$this->set('thisYearDelivery', $thisYearDelivery);
    	 
    	 
    	// count by this week
    	 
    	$query = $this->ThaaliDelivery->find();
    	$weekDelivery  = $query
    	->select([
    			'count' => $query->func()->count('id'),
    			'day' => 'dayname(delivery_date)'
    	])
    	->where(['user_id' => $id,   'delivery_date >=' => $sdate, 'delivery_date <=' => $edate, 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']],  'OR' => [['ThaaliDelivery.delivery_type' => '1'], ['ThaaliDelivery.delivery_type' => '2']]])
    	->group('delivery_date');
    	 //debug($weekDelivery);
    	 
    	 $res    = array('Sunday' => '0', 'Monday'=> '0', 'Tuesday'=> '0', 'Wednesday'=> '0', 'Thursday'=> '0', 'Friday'=> '0', 'Saturday'=> '0');
    	  
    	 foreach ($weekDelivery as $day):
    	  	 $res[$day['day']] =  $day['count'];
    	 endforeach;
    	 
    	 $this->set('weekDelivery', $res);
    	 $this->set('user', $user);
    	 $this->set('_serialize', ['user']);
     }
     
     public function menus($month=null, $year=null) {
     	
     	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	//$noDays = USER_THAALI_UPDATE_DAYS;
    	//$this->set('noDays', $noDays);
		$user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
        ]);
     	
     	if(!empty($user->user_thaali_info) > 0 ) {
     		foreach ($user->user_thaali_info as $thaaliInfo) {
     			$thaali_size = $thaaliInfo->thaali_size;
     			$delivery_method = $thaaliInfo->delivery_method;
     			$driver_notes =  $thaaliInfo->driver_notes;
     		}
     	}
     	 switch ($thaali_size) {
     		case '0': $thaali_size =  'None';break;
     		case '1': $thaali_size =  'Small (1-2 servings)'; break;
     		case '2': $thaali_size =  'Medium (3-4 Servings)';break;
     		case '3': $thaali_size =  'Large (5-6 Servings)';break;
     		case '4': $thaali_size =  'X-Small (Salawat)';break;
     		case '5': $thaali_size =  'X-Large';break;
     	}
     	
     	switch ($delivery_method) {
     		case '1': $delivery_method = 'Pick Up'; break;
     		case '2': $delivery_method = 'Delivery';break;
     	}
     	
     	$this->set('thaali_size', $thaali_size);
     	$this->set('delivery_method',$delivery_method);
     	$this->set('driver_notes', $driver_notes);
     }
     
     public function changesize() {
     	
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     	//$noDays = USER_THAALI_UPDATE_DAYS;
     	$user = $this->Users->get($id, [
     			'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
     	]);
     	
     	if(!empty($user->user_thaali_info) > 0 ) {
     		foreach ($user->user_thaali_info as $thaaliInfo) {
     			$thaali_size = $thaaliInfo->thaali_size;
     			$delivery_method = $thaaliInfo->delivery_method;
     			$driver_notes =  $thaaliInfo->driver_notes;
     		}
     	}
     	switch ($thaali_size) {
     		case '0': $thaali_size =  'None';break;
     		case '1': $thaali_size =  'Small (1-2 servings)'; break;
     		case '2': $thaali_size =  'Medium (3-4 Servings)';break;
     		case '3': $thaali_size =  'Large (5-6 Servings)';break;
     		case '4': $thaali_size =  'X-Small (Salawat)';break;
     		case '5': $thaali_size =  'X-Large';break;
     	}
     	
     	switch ($delivery_method) {
     		case '1': $delivery_method = 'Pick Up'; break;
     		case '2': $delivery_method = 'Delivery';break;
     	}
     	
     	$this->set('thaali_size', $thaali_size);
     	$this->set('delivery_method',$delivery_method);
     	$this->set('driver_notes', $driver_notes);
     }
     
     public function vacation() {
     	$this->loadModel('ThaaliDelivery');
     	$this->loadModel('UserVacationPlanner');
     	$userVacationPlanner = $this->UserVacationPlanner->newEntity();
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$userId = $user_data['id'];
     	$now = Time::now();
     	$min = $now->modify('+7 days')->i18nFormat('MM/dd/yyyy');
     	$this->set('user_id', $userId);
     	$this->set('min', $min);
     	if ($this->request->is('post')) {
     		$startDate = $this->request->data['start_date'];
     		$endDate = $this->request->data['end_date'];
     
     	    $vacationCount = $this->UserVacationPlanner->find()
     		->where(['user_id' =>  $userId , 'OR' => [['start_date <=' => $startDate, 'start_date >=' => $endDate], ['end_date >=' => $startDate, 'end_date <=' => $endDate]]])->count();
     		 
     		 if ($vacationCount == 0 ) {
     			$now = Time::now();
     			$minDate = $now->modify('+7 days')->i18nFormat('yyyy-MM-dd');
     			 
     			if ($minDate <= $startDate ) {
     				$userVacationPlanner = $this->UserVacationPlanner->patchEntity($userVacationPlanner, $this->request->data);
     		        if ($this->UserVacationPlanner->save($userVacationPlanner)) { 
     					$this->Flash->success(__('The user vacation planner has been added succesfully.')); 
     					//Update User Thaali order status
     					$query = $this->ThaaliDelivery->query();
     					$query->update()
     					->set(['order_status' => '3'])
     					->where(['delivery_date >=' =>  $startDate, 'delivery_date <=' =>  $endDate,  'user_id' => $userId ])
     					->execute();
     					// End of Update User Thaali order status
     					return $this->redirect(['controller' => 'users', 'action' => 'menus']);
     				} else $this->Flash->error(__('There is the problem while adding vacation details.'));
     			}
     			else {
     				$this->Flash->error(__('Adding/modifing vacation information will be considered valid only if the event is scheduled for after '. $min));
     			}
     		}
     		else {
     			$this->Flash->error(__('The selected date already added in your Vacation Planner list.'));
     		}
     	}
     	$users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
     	$this->set(compact('userVacationPlanner', 'users'));
     	$this->set('_serialize', ['userVacationPlanner']);
     }
     
     
     public function __checkUserVacationDetails($userId, $menuDate) {
     	$this->loadModel('UserVacationPlanner');
     	$userVacation = $this->UserVacationPlanner->find()
     	->select('id')->
     	where(['start_date <=' => $menuDate, 'end_date >=' => $menuDate, 'user_id' => $userId])->count();
     	if ($userVacation == 0)
     		return '0';
     	else return '3';
     }
     
     public function survey() {
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     	//$noDays = USER_THAALI_UPDATE_DAYS;
     	$user = $this->Users->get($id, [
     			'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
     	]);
     	
     	if(!empty($user->user_thaali_info) > 0 ) {
     		foreach ($user->user_thaali_info as $thaaliInfo) {
     			$thaali_size = $thaaliInfo->thaali_size;
     			$delivery_method = $thaaliInfo->delivery_method;
     			$driver_notes =  $thaaliInfo->driver_notes;
     		}
     	}
     	switch ($thaali_size) {
     		case '0': $thaali_size =  'None';break;
     		case '1': $thaali_size =  'Small (1-2 servings)'; break;
     		case '2': $thaali_size =  'Medium (3-4 Servings)';break;
     		case '3': $thaali_size =  'Large (5-6 Servings)';break;
     		case '4': $thaali_size =  'X-Small (Salawat)';break;
     		case '5': $thaali_size =  'X-Large';break;
     	}
     	
     	switch ($delivery_method) {
     		case '1': $delivery_method = 'Pick Up'; break;
     		case '2': $delivery_method = 'Delivery';break;
     	}
     	
     	$this->set('thaali_size', $thaali_size);
     	$this->set('delivery_method',$delivery_method);
     	$this->set('driver_notes', $driver_notes);
     	
     }
     public function getSurveyinfo()
	 {
		$monthstart = '2017-01-01';//start Date
    	$monthend =   '2017-05-31';//end Date
    	$thaaliList = array();
    	$this->loadModel('Thaali');
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('Miqat');
		$this->loadModel('ThaaliSurvey');
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    
    	 // User Thaali delivery information
    	$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    	->select(['id', 'delivery_date', 'ThaaliInfo.menu_item',  'ThaaliDelivery.thaali_size',  'CatererInfo.name','order_status','thaali_id','ThaaliSurvey.rating'])
    	->join([
    			't' => [
    					'table' => 'thaali',
    					'alias' => 'ThaaliInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    					'fields' => ['ThaaliInfo.menu_item']
    					 
    			],
    			'c' => [
    					'table' => 'caterer',
    					'alias' => 'CatererInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    					'fields' => ['CatererInfo.name']
    			],
				's' => [
    					'table' => 'thaali_survey',
    					'alias' => 'ThaaliSurvey',
    					'type' => 'LEFT ',
    					'conditions' => 'ThaaliSurvey.thaali_id = ThaaliInfo.id AND ThaaliSurvey.user_id ='.$id,
    					'fields' => ['ThaaliSurvey.rating']
    			]
    	])
    	->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
    	->order(['delivery_date' => 'DESC']);
    
    	$calandar = array();
    
    	 
    	foreach($thaaliDeleiveryInfo as $thaali):
    	switch ($thaali['thaali_size']) {
    		case '0': $thaali_size =  'None';break;
    		case '1': $thaali_size =  'Small Thaali'; break;
    		case '2': $thaali_size =  'Medium Thaali';break;
    		case '3': $thaali_size =  'Large Thaali';break;
    		case '4': $thaali_size =  'X-Small Thaali)';break;
    		case '5': $thaali_size =  'X-Large Thaali';break;
    	}
    	$res = array();
    	$res['id'] = $thaali['id'];
    	if($thaali['order_status']== 1 && $thaali['ThaaliSurvey']['rating'] == '' && $thaali['delivery_date'] < date('Y-m-d') ){  
			$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<a href="javascript:thaaliReview('.$thaali['thaali_id'].')"><div class="fc-edit-content" data-toggle="modal" data-target="#review_dialog"><span>Add Review</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div></a>';
    	}
		else if($thaali['order_status']== 0 )
    	{
    		$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'];
    	}
		else if($thaali['order_status']== 1 && $thaali['ThaaliSurvey']['rating'] != ''){  
			$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<div class="fc-edit-content"><span>Review Added</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div>';
    	}
    	
    	$res['start'] = $thaali['delivery_date']->format('Y-m-d');
    	$res['cssclass'] = 'blue';
    	$res['allDay'] = false;
    	array_push($calandar, $res);
    	endforeach;
        
    	echo json_encode($calandar);
    	exit(); 
	 }
     public function drivers() {
     
     	$driverDetails = $this->paginate = [
     			'contain' => ['States'],
     			'conditions' => ['Users.status ' => '1', 'OR' => [['user_role' => '3' ], ['user_role' => '5']]] ,
     			'order' => ['Users.created' => 'DESC']
     	];
     	$driverDetails = $this->paginate($this->Users);
     	$this->set('driverDetails', $driverDetails);
     }
      
     
     public function contactus() {
     	$this->loadModel('ContactFeeds');
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     
     	 
     
     	if ($this->request->is('post')) {
     		$contactFeed = $this->ContactFeeds->newEntity();
     		$contactFeed->user_id = $id;
     		$contactFeed->message = $this->request->data['msg'];
     		$to ='vivek.t@royalcyber.com';
     		$msg = "Name: ".$user_data['first_name']." ". $user_data['middle_name']." ".$user_data['last_name']."<br/>";
     		$msg .=  "Email: ".$user_data['email_address']."<br/>";
     		$msg .=  "Home Phone: ".$user_data['home_phone']."<br/>";
     		$msg .=  "Mobile Phone: ".$user_data['mobile_phone']."<br/><br/>";
     		$msg .=  "Message: ".$this->request->data['msg']."<br/>";
     		 
     		if ($this->ContactFeeds->save($contactFeed)) {
     			$this->Flash->success(__('Your message has been send succesfully.'));
     			$email = new Email('default');
     			$email->from(['faizchicago@royalcyber.org' => 'Faizchicago'])
     			->emailFormat('both')
     			->to($to)
     			->subject('Faizchicago - Contact Us')
     			->send($msg);
     
     			return $this->redirect(['action' => 'contactus']);
     		} else {
     			$this->Flash->error(__('There is a problem while sending your message.'));
     		}
     	}
     
     }
     
     public function deliveries() {
     	$this->loadModel('ThaaliDelivery');
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     	$fromDate = date('Y-m-d') ;
     	$toDate =  date('Y-m-d') ;
     	if ($this->request->is('post')) {
     		$fromDate = $this->request->data['dtfrom'];
     		$toDate = $this->request->data['dtto'];
     	
     	}
     	// Driver Thaali delivery information
     	$driverDeleiveryInfo = $this->ThaaliDelivery->find('all')
     	->hydrate(false)
     	->select(['delivery_date', 'delivery_type', 'delivery_notes', 'thaali_size', 'Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode', 'States.abbrev'])
     	->join([
     
     			'c' => [
     					'table' => 'users',
     					'alias' => 'Users',
     					'type' => 'LEFT ',
     					'conditions' => 'Users.id = ThaaliDelivery.user_id',
     					'fields' => ['Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode']
     			],
     			's' => [
     					'table' => 'states',
     					'alias' => 'States',
     					'type' => 'LEFT ',
     					'conditions' => 'States.id = Users.user_state',
     					'fields' => ['States.abbrev']
     
     			],
     	])
     	->where([' ThaaliDelivery.driver_id' => $id, ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $fromDate, 'ThaaliDelivery.delivery_date <=' => $toDate, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
     	->order(['delivery_date' => 'DESC']);
     	
     	//debug($driverDeleiveryInfo);
     	$this->set('driverDeleiveryInfo', $driverDeleiveryInfo);
     	$this->set('fromdate',$fromDate);
     	$this->set('todate',$toDate);
    }
     
     public function deliveryStatistics() {
     	$this->loadModel('ThaaliDelivery');
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     
     
     	// Driver Delivery Statistics
     
     	//For get this week
     
     	$sdate = date('y-m-d', strtotime("sunday last week"));
     	//$edate = date('y-m-d', strtotime("saturday this week"));
     	$edate = date('y-m-d');
     
     	//For get this month
     
     	$m_sdate = date('y-m-01');
     	$m_edate = date("y-m-t");
     
     	//For get this year
     	$y_sdate = date('y-01-01');
     	$y_edate = date('Y-12-31');
     
     	// to get the count of today's delivery
     	$now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
     	$todaysDelivery = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', 'ThaaliDelivery.delivery_date' => $now, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
     	])->count();
     	$this->set('todaysDelivery', $todaysDelivery);
     
     	// to get the count of this week delivery
     	$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id ,  'ThaaliDelivery.thaali_size !=' => '0',' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate,    'ThaaliDelivery.order_status' => '1')
     	])->count();
     	//debug($thisWeekDelivery);
     	$this->set('thisWeekDelivery', $thisWeekDelivery);
     
     	// to get the count of this month delivery
     	$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1')
     	])->count();
     	$this->set('thisMonthDelivery', $thisMonthDelivery);
     
     	// to get the count of this year delivery
     	$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
     	])->count();
     	$this->set('thisYearDelivery', $thisYearDelivery);
     
     	// to get the count of All time delivery
     	$allTimeDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2',  'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
     	])->count();
     	$this->set('allTimeDelivery', $allTimeDelivery);
     	// End of Driver Delivery Statistics
     
     	// count by month
     
     	$query = $this->ThaaliDelivery->find();
     	$monthsDelivery  = $query
     	->select([
     			'count' => $query->func()->count('id'),
     			'mon' => 'MONTH(delivery_date)'
     	])
     	->where(['driver_id' => $id,  'delivery_type' => '2', 'delivery_date >=' => $y_sdate, 'delivery_date <=' => $y_edate,  'order_status' => '1'])
     	->group('mon');
     
     	$res    = array(1 => '0', 2 => '0', 3 => '0', 4 => '0', 5 => '0', 6 => '0', 7 => '0', 8 => '0', 9 => '0', 10 => '0', 11 => '0', 12 => '0');
     
     	foreach ($monthsDelivery as $monthsDelivery):
     	$res[$monthsDelivery['mon']] =  $monthsDelivery['count'];
     	endforeach;
     	 
     	$this->set('monthsDelivery', $res);
     
     }
     
     public function __getThisWeekCalandar() {
     	$thaaliDetails = $this->__getThisWeekThaali();
     	$miqatDetails = $this->__getThisWeekMiqat();

     	$result = array_merge($thaaliDetails, $miqatDetails);
     	if(count($result) > 0) {
        foreach ($result as $key => $row) {
       	 $mid[$key]  = $row['date'];
        }
        array_multisort($mid, SORT_ASC, $result);
     	}
       return $result;
     }
     
     
     
     public function __getThisWeekThaali() {
     	$weekstart = date('y-m-d', strtotime("sunday last week"));
     	$weekend =  date('y-m-d', strtotime("saturday this week"));
     	$thaaliList = array();
     	$this->loadModel('Thaali');
     	$thisWeekThaali = $query = $this->Thaali->find()
		->select(['Caterer.name','Thaali.menu_date', 'Thaali.menu_item'])
		->join([
				'c' => [
						'table' => 'caterer',
						'alias' => 'Caterer',
						'type' => 'LEFT',
						'conditions' => 'Thaali.caterer_id = Caterer.id',
						'fields' => ['Caterer.name']
				],
				 
		])
		->where([ 'date(Thaali.menu_date) >=' => $weekstart, 'date(Thaali.menu_date) <=' => $weekend ]);
		$thisWeekThaali = $thisWeekThaali->toArray();
		$res = array();
			foreach($thisWeekThaali as $thaali):
			$res['type'] = 'Thaali';
	     	$res['date'] = $thaali['menu_date']->format('Y-m-d');
	     	$res['title'] =  $thaali['menu_item'];
	     	$res['details'] = $thaali['Caterer']['name'];
	     	array_push($thaaliList, $res) ;
	    endforeach;
	   return $thaaliList;			     	 
     }
     
     public function __getThisWeekMiqat() {
     	$weekList = array();
     	$miqatList = array();
     	$weekstart = strtotime("sunday last week");
     //	$weekend =  date('y-m-d', strtotime("saturday this week"));
     	if (date('Y-m-d', $weekstart) == date('Y-m-d', time() - 7*24*3600)) {
     		// we are that day... => add one week
     		$weekstart += 7 * 24 * 3600;
     	}
     	
     	$currentDay = $weekstart;
     	for ($i = 0 ; $i < 7 ; $i++) {
     		array_push($weekList, date('Y-m-d', $currentDay));
     		$currentDay += 24 * 3600;
     	}
     	 
     	$this->loadModel('Miqat');
     	
     	foreach ($weekList as $mday): 
	     	$thisWeekMiqat = $query = $this->Miqat->find('all')
	     	->select(['event_title','start_date', 'end_date','details'])
	     	->where([ 'date(Miqat.start_date) <=' => $mday, 'date(Miqat.end_date) >=' => $mday ]);
	     	$res = array();
	     	foreach($thisWeekMiqat as $miqat):
	     	$res['type'] = 'Miqat';
	     	$res['date'] = $mday;
	     	$res['title'] = $miqat->event_title;
	     	$res['details'] = $miqat->details;
	     	array_push($miqatList, $res) ;
	     	endforeach;
	     	
     	endforeach; 
     	return $miqatList;
     }
    
	public function view()	{
		$this->loadModel('ThaaliDelivery');
		$this->loadModel('UserVacationPlanner');
		$this->loadModel('UserPayment');
		$this->loadModel('Miqat');
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$this->set('noDays', $noDays);
		$user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
        ]);
		
		// User Thaali delivery information
		$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
		->hydrate(false)
		->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
		->join([
				't' => [
						'table' => 'thaali',
						'alias' => 'ThaaliInfo',
						'type' => 'LEFT ',
						'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
						'fields' => ['ThaaliInfo.menu_item']
		
				],
				'c' => [
						'table' => 'caterer',
						'alias' => 'CatererInfo',
						'type' => 'LEFT ',
						'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
						'fields' => ['CatererInfo.name']
				],
		])
		->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
		->order(['delivery_date' => 'DESC']);
		$this->set('thaaliDeleiveryInfo', $thaaliDeleiveryInfo);
		 
		// Driver Thaali delivery information
		$driverDeleiveryInfo = $this->ThaaliDelivery->find('all')
		->hydrate(false)
		->select(['delivery_date', 'delivery_type', 'delivery_notes', 'thaali_size', 'Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode', 'States.abbrev'])
		->join([
		
				'c' => [
						'table' => 'users',
						'alias' => 'Users',
						'type' => 'LEFT ',
						'conditions' => 'Users.id = ThaaliDelivery.user_id',
						'fields' => ['Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode']
				],
				's' => [
						'table' => 'states',
						'alias' => 'States',
						'type' => 'LEFT ',
						'conditions' => 'States.id = Users.user_state',
						'fields' => ['States.abbrev']
		
				],
		])
		->where([' ThaaliDelivery.driver_id' => $id, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
		->order(['delivery_date' => 'DESC']);
		$this->set('driverDeleiveryInfo', $driverDeleiveryInfo);
		
		// Driver Delivery Statistics
		
		//For get this week
		
		$sdate = date('y-m-d', strtotime("sunday last week"));
		//$edate = date('y-m-d', strtotime("saturday this week"));
		$edate = date('y-m-d');
		
		//For get this month
		
		$m_sdate = date('y-m-01');
		$m_edate = date("y-m-t");
		
		//For get this year
		$y_sdate = date('y-01-01');
		$y_edate = date('Y-12-31');
		
		// to get the count of today's delivery
		$now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
		$todaysDelivery = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', 'ThaaliDelivery.delivery_date' => $now, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
		])->count();
		$this->set('todaysDelivery', $todaysDelivery);
		
		// to get the count of this week delivery
		$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id ,  'ThaaliDelivery.thaali_size !=' => '0',' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate,    'ThaaliDelivery.order_status' => '1')
		])->count();
		//debug($thisWeekDelivery);
		$this->set('thisWeekDelivery', $thisWeekDelivery);
		
		// to get the count of this month delivery
		$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1')
		])->count();
		$this->set('thisMonthDelivery', $thisMonthDelivery);
		
		// to get the count of this year delivery
		$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
		])->count();
		$this->set('thisYearDelivery', $thisYearDelivery);
		
		// to get the count of All time delivery
		$allTimeDelivery  = $this->ThaaliDelivery->find('all', [
				'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2',  'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
		])->count();
		$this->set('allTimeDelivery', $allTimeDelivery);
		// End of Driver Delivery Statistics
		
		// count by month
		
		$query = $this->ThaaliDelivery->find();
		$monthsDelivery  = $query
		->select([
				'count' => $query->func()->count('id'),
				'mon' => 'MONTH(delivery_date)'
		])
		->where(['driver_id' => $id,  'delivery_type' => '2', 'delivery_date >=' => $y_sdate, 'delivery_date <=' => $y_edate,  'order_status' => '1'])
		->group('mon');
		
		$res    = array(1 => '0', 2 => '0', 3 => '0', 4 => '0', 5 => '0', 6 => '0', 7 => '0', 8 => '0', 9 => '0', 10 => '0', 11 => '0', 12 => '0');
		
		foreach ($monthsDelivery as $monthsDelivery):
		$res[$monthsDelivery['mon']] =  $monthsDelivery['count'];
		endforeach;
			
		$this->set('monthsDelivery', $res);
		
		// User Payment information
		$userPaymentHistory = $this->UserPayment->find('all')
		->hydrate(false)
		->select(['created', 'payment_type', 'amount', 'payment_status'])
			
		->where([' UserPayment.user_id' => $id] )
		->order(['created' => 'DESC']);
		$this->set('userPaymentHistory', $userPaymentHistory);
		 
		
		// Get Miqat list
		$miqat = $this->Miqat->find('all')
		->hydrate(false)
		->select(['start_date', 'end_date', 'details'])
		->where([' status' => '1'] )
		->order(['start_date' => 'DESC']);
		//echo "<pre>";print_r($miqat);
		$this->set('miqat', $miqat);
			
		//User Vacation list
		$userVacationPlanner = $this->UserVacationPlanner->find('all')
		->hydrate(false)
		->select(['start_date', 'end_date'])
		->where([' user_id' => $id] )
		->order(['start_date' => 'DESC']);
			
		$this->set('userVacationPlanner', $userVacationPlanner);
		
			
		
       // Get Assigned Distribution Name
       $query = $this->Users->find()
       ->hydrate(false)
       ->select(['u.name'])
       ->join([
       		'c' => [
       				'table' => 'user_distribution_mapping',
       				'type' => 'LEFT',
       				'conditions' => [ 'Users.id' => $id, 'c.user_id = Users.id'  ]
       		],
       		'u' => [
       				'table' => 'distribution_center',
       				'type' => 'INNER ',
       				'conditions' => 'u.id = c.distribution_id',
       		]
       ],
       [' Users.id' => $id]);
       
       if ($query->count() > 0 ) {
       		$query = $query->toArray();
      		$this->set('distributionName', $query[0]['u']['name']);
       }
       else 
       {
       		$this->set('distributionName', '');
       }	
       
       // Get Assigned Driver Name
       $query = $this->Users->UserDriverMapping->find()
     
        ->contain([
    		'Users' => function ($q) use($id) {
    		return $q
    		 
    		 ->where(['UserDriverMapping.user_id' => $id]);
    		}
    		]);
        if ($query->count() > 0 ) { 
		foreach ($query as $driver) {
      		 $driverId = $driver->driver_id;
      	}
		
		if ($driverId > 0) {
			$query = $this->Users->get($driverId);
		}
        $query = $query->toArray();
      	$this->set('driverName', $query['first_name']." ".$query['middle_name']." ".$query['last_name']);
        }
        else 
        	$this->set('driverName', '');
        
        
        
        if(!empty($user->user_thaali_info) > 0 ) {
        	foreach ($user->user_thaali_info as $thaaliInfo) {
        		$this->set('thaali_size', $thaaliInfo->thaali_size);
        		$this->set('delivery_method',$thaaliInfo->delivery_method);
        		$this->set('driver_notes', $thaaliInfo->driver_notes);
        		$this->set('active_days', $thaaliInfo->active_days);
        	}
        }
        else
        {
        	$this->set('thaali_size','');
        	$this->set('delivery_method','');
        	$this->set('driver_notes', '');
        	$this->set('active_days', '');
        }
        $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
        $this->set('thaaliSize', $thaaliSize);
         
        $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
        $this->set('deliveryMethod', $deliveryMethod);
        
        
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
        
        // Getting Notification
        $this->loadModel('Notification');
        $edate = date('Y-m-d');
        $notification = $this->Notification->find()
    	->select('details') 
    	->where(['start_date <=' => $edate, 'end_date >=' => $edate]); 
    	 $info = ' ';
    	
        foreach ($notification as $not):
        	$info .= $not->details." ";
        endforeach;
        $this->set('info', $info);
    }

	public function edit()	{
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
        $user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'UserThaaliInfo']
        ]);
        
         $noDays = USER_THAALI_UPDATE_DAYS;
         $role = $this->Users->Role->find('list', ['limit' => 200]);
         $states = $this->Users->States->find('list', ['limit' => 200]);
         $userStatus = array('0' => 'Unapproved', '1' => 'active', '2' => 'Inactive', '3' => 'Trashed - Unapproved User', '4' => 'Trashed- Approved User');
         $this->set('userStatus', $userStatus);
         $mobileCarriers = $this->Users->MobileCarrierList->find('list', ['limit' => 200]);
         $this->set('mobileCarriers', $mobileCarriers);
          
         $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
         $this->set('thaaliSize', $thaaliSize);
         
         $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
         $this->set('deliveryMethod', $deliveryMethod); 
         
         $days = array('1' =>'Monday',  '2' =>  'Tuesday', '3' => 'Wednesday', '4' =>  'Thursday', '5' =>  'Friday');
         $this->set('days', $days);
         
         if (!$this->request->is(['post'])) {
         	$this->set('role', $role);
         	$this->set('role_id',$user->role);
         	$this->set('states', $states);
         	$this->set('state_id',$user->state);
         	$this->set('registration_type', $user->registration_type);
         	$this->set('mobile_carrier',$user->mobile_carrier);
         	$this->set('status', $user->status);
         }	
         
         if(!empty($user->user_thaali_info) > 0 ) {
        	foreach ($user->user_thaali_info as $thaaliInfo) {
        	$this->set('thaali_id', $thaaliInfo->id);
        	$this->set('thaali_size', $thaaliInfo->thaali_size);
        	$this->set('delivery_method',$thaaliInfo->delivery_method);
        	$this->set('driver_notes', $thaaliInfo->driver_notes);
        	$this->set('active_days', $thaaliInfo->active_days);
        }	
        }
        else
        {
        	$this->set('thaali_size','');
        	$this->set('delivery_method','');
        	$this->set('driver_notes', '');
        	$this->set('active_days', '');
        }
         	
         	if ($this->request->is(['patch', 'post', 'put'])) {
         		$user = $this->Users->patchEntity($user, $this->request->data);
         		if ($this->Users->save($user)) {
         			 
	         		//Update User Thaali information
	         		 $UserThaaliInfo = $this->Users->UserThaaliInfo->newEntity();
	         			 
	         		 	if ($user->user_role != 3 )	{
		         			$UserThaaliInfo->id = $this->request->data['thaali_id'];
		         			$UserThaaliInfo->user_id = $user->id; 
		         			$UserThaaliInfo->thaali_size		= $this->request->data['thaali_size'];
		         	    	$UserThaaliInfo->delivery_method 	= $this->request->data['delivery_method'];
		         			$UserThaaliInfo->driver_notes  		= $this->request->data['driver_notes'];
		         			$UserThaaliInfo->active_days 		= implode(',', $this->request->data['active_days']);
		         		    
		         		    if($this->request->data['thaali_size'] != '') {
		         				$this->Users->UserThaaliInfo->save($UserThaaliInfo);
		         				$this->__updateUserThaaliDeliveryInfo ($user->id, $noDays, 'update' );
		         			}
		         		 }	 
		         		 //$this->Flash->success(__('The user details has been updated successfully.'));
         	  			 return $this->redirect(['action' => 'dashboard']);
         		} else {
         			$this->Flash->error(__('The user could not be saved. Please, try again.'));
         		}
         	}  	 
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
	
	
    
    public function getUserThaaliDeliveryById ($id = null) {
    	$this->viewBuilder()->layout('empty');
    	if ($this->request->is('ajax')) {
    
    		$id			= $this->request->data['id'];
    		$this->loadModel('ThaaliDelivery');
    		$thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
    		$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    		->hydrate(false)
    		->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
    		->join([
    				't' => [
    						'table' => 'thaali',
    						'alias' => 'ThaaliInfo',
    						'type' => 'LEFT ',
    						'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    						'fields' => ['ThaaliInfo.menu_item']
    
    				],
    				'c' => [
    						'table' => 'caterer',
    						'alias' => 'CatererInfo',
    						'type' => 'LEFT ',
    						'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    						'fields' => ['CatererInfo.name']
    				],
    		])
    		->where([' ThaaliDelivery.id' => $id] );
    		$this->set('thaaliDeleiveryInfo', $thaaliDeleiveryInfo);
    		$this->set('thaaliSize', $thaaliSize);
    	}
    }
    public function editThaaliDelivery() {
    	if ($this->request->is('ajax')) {
    		$this->autoRender = false;
    		 
    		$this->loadModel('ThaaliDelivery');
    		$id			= $this->request->data['delivery_id'];
    		$thaaliSize			= $this->request->data['thaali_size'];
    		
    		$query = $this->ThaaliDelivery->query();
    		$query->update()
    		->set(['thaali_size' => $thaaliSize] )
    		->where(['id ' => $id])
    		->execute();
    
    	}
    	$this->autoRender = false;
    }
    
    public function __updateUserThaaliDeliveryInfo ($userId, $days = null, $type= null) {
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('DriverReplacement');
    	 
    	// Update delivery details from next day onwards
    	//Getting user information
    	$query = $this->Users->find('all')
    	->hydrate(false)
    	->select(['Users.id', 'd.driver_id', 't.thaali_size', 'ud.distribution_id','t.delivery_method', 't.driver_notes', 't.active_days'])
    	->join([
    			't' => [
    					'table' => 'user_thaali_info',
    					'type' => 'RIGHT',
    					'conditions' =>  'Users.id = t.user_id',
    			],
    			'd' => [
    					'table' => 'user_driver_mapping ',
    					'type' => 'LEFT ',
    					'conditions' => 'Users.id = d.user_id',
    			],
    			'ud' => [
    					'table' => 'user_distribution_mapping  ',
    					'type' => 'LEFT ',
    					'conditions' => 'Users.id = ud.user_id',
    			],
    	])
    	->where([' Users.id' => $userId]);
    	 
    	foreach ($query as $info) {
    		$user_id =  $info['id'];
    		$driver_id = $info['d']['driver_id'];
    		$thaali_size = $info['t']['thaali_size'];
    		$distribution_id = $info['ud']['distribution_id'];
    		$delivery_type = $info['t']['delivery_method'];
    		$delivery_notes = $info['t']['driver_notes'];
    		$userWeekDays = $info['t']['active_days'];
    	}
    	 
    	if ($days > 0 )
    		$sdate = date('Y-m-d',  strtotime("+$days days"));
    	else
    	{
    		$sdate = date('Y-m-d',  strtotime("+1 days"));
    		$query = $this->ThaaliDelivery->query();
    		$query->update()
    		->set(['order_status' => '0'])
    		->where(['delivery_date >=' =>  $sdate, 'user_id' => $id ])
    		->execute();
    	}
    
    
    	// Getting Thaali information for the upcoming days
    	$connection = ConnectionManager::get('default');
    	$thaali = $connection->execute('SELECT id AS `id`,menu_date FROM thaali Thaali
								WHERE  Thaali.menu_date >="'.$sdate.'"  AND (DAYOFWEEK(Thaali.menu_date))-1 in ('.$userWeekDays.') ');
    	//Deleting old upcoming data thaali information from thaali delivery table
    	 
    
    	$query = $this->ThaaliDelivery->query();
    	$query->delete()
    	->where(['delivery_date >=' =>  $sdate, 'user_id' =>  $userId ])
    	->execute();
    	// end of thaali delivery deletion
    	 
    	 
    	//inserting thaali delivery information
    	foreach ($thaali as $info) {
    		$ThaaliDelivery = $this->ThaaliDelivery->newEntity();
    		$ThaaliDelivery->user_id =  $user_id ;
    		$ThaaliDelivery->driver_id = $driver_id;
    		$ThaaliDelivery->thaali_size = $thaali_size;
    		$ThaaliDelivery->distribution_id = $distribution_id;
    		$ThaaliDelivery->delivery_type = $delivery_type;
    		$ThaaliDelivery->delivery_notes = $delivery_notes;
    		$ThaaliDelivery->order_status = $this->__checkUserVacationDetails($user_id, $info['menu_date']);
    		$ThaaliDelivery->thaali_id = $info['id'];
    		$ThaaliDelivery->delivery_date = $info['menu_date'];
    
    		// Checking driver replacment data
    		$driverData = $this->DriverReplacement->find()
    		->select('replace_driver_id')->
    		where(['from_date <=' => $info['menu_date'], 'to_date >=' => $info['menu_date'], 'driver_id' => $driver_id])->order(['id' => 'DESC'])->first();
    		//if ($driverData->count() > 0 ) {
    		foreach ($driverData as $driver) {
    			$driverId = $driver->replace_driver_id;
    			$ThaaliDelivery->driver_id = $driverId;
    		}
    		//}
    
    		$this->ThaaliDelivery->save($ThaaliDelivery);
    		
    	}
    	if ($days == 7)
    			$this->Flash->success(__('The user details has been saved.The Thaali changes will be effet from after '.$days." days."));
    		else 
    			$this->Flash->success(__('The user details has been saved.'));
    
    }
    
	public function getCustomercalendar($monthstart = null,$monthend = null)  {
     $monthstart = '2017-01-01';//start Date
     $monthend =   '2017-05-31';//end Date
     $thaaliList = array();
     $this->loadModel('Thaali');
     $this->loadModel('ThaaliDelivery');
     $this->loadModel('UserVacationPlanner');
        $this->loadModel('Miqat');
     $session = $this->request->session();
     $user_data = $session->read('Auth.User');
     $id = $user_data['id'];
      
     $user = $this->Users->get($id, [
       'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
     ]);
     
     // User Thaali delivery information
     $thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
     ->select(['id', 'delivery_date', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
     ->join([
       't' => [
         'table' => 'thaali',
         'alias' => 'ThaaliInfo',
         'type' => 'LEFT ',
         'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
         'fields' => ['ThaaliInfo.menu_item']
     
       ],
       'c' => [
         'table' => 'caterer',
         'alias' => 'CatererInfo',
         'type' => 'LEFT ',
         'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
         'fields' => ['CatererInfo.name']
       ],
     ])
     ->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
     ->order(['delivery_date' => 'DESC']);

     $calandar = array();
    
     
     foreach($thaaliDeleiveryInfo as $thaali):  
      $res = array();
      $res['id'] = $thaali['id'];
      $res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."<hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'];
      $res['start'] = $thaali['delivery_date']->format('Y-m-d');
   	  $res['allDay'] = true;
      array_push($calandar, $res);
     endforeach;

     // Get Miqat list
     $miqat = $this->Miqat->find('all')
     ->select(['event_title','start_date', 'end_date', 'details'])
     ->where([' status' => '1'] )
     ->order(['start_date' => 'DESC']);
        
     //User Vacation list
     $userVacationPlanner = $this->UserVacationPlanner->find('all')
     ->select(['start_date', 'end_date'])
     ->where([' user_id' => $id] )
     ->order(['start_date' => 'DESC']);
 
        foreach($miqat as $mq):
         $res = array();
      $res['id'] = '';
      $res['title'] =  $mq['event_title']."<br/> ". $mq['details'];
      $res['start'] = $mq['start_date']->format('Y-m-d');
      $res['end'] =  $mq['end_date']->format('Y-m-d');
  	  $res['color'] = 'green';
      $res['allDay'] = true;
      array_push($calandar, $res);
     endforeach;
  //$calandar =  array_merge($calandar,$res );
     
   
     foreach($userVacationPlanner as $vacation):
      $res = array();
      $res['id'] = "";
      $res['title'] = "Vacation Period - ". $vacation['start_date']->format('m/d/Y'). "  ". $vacation['end_date']->format('m/d/Y') ;
      $res['start'] = $vacation['start_date']->format('Y-m-d');
      $res['end'] =  $vacation['end_date']->format('Y-m-d');
      $res['color'] = 'red';
      $res['textColor'] = 'white';
      $res['allDay'] = true;
      array_push($calandar, $res);
     endforeach;

     echo json_encode($calandar);
     exit();
    }
    
    public function getCustomereditcalendar($monthstart = null,$monthend = null)  {
    	$monthstart = '2017-01-01';//start Date
    	$monthend =   '2017-05-31';//end Date
    	$thaaliList = array();
    	$this->loadModel('Thaali');
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('Miqat');
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    
    	 // User Thaali delivery information
    	$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    	->select(['id', 'delivery_date', 'ThaaliInfo.menu_item',  'ThaaliDelivery.thaali_size',  'CatererInfo.name'])
    	->join([
    			't' => [
    					'table' => 'thaali',
    					'alias' => 'ThaaliInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    					'fields' => ['ThaaliInfo.menu_item']
    					 
    			],
    			'c' => [
    					'table' => 'caterer',
    					'alias' => 'CatererInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    					'fields' => ['CatererInfo.name']
    			],
    	])
    	->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
    	->order(['delivery_date' => 'DESC']);
    
    	$calandar = array();
    
    	 
    	foreach($thaaliDeleiveryInfo as $thaali):
    	switch ($thaali['thaali_size']) {
    		case '0': $thaali_size =  'None';break;
    		case '1': $thaali_size =  'Small Thaali'; break;
    		case '2': $thaali_size =  'Medium Thaali';break;
    		case '3': $thaali_size =  'Large Thaali';break;
    		case '4': $thaali_size =  'X-Small Thaali)';break;
    		case '5': $thaali_size =  'X-Large Thaali';break;
    	}
    	$res = array();
    	$res['id'] = $thaali['id'];
    	if(date('Y-m-d', strtotime("+$noDays days")) <= date('Y-m-d', strtotime($thaali['delivery_date'])) ){  
    	$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<a href="javascript:userEdit('.$thaali['id'].')"><div class="fc-edit-content"><i class="fa fa-truck fa-3 fa-flip-horizontal" aria-hidden="true"></i><span>'.$thaali_size.'</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div></a>';
    	}
    	else 
    	{
    		$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<div class="fc-edit-content"><i class="fa fa-truck fa-3 fa-flip-horizontal" aria-hidden="true"></i><span>'.$thaali_size.'</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div>';
    	}
    	$res['start'] = $thaali['delivery_date']->format('Y-m-d');
    	$res['cssclass'] = 'blue';
    	$res['allDay'] = false;
    	array_push($calandar, $res);
    	endforeach;
        
    	echo json_encode($calandar);
    	exit();
    }
    
	public function surveySubmit()
	{
		$session = $this->request->session();
		$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
		$this->loadModel('ThaaliSurvey');
		$surveyInfo = $this->ThaaliSurvey->newEntity();
		
		if ($this->request->is('post'))
		{
			$surveyInfo->user_id = $id;
			$surveyInfo->thaali_id = $this->request->data['thaali_id'];
			$surveyInfo->thaali_taste = $this->request->data['taste'];
			$surveyInfo->thaali_qty = $this->request->data['happy'];
			$surveyInfo->rating = $this->request->data['rating_value'];
			if($this->ThaaliSurvey->save($surveyInfo))
			{
				echo "Survey Submitted Successfully";
			} 		
			else{
				echo "There is the problem while submitting Survey";
			}
		}
		exit;
	}
}