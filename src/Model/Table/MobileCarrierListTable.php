<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MobileCarrierList Model
 *
 * @method \App\Model\Entity\MobileCarrierList get($primaryKey, $options = [])
 * @method \App\Model\Entity\MobileCarrierList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MobileCarrierList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MobileCarrierList|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MobileCarrierList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MobileCarrierList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MobileCarrierList findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */class MobileCarrierListTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('mobile_carrier_list');
        $this->displayField('carrier_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('carrier_name', 'create')            ->notEmpty('carrier_name');
        $validator
            ->requirePresence('status', 'create')            ->notEmpty('status');
        return $validator;
    }
}
