<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $ContactFeeds
 * @property \Cake\ORM\Association\HasMany $DriverInfo
 * @property \Cake\ORM\Association\HasMany $LoginHistory
 * @property \Cake\ORM\Association\HasMany $ThaaliDelivery
 * @property \Cake\ORM\Association\HasMany $UserDistributionMapping
 * @property \Cake\ORM\Association\HasMany $UserDriverMapping
 * @property \Cake\ORM\Association\HasMany $UserPayment
 * @property \Cake\ORM\Association\HasMany $UserThaaliInfo
 * @property \Cake\ORM\Association\HasMany $UserVacationPlanner
 * @property \Cake\ORM\Association\BelongsTo $Role
 * @property \Cake\ORM\Association\BelongsTo $States
 * @property \Cake\ORM\Association\BelongsTo $MobileCarrierList
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('first_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('ContactFeeds', [
        		'foreignKey' => 'user_id'
        ]);
        
        $this->hasMany('DriverInfo', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('LoginHistory', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ThaaliDelivery', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserDistributionMapping', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserDriverMapping', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserThaaliInfo', [
            'foreignKey' => 'user_id'
        ]);
         $this->hasMany('UserPayment', [
            'foreignKey' => 'user_id'
        ]);
		
		$this->hasMany('UserVacationPlanner', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Role', [
        		'foreignKey' => 'user_role',
        		'joinType' => 'INNER'
        ]);
        $this->belongsTo('States', [
        		'foreignKey' => 'user_state',
        		'joinType' => 'INNER'
        ]);
        $this->belongsTo('MobileCarrierList', [
        		'foreignKey' => 'mobile_carrier',
        		'joinType' => 'INNER'
        		
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ejamaatid', 'create')
            ->notEmpty('ejamaatid')
        	->add('ejamaatid', 'unique', [
        			'rule' =>	'validateUnique',
        			'provider' => 'table',
        			'message' => 'Not unique'
        	]);
        

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('middle_name', 'create')
            ->notEmpty('middle_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('email_address', 'create')
            ->notEmpty('email_address')
            ->add('email_address', 'unique', [
        			'rule' =>	'validateUnique',
        			'provider' => 'table',
            		'message' => 'Not unique'
        	]);
        
        $validator
            ->allowEmpty('secondary_email_address');
		
		$validator
            ->requirePresence('home_phone', 'create')
            ->notEmpty('home_phone');

        $validator
            ->integer('mobile_carrier')
            ->requirePresence('mobile_carrier', 'create')
            ->notEmpty('mobile_carrier');
			
        $validator
            ->requirePresence('mobile_phone', 'create')
            ->notEmpty('mobile_phone');

        $validator
            ->requirePresence('zipcode', 'create')
            ->notEmpty('zipcode');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
        ->requirePresence('city', 'create')
        ->notEmpty('address');

         $validator
            ->integer('state')
            ->requirePresence('user_state', 'create')
            ->notEmpty('user_state');
        
        $validator
            ->requirePresence('registration_type', 'create')
            ->notEmpty('registration_type');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('user_role')
            ->requirePresence('user_role', 'create')
            ->notEmpty('user_role');

        return $validator;
    }
}
