<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ThaaliDelivery Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Thaali
 * @property \Cake\ORM\Association\BelongsTo $DistributionCenter
 *
 * @method \App\Model\Entity\ThaaliDelivery get($primaryKey, $options = [])
 * @method \App\Model\Entity\ThaaliDelivery newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ThaaliDelivery[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ThaaliDelivery|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ThaaliDelivery patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ThaaliDelivery[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ThaaliDelivery findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ThaaliDeliveryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('thaali_delivery');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'driver_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Thaali', [
            'foreignKey' => 'thaali_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DistributionCenter', [
            'foreignKey' => 'distribution_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('thaali_size', 'create')
            ->notEmpty('thaali_size');

        $validator
            ->date('delivery_date')
            ->requirePresence('delivery_date', 'create')
            ->notEmpty('delivery_date');

        $validator
            ->requirePresence('delivery_type', 'create')
            ->notEmpty('delivery_type');

        $validator
            ->requirePresence('delivery_notes', 'create')
            ->notEmpty('delivery_notes');

        $validator
            ->requirePresence('order_status', 'create')
            ->notEmpty('order_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['driver_id'], 'Users'));
        $rules->add($rules->existsIn(['thaali_id'], 'Thaali'));
        $rules->add($rules->existsIn(['distribution_id'], 'DistributionCenter'));

        return $rules;
    }
}
