<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Thaali Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Caterer
 *
 * @method \App\Model\Entity\Thaali get($primaryKey, $options = [])
 * @method \App\Model\Entity\Thaali newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Thaali[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Thaali|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Thaali patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Thaali[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Thaali findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ThaaliTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('thaali');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Caterer', [
            'foreignKey' => 'caterer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        /*$validator
            ->requirePresence('name', 'create')
            ->Empty('name');
        	*/

        $validator
            ->date('menu_date', ['ymd'])
            ->requirePresence('menu_date', 'create')
            ->notEmpty('menu_date');
        	 

        $validator
            ->requirePresence('menu_item', 'create')
            ->notEmpty('menu_item');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['caterer_id'], 'Caterer'));

        return $rules;
    }
}
