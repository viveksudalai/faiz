<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ThaaliSurvey Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Thaalis
 *
 * @method \App\Model\Entity\ThaaliSurvey get($primaryKey, $options = [])
 * @method \App\Model\Entity\ThaaliSurvey newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ThaaliSurvey[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ThaaliSurvey|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ThaaliSurvey patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ThaaliSurvey[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ThaaliSurvey findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */class ThaaliSurveyTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('thaali_survey');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Thaali', [
            'foreignKey' => 'thaali_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('thaali_taste', 'create')            ->notEmpty('thaali_taste');
        $validator
            ->requirePresence('thaali_qty', 'create')            ->notEmpty('thaali_qty');
        $validator
            ->integer('rating')            ->requirePresence('rating', 'create')            ->notEmpty('rating');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['thaali_id'], 'Thaali'));

        return $rules;
    }
}
