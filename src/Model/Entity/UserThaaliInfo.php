<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserThaaliInfo Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $thaali_size
 * @property string $delivery_method
 * @property string $driver_notes
 * @property string $active_days
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserThaaliInfo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
