<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $ejamaatid
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $email_address
 * @property string $secondary_email_address
 * @property string $home_phone
 * @property int $mobile_carrier
 * @property string $mobile_phone
 * @property string $zipcode
 * @property string $password
 * @property string $address
 * @property string $city
 * @property int $user_state
 * @property string $registration_type
 * @property string $status
 * @property int $user_role
 * @property string $color_code
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\DriverInfo[] $driver_info
 * @property \App\Model\Entity\LoginHistory[] $login_history
 * @property \App\Model\Entity\ThaaliDelivery[] $thaali_delivery
 * @property \App\Model\Entity\UserDistributionMapping[] $user_distribution_mapping
 * @property \App\Model\Entity\UserDriverMapping[] $user_driver_mapping
 * @property \App\Model\Entity\UserPayment[] $user_payment
 * @property \App\Model\Entity\UserThaaliInfo[] $user_thaali_info
 * @property \App\Model\Entity\UserVacationPlanner[] $user_vacation_planner
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    
    protected function _setPassword($value) {
    	$hasher = new DefaultPasswordHasher();
    	return $hasher->hash($value);
    }
    
    protected function _getFullName()
    {
    	return  $this->_properties['first_name'] . '  ' .
      			$this->_properties['middle_name'] . '  ' .
    			$this->_properties['last_name'];
    }
    
    protected function _getLabel()
    {  
    	return $this->_properties['first_name'] . ' ' . $this->_properties['middle_name']. ' ' . $this->_properties['last_name'];
    	//. ' / ' . __('User ID %s', $this->_properties['id']);
    }
}
