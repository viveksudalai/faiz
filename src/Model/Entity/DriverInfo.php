<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DriverInfo Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $insurance_provider
 * @property string $insurance_policy_no
 * @property \Cake\I18n\Time $insurance_expiry_date
 * @property string $license_number
 * @property \Cake\I18n\Time $license_expiry_date
 * @property string $vehicle_make
 * @property string $vehicle_model
 * @property \Cake\I18n\Time $vehicle_year
 *
 * @property \App\Model\Entity\User $user
 */
class DriverInfo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
