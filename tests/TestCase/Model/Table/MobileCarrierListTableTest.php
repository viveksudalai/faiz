<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MobileCarrierListTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MobileCarrierListTable Test Case
 */
class MobileCarrierListTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\MobileCarrierListTable     */
    public $MobileCarrierList;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mobile_carrier_list'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MobileCarrierList') ? [] : ['className' => 'App\Model\Table\MobileCarrierListTable'];        $this->MobileCarrierList = TableRegistry::get('MobileCarrierList', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MobileCarrierList);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
