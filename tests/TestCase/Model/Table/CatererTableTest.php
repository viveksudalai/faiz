<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatererTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatererTable Test Case
 */
class CatererTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatererTable
     */
    public $Caterer;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.caterer',
        'app.thaali'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Caterer') ? [] : ['className' => 'App\Model\Table\CatererTable'];
        $this->Caterer = TableRegistry::get('Caterer', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Caterer);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
