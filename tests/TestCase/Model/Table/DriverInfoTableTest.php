<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DriverInfoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DriverInfoTable Test Case
 */
class DriverInfoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DriverInfoTable
     */
    public $DriverInfo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.driver_info',
        'app.users',
        'app.login_history',
        'app.user_distribution_mapping',
        'app.distribution_center',
        'app.user_driver_mapping',
        'app.role'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DriverInfo') ? [] : ['className' => 'App\Model\Table\DriverInfoTable'];
        $this->DriverInfo = TableRegistry::get('DriverInfo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DriverInfo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
