<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ThaaliSurveyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ThaaliSurveyTable Test Case
 */
class ThaaliSurveyTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\ThaaliSurveyTable     */
    public $ThaaliSurvey;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.thaali_survey',
        'app.users',
        'app.contact_feeds',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_thaali_info',
        'app.user_payment',
        'app.user_vacation_planner',
        'app.role',
        'app.states',
        'app.mobile_carrier_list',
        'app.thaalis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ThaaliSurvey') ? [] : ['className' => 'App\Model\Table\ThaaliSurveyTable'];        $this->ThaaliSurvey = TableRegistry::get('ThaaliSurvey', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ThaaliSurvey);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
