<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContactFeedsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContactFeedsTable Test Case
 */
class ContactFeedsTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\ContactFeedsTable     */
    public $ContactFeeds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contact_feeds',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_thaali_info',
        'app.user_payment',
        'app.user_vacation_planner',
        'app.role',
        'app.states',
        'app.mobile_carrier_list'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContactFeeds') ? [] : ['className' => 'App\Model\Table\ContactFeedsTable'];        $this->ContactFeeds = TableRegistry::get('ContactFeeds', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContactFeeds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
