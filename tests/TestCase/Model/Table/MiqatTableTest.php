<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MiqatTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MiqatTable Test Case
 */
class MiqatTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MiqatTable
     */
    public $Miqat;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.miqat'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Miqat') ? [] : ['className' => 'App\Model\Table\MiqatTable'];
        $this->Miqat = TableRegistry::get('Miqat', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Miqat);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
