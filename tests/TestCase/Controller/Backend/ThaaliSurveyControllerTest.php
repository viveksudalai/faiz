<?php
namespace App\Test\TestCase\Controller\Backend;

use App\Controller\Backend\ThaaliSurveyController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Backend\ThaaliSurveyController Test Case
 */
class ThaaliSurveyControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.thaali_survey',
        'app.users',
        'app.contact_feeds',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_thaali_info',
        'app.user_payment',
        'app.user_vacation_planner',
        'app.role',
        'app.states',
        'app.mobile_carrier_list',
        'app.thaalis'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
