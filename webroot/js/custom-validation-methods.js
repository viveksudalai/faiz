jQuery.validator.addMethod("alphanumericspecial", function(value, element) {
        return this.optional(element) || value == value.match(/^[-a-zA-Z0-9_ ]+$/);
        }, "Only letters, Numbers and Space/underscore Allowed.");

    jQuery.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
},"Only Characters Allowed.");

    jQuery.validator.addMethod("alphanumeric", function(value, element) {
return this.optional(element) || value == value.match(/^[a-z0-9A-Z#]+$/);
},"Only Characters, Numbers and Hash Allowed.");
    
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
    	 return this.optional(element) || value != $(param).val();
    	}, "This has to be different...");
 // Edited / adapted from http://forum.jquery.com/topic/jquery-validate-comma-seperated-multiple-emails-validation#14737000002179275
    
    jQuery.validator.addMethod("multiemail", function (value, element) {
        if (this.optional(element)) {
            return true;
        }

        var emails = value.split(','),
            valid = true;

        for (var i = 0, limit = emails.length; i < limit; i++) {
        	jQuery.trim(emails[i])	
            valid = valid && jQuery.validator.methods.email.call(this, value, element);
        }

        return valid;
    }, "Invalid email format: please use a comma to separate multiple email addresses.");