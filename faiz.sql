-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2016 at 05:29 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `faizchicagodemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `caterer`
--

CREATE TABLE IF NOT EXISTS `caterer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `caterer`
--

INSERT INTO `caterer` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Mohammed Yusuf', '2016-08-30 17:05:00', '2016-09-05 09:51:24'),
(2, 'Farida Bhen Kapadia', '2016-09-02 14:30:25', '2016-09-02 14:30:25'),
(3, 'new caterer test', '2016-09-07 13:47:16', '2016-10-20 12:25:21');

-- --------------------------------------------------------

--
-- Table structure for table `distribution_center`
--

CREATE TABLE IF NOT EXISTS `distribution_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `distribution_center`
--

INSERT INTO `distribution_center` (`id`, `name`, `phone`, `mobile`, `address`, `status`, `created`, `modified`) VALUES
(1, 'distribution 1', '123456789', '123456789', ' 7th floor, 123/A Hall Mark Towers, Guindy, Chennai', '1', '2016-08-31 02:23:00', '2016-10-20 12:36:25'),
(2, 'distribution 2', '9840568439', '9840568439', '7th floor,\r\n123/A Hall Mark Towers,\r\nGuindy,\r\nChennai', '1', '2016-09-29 12:26:55', '2016-09-29 12:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `distribution_replacement`
--

CREATE TABLE IF NOT EXISTS `distribution_replacement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distribution_id` int(11) NOT NULL,
  `replace_distribution_id` int(11) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `distribution_id` (`distribution_id`,`replace_distribution_id`),
  KEY `replace_distribution_id` (`replace_distribution_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `distribution_replacement`
--

INSERT INTO `distribution_replacement` (`id`, `distribution_id`, `replace_distribution_id`, `from_date`, `to_date`, `status`, `created`, `modified`) VALUES
(1, 2, 1, '2016-10-21 00:00:00', '2016-10-24 00:00:00', '1', '2016-10-21 10:55:29', '2016-10-21 10:55:29'),
(2, 1, 2, '2016-10-24 00:00:00', '2016-10-25 00:00:00', '1', '2016-10-24 11:30:01', '2016-10-24 11:30:18'),
(3, 1, 2, '2016-11-02 00:00:00', '2016-11-23 00:00:00', '1', '2016-11-02 16:09:30', '2016-11-02 16:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `driver_info`
--

CREATE TABLE IF NOT EXISTS `driver_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `insurance_provider` varchar(255) NOT NULL,
  `insurance_policy_no` varchar(255) NOT NULL,
  `insurance_expiry_date` datetime NOT NULL,
  `license_number` varchar(255) NOT NULL,
  `license_expiry_date` datetime NOT NULL,
  `vehicle_make` varchar(255) NOT NULL,
  `vehicle_model` varchar(50) NOT NULL,
  `vehicle_year` year(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `driver_info`
--

INSERT INTO `driver_info` (`id`, `user_id`, `insurance_provider`, `insurance_policy_no`, `insurance_expiry_date`, `license_number`, `license_expiry_date`, `vehicle_make`, `vehicle_model`, `vehicle_year`) VALUES
(1, 4, 'LASD', '21323213213', '2016-09-02 00:00:00', '21321323213', '2016-09-02 00:00:00', 'Honda', 'honda civic', 2012),
(2, 5, 'gggfg', 'rewrwerewr', '2016-11-16 00:00:00', 'ewrewrewr', '2016-11-23 00:00:00', 'fdsdsf', 'swift', 2012);

-- --------------------------------------------------------

--
-- Table structure for table `driver_replacement`
--

CREATE TABLE IF NOT EXISTS `driver_replacement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `replace_driver_id` int(11) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `driver_id` (`driver_id`,`replace_driver_id`),
  KEY `replace_driver_id` (`replace_driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

CREATE TABLE IF NOT EXISTS `login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `miqat`
--

CREATE TABLE IF NOT EXISTS `miqat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `details` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `miqat`
--

INSERT INTO `miqat` (`id`, `name`, `start_date`, `end_date`, `details`, `status`, `created`, `modified`) VALUES
(1, 'Shab e Meraj', '2016-09-17 14:32:00', '2016-09-18 14:32:00', '	No Thali', '1', '2016-09-02 14:33:07', '2016-09-02 14:33:58'),
(2, 'Yaum al Mabas', '2016-09-21 00:00:00', '2016-09-30 00:00:00', 'No thali', '1', '2016-09-02 14:34:29', '2016-10-20 12:23:34'),
(3, 'test', '2016-10-26 00:00:00', '2016-10-27 00:00:00', 'testing', '0', '2016-09-07 13:46:44', '2016-10-24 10:58:41');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `status`, `created`, `modified`) VALUES
(1, 'Super Admin', '1', '2016-10-20 12:18:01', '2016-10-20 12:24:49'),
(2, 'admin', '1', '2016-10-20 12:18:01', '2016-10-20 12:27:08'),
(3, 'Driver', '1', '2016-10-20 12:18:01', '2016-10-20 12:18:01'),
(4, 'Household', '1', '2016-10-20 12:18:01', '2016-10-20 12:18:01'),
(5, 'user', '1', '2016-10-20 12:18:01', '2016-10-20 12:18:01'),
(6, 'Driver & User', '1', '2016-11-10 14:44:23', '2016-11-10 14:44:23');

-- --------------------------------------------------------

--
-- Table structure for table `thaali`
--

CREATE TABLE IF NOT EXISTS `thaali` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `caterer_id` int(11) NOT NULL,
  `menu_date` datetime NOT NULL,
  `menu_item` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `caterer_id` (`caterer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `thaali`
--

INSERT INTO `thaali` (`id`, `name`, `caterer_id`, `menu_date`, `menu_item`, `created`, `modified`) VALUES
(5, 'dsfdsfdsf', 1, '2016-09-16 11:19:00', 'sfdsfd', '2016-09-16 11:19:41', '2016-09-16 11:19:41'),
(6, 'testing', 3, '2016-09-22 02:43:00', 'test', '2016-09-22 02:43:14', '2016-10-21 11:17:11'),
(7, 'tytry', 2, '2016-09-22 02:43:00', 'tyrtyrty', '2016-10-19 09:24:50', '2016-10-21 11:17:05'),
(9, 'test', 1, '2016-10-19 00:00:00', 'test', '2016-10-19 09:33:52', '2016-10-20 12:21:18'),
(10, 'testing', 1, '2016-11-02 00:00:00', 'test', '2016-11-02 16:07:05', '2016-11-03 16:52:17'),
(11, '', 1, '2016-11-03 00:00:00', 'fd', '2016-11-03 16:54:44', '2016-11-03 16:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ejamaatid` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `mobile_phone` varchar(50) NOT NULL,
  `home_phone` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `registration_type` enum('0','1') NOT NULL,
  `status` enum('0','1','2','3','4') NOT NULL COMMENT '0 - unapproved, 1- active, 2-inactive, 3-trashed before approved, 4 - trashed after approved',
  `user_role` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`user_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ejamaatid`, `first_name`, `middle_name`, `last_name`, `email_address`, `mobile_phone`, `home_phone`, `password`, `address`, `registration_type`, `status`, `user_role`, `created`, `modified`) VALUES
(1, 111, 'vivek', 't', 'sudalai', 'viveksudalai@gmail.com', '9840568439', '9840568439', '$2y$10$mWB5w9g15LbD/imSH.CboOsYSppcEeWSkwiahyugY0TqpzNNcm.Te', 's.v. regency,', '1', '1', 1, '2016-08-01 08:48:00', '2016-10-05 09:21:23'),
(4, 3, 'mohammed', 'yusuf', 'hassan', 'mohammed@gmail.com', '9999998888', '9999998888', '$2y$10$0mFP1wFI0GaI2A/D0oVP2ujJpJdf8uUrF.GcRncwbnD3ZTIJPU3MW', 'No.1, SIDCO Industrial Estate, Guindy, NH45, Guindy, Chennai, Tamil Nadu 600032', '1', '2', 3, '2016-08-31 11:31:00', '2016-11-11 09:19:18'),
(5, 132, 'test', 'test', 'erwrewr', 'test@gmail.com', '8978787878', '9840568439', '$2y$10$w76RfgY2FbW54sPrtqcNzurpPODRu5DRgpy9FLI1R3VJDmuv4uNvq', 'hghjhjjjk', '1', '1', 6, '2016-10-01 11:37:40', '2016-11-11 09:20:01'),
(6, 212, 'Mohideen', 'abdul', 'kadir', 'mohideen.a@royalcyber.com', '9894440092', '9894440092', '$2y$10$SgHx4jbNMla3vDQBP2ktH.iTsMXvmZNqKbeJgDmnWSqd7m.S.F4x6', '32, Hallmark towers\r\nGuindy\r\nChennai', '1', '1', 5, '2016-09-21 16:25:41', '2016-10-05 09:16:30'),
(7, 11115, 'Tariq', 'ashref', 'mohammad', 'david@royalcyber.com', '9840568439', '9840568439', '$2y$10$pPWXul8Z3OCzta3D64QgsOgVfKXhlpK4LV1TJA2kHhfSz8b.Yus0i', 'No.1, SIDCO Industrial Estate, Guindy, NH45, Guindy, Chennai, Tamil Nadu 600032', '1', '2', 2, '2016-09-26 12:34:36', '2016-10-24 12:22:26'),
(8, 2147483647, 'david', 'j', 'jason', 'david1@royalcyber.com', '34234234', '234324324', '$2y$10$pCbuNg7U11ZwozmAV/0fsOIAkUE1Y.8GVoypI3kYzEmRgdZvg.cS2', 'dsfdsfdsf', '1', '1', 1, '2016-10-20 09:02:43', '2016-10-24 12:43:20'),
(9, 213213, 'sadasd', 'sadsa', 'dsad', 'david2@royalcyber.com', '34234324324', '4324324', '$2y$10$nllYUXnHxYy23FuxBHnW2.sXw75gbdi8F6wqmFhx9/oAWjjN3wy3y', 'fdsfsdfsdfsdfdsf', '1', '4', 5, '2016-10-24 12:24:41', '2016-10-24 14:42:27'),
(11, 323, 'fsd', 'sdf', 'sfdgsdf', 'sdf@efdsasgf.ty', '934324324', '344324324324', '$2y$10$vYgT0k.thydER99c8IMY6.PNMY.kGdlLDF0JiZDWDfIimCaNUd8b.', 'sadsad', '1', '0', 1, '2016-11-10 17:11:18', '2016-11-10 17:11:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_distribution_mapping`
--

CREATE TABLE IF NOT EXISTS `user_distribution_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `distribution_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`distribution_id`),
  KEY `distribution_id` (`distribution_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user_distribution_mapping`
--

INSERT INTO `user_distribution_mapping` (`id`, `user_id`, `distribution_id`) VALUES
(1, 1, 1),
(3, 5, 2),
(9, 7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_driver_mapping`
--

CREATE TABLE IF NOT EXISTS `user_driver_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`driver_id`),
  KEY `driver_id` (`driver_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_driver_mapping`
--

INSERT INTO `user_driver_mapping` (`id`, `user_id`, `driver_id`) VALUES
(1, 1, 4),
(8, 6, 4),
(7, 7, 5);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `distribution_replacement`
--
ALTER TABLE `distribution_replacement`
  ADD CONSTRAINT `distribution_replacement_ibfk_1` FOREIGN KEY (`distribution_id`) REFERENCES `distribution_center` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `distribution_replacement_ibfk_2` FOREIGN KEY (`replace_distribution_id`) REFERENCES `distribution_center` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `driver_info`
--
ALTER TABLE `driver_info`
  ADD CONSTRAINT `driver_info_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `driver_replacement`
--
ALTER TABLE `driver_replacement`
  ADD CONSTRAINT `driver_replacement_ibfk_1` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `driver_replacement_ibfk_2` FOREIGN KEY (`replace_driver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `login_history`
--
ALTER TABLE `login_history`
  ADD CONSTRAINT `login_history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thaali`
--
ALTER TABLE `thaali`
  ADD CONSTRAINT `thaali_ibfk_1` FOREIGN KEY (`caterer_id`) REFERENCES `caterer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_distribution_mapping`
--
ALTER TABLE `user_distribution_mapping`
  ADD CONSTRAINT `user_distribution_mapping_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_distribution_mapping_ibfk_2` FOREIGN KEY (`distribution_id`) REFERENCES `distribution_center` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_driver_mapping`
--
ALTER TABLE `user_driver_mapping`
  ADD CONSTRAINT `user_driver_mapping_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_driver_mapping_ibfk_2` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
